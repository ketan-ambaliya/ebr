<?php
/**
 * Template Name: superintendent Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		<section class="overlay breadcrumbs " style="background-image: url(<?php echo $path; ?>/img/breadcrumb.jpg);">
			<div class="container-fluid">
				<div class="row">					
					<div class="col-md-12">
					 <ul>
					 	<li><a href="">About</a></li>
					 	<li><a href="">SUPERINTENDENT & LEADERSHIP</a></li>
					 </ul>					
					</div>
				</div>
			</div>
		</section>

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<label>SUPERTINTENDENT & LEADERSHIP</label>
						<h3>Meet the Leadership Team</h3>
						<p>Learn more about Our Superintendent, Warren Drake, and our Senior Cabinet members.</p>
					</div>	
				</div>		
		</section>
	<!-- 	<?//php echo do_shortcode("[shortcode category='superintendent']");?> --> 
	<!-- image size must be 825px x 550px -->

		<section class="areapart-img">	
			<div class="row">
				<div class="col-lg-6 leftimg">
					<img src="<?php echo $path; ?>/img/supre.jpg">
				</div>	
				<div class="col-lg-6">
					<div class="slide-description">
						<h1>A Message from our Superintendent</h1>
	       				<p><label>WARREN DRAKE</label></p>
	       				<hr>
						<h4>LEE MAGNET HIGH SCHOOL GIRL’S BASKETBALL COACH</h4>
						<p>“The East Baton Rouge Parish School System serves more than 41,000 students in pre-kindergarten through 12th grade and we take tremendous pride in ‘Building the Future of Baton Rouge.’ We are committed to educating every student in a welcoming, inclusive and safe environment. Our mission is to pave the way for students to succeed, by providing technology rich instruction and an array of unique and interactive programs. EBRPSS students continue to show remarkable growth and progress towards closing the achievement gap across all subject areas. In 2018 EBR grew +3 points in growth to mastery on the LEAP 2025 scores, compared to the state’s average of +1. The district matched the state, earning a “B” letter grade in the new progress formula and EBRPSS ranks 1st in the state for the 2018 number of Advanced Placement tests taken with a qualifying score of 3 or higher. We know that students must be prepared for life when they leave us, so we are enhancing our traditional, magnet and choice academic programs and creating new opportunities constantly to meet the diverse interests of every child.”</p>
						<a href="" tabindex="0">get to know him</a>
					</div>
				</div>	

				<div class="col-lg-6 leftimg order-2">
					<img src="<?php echo $path; ?>/img/supre.jpg">
				</div>	
				<div class="col-lg-6">
					<div class="slide-description">
						<h1>A Message from our Superintendent</h1>
	       				<p><label>WARREN DRAKE</label></p>
	       				<hr>
						<h4>LEE MAGNET HIGH SCHOOL GIRL’S BASKETBALL COACH</h4>
						<p>“The East Baton Rouge Parish School System serves more than 41,000 students in pre-kindergarten through 12th grade and we take tremendous pride in ‘Building the Future of Baton Rouge.’ We are committed to educating every student in a welcoming, inclusive and safe environment. Our mission is to pave the way for students to succeed, by providing technology rich instruction and an array of unique and interactive programs. EBRPSS students continue to show remarkable growth and progress towards closing the achievement gap across all subject areas. In 2018 EBR grew +3 points in growth to mastery on the LEAP 2025 scores, compared to the state’s average of +1. The district matched the state, earning a “B” letter grade in the new progress formula and EBRPSS ranks 1st in the state for the 2018 number of Advanced Placement tests taken with a qualifying score of 3 or higher. We know that students must be prepared for life when they leave us, so we are enhancing our traditional, magnet and choice academic programs and creating new opportunities constantly to meet the diverse interests of every child.”</p>
						<a href="" tabindex="0">get to know him</a>
					</div>
				</div>
			</div>	
		</section>

		<section class="overlapcontent" style="background-image: url(<?php echo $path; ?>/img/banner1.jpg);">
			<div class="overalappart inner-container">
				<div class="row paragraphpart">
					<div class="col-md-12 text-center">
						<h1>A Message from our Superintendent</h1>
						<p><label>WARREN DRAKE</label></p>
						<hr>
						<h4>LEE MAGNET HIGH SCHOOL GIRL’S BASKETBALL COACH</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus quidem officiis magnam! Mollitia reiciendis maxime, quae accusantium quo, aut commodi est earum possimus iusto quod similique deleniti inventore libero, temporibus!</p>
						<a href="" class="btn">find out how to enroll</a>
					</div>
				</div>
			</div>	
		</section>	

		<section class="yellowadvance">
			<div class="content-conainer">
				<div class="row justify-content-center">
					<div class="col-md-4">
						<div class="darkshade">
						<h2 class="no-background">
							<span>ONE TEAM, ONE MISSION</span>					
					    </h2>
					    <p>Read how EBRPSS is planning to reach its goals. </p>
					    <div class="text-right"><a href="" class="btn"> STRATEGIC PLAN</a></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="darkshade">
						<h2 class="no-background">
							<span>ONE ORGANIZATION</span>					
					    </h2>
					    <p>Learn more about how EBRPSS functions. </p>
					    <div class="text-right"><a href="" class="btn"> ORGANIZATIONAL CHART</a></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="darkshade">
						<h2 class="no-background">
							<span>100 DAYS & BEYOND!</span>					
					    </h2>
					    <p>See the plan that the Superintendent inacted when he started with EBRPSS.</p>
					    <div class="text-right"><a href="" class="btn">100 DAY PLAN</a></div>
						</div>
					</div>					
			</div>
		</section>

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<label>MEET OUR SENIOR CABINET</label>						
						<p class="subtitle">Under the guidance of Superintendent Warren Drake, East Baton Rouge Parish Schools are committed to providing <br>quality education for the students of the capital city. The District Leadership Team is here to serve you.</p>
					</div>	
				</div>		
		</section>

		<section class="inner-container">
			<div class="row justify-content-center">
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<img src="<?php echo $path; ?>/img/board-member.png">
						<div class="ribbonpart"><label>MARK BELLUE</label></div>
						<span>General Counsel</span>
						<a href="#" class="btn">GET TO KNOW THEM</a>
					</div>	
				</div>
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<img src="<?php echo $path; ?>/img/board-member2.png">
						<div class="ribbonpart"><label>DADRIUS LANUS</label></div>
						<span>Associate Superintendent</span>						
						<a href="#" class="btn">GET TO KNOW THEM</a>
					</div>	
				</div>
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<img src="<?php echo $path; ?>/img/board-member3.png">
						<div class="ribbonpart"><label>TRAMELLE HOWARD</label></div>
						<span>Associate Superintendent</span>
						<a href="#" class="btn">GET TO KNOW THEM</a>
					</div>	
				</div>	
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<img src="<?php echo $path; ?>/img/board-member.png">
						<div class="ribbonpart"><label>MARK BELLUE</label></div>
						<span>General Counsel</span>
						<a href="#" class="btn">GET TO KNOW THEM</a>
					</div>	
				</div>
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<img src="<?php echo $path; ?>/img/board-member.png">
						<div class="ribbonpart"><label>MARK BELLUE</label></div>
						<span>General Counsel</span>
						<a href="#" class="btn">GET TO KNOW THEM</a>
					</div>	
				</div>
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<img src="<?php echo $path; ?>/img/board-member.png">
						<div class="ribbonpart"><label>MARK BELLUE</label></div>
						<span>General Counsel</span>
						<a href="#" class="btn">GET TO KNOW THEM</a>
					</div>	
				</div>
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<img src="<?php echo $path; ?>/img/board-member.png">
						<div class="ribbonpart"><label>MARK BELLUE</label></div>
						<span>General Counsel</span>
						<a href="#" class="btn">GET TO KNOW THEM</a>
					</div>	
				</div>
			</div>	
		</section>


		<section class="selectionpart">
			<div class="title">learn more about our:</div>			
		<div class="bottom_menu">
			<ul>
				<li class="active"><a href="">Leadership</a></li>
				<li><a href="">Story</a></li>
				<li><a href="">board</a></li>
				<li><a href="">policies</a></li>
				<li><a href="">communication</a></li>
			</ul>
		</div>
		</section>

		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
