<?php
/**
 * Template Name: school detail Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
$url = home_url();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		<!-- <section class="overlay breadcrumbs " style="background-image: url(<?php echo $path; ?>/img/breadcrumb.jpg);">
			<div class="container-fluid">
				<div class="row">					
					<div class="col-md-12">
					 <ul>
					 	<li><a href="">School</a></li>
					 	<li><a href="">Broadmoor middle school</a></li>
					 </ul>					
					</div>
				</div>
			</div>
		</section> -->

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<label>Broadmoor middle school</label>
						<h3>The Buccaneers</h3>
					</div>	
				</div>		
		</section>
		<div class="overlapcontent" style="background-image: url(<?php echo $path; ?>/img/banner1.jpg);">				
		</div>
		<div class="overlaptextarea">
			<div class="overalappart inner-container">
				<div class="row paragraphpart">
					<div class="col-md-12 text-center">
						<label>6th - 8th grade</label>
						<h3>About our school</h3>
					</div>
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus quidem officiis magnam! Mollitia reiciendis maxime, quae accusantium quo, aut commodi est earum possimus iusto quod similique deleniti inventore libero, temporibus!</p>
				<a href="" class="btn">find out how to enroll</a>			
			</div>
		</div>
			
		<section class="yellowadvance yfixed">
			<div class="content-conainer">
				<div class="row justify-content-center">
					<div class="col-md-6">
						<div class="darkshade">
						<h2 class="no-background">
							<span>Address</span>					
					    </h2>
					    <p>1225 Sharp Road, Baton Rouge, LA 10815</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="darkshade">
						<h2 class="no-background">
							<span>Contact</span>					
					    </h2>
					    <p><strong>Ph</strong> : <a href="tel:2252720540">(225) 272-0540</a></p>
						</div>
					</div>
					<div class="col-md-12">
						<div class="darkshade">
						<h2 class="no-background">
							<span>Principal</span>					
					    </h2>
					    <p>Daniel B. Edwards is the current principal of Coadmoor Middle School.</p>
						</div>
					</div>					
			</div>
		</section>
	
		<section class="did-you-konw-section blue_bg" style="background-color: #002e59;">
			<div class="inner-container">
				<div class="row">
					<div class="col-md-8">
						<label>academics, Attendance and attitude</label>
						<p>
							High School courses include Algebra I, Journey to Careers, Keyboarding, and Family and Consumer Sciences. The Buccaneers Can also opt to participate in a variety of basic athletics, sports and clubs.
						</p>
						<a href="#">Visit School Website</a>
					</div>
					<div class="col-md-4">
						<div class="image-thum-block">
							<img src="<?php echo $path; ?>/img/ne1.jpg" alt="" class="primary-image">
							<img src="<?php echo $path; ?>/img/ne2.jpg" alt="" class="secondary-image">
						</div>
					</div>
				</div>
			</div>
		</section>

     	<section class="parallaxpart threebox" style="background-image: url(<?php echo $path; ?>/img/banner2.jpg);">
			<div class="ribbonpart"><label>you may also want to...</label></div>
			<div class="inner-container">
				<div class="row justify-content-center display-flex">
					<div class="col-md-6 col-lg-4">
						<div class="greenbox">
							<h2 >Find Anpther school </h2>
							<hr>
							<p class="text-center">View our full list school and search our interactive map</p>
							<a href="" class="btn">See all schools</a>
							</div>					   
					</div> 
					<div class="col-md-6 col-lg-4">
						<div class="greenbox">
							<h2 >Login to JCampus </h2>
							<hr>
							<p class="text-center">Login to the EBRPSS student progress Center.</p>
							<a href="" class="btn">enroll now</a>
							</div>					   
					</div> 
					<div class="col-md-6 col-lg-4">
						<div class="greenbox">
							<h2 >Access Online payments </h2>
							<hr>
							<p class="text-center">Access the portal for school fees & othere payments</p>
							<a href="" class="btn">enroll now</a>
							</div>					   
					</div>    
				</div>	
			</div>			
		</section>
		


		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
