<?php
/**
 * Template Name: News page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
					<label>	
						<img src="<?php echo $path; ?>/img/news.png" /> NEWS
					</label>					
						<h3>See What is Going on in East Baton Rouge</h3>
					</div>	
				</div>		
		</section>

		

		<section class="inner-container news_content">	
			<div class="row">	
			    <div class="col-lg-8">
			    	<label>Recent News </label>
			    	<ul class="nav nav-tabs" id="myTab" role="tablist">
					  <li class="nav-item">
					    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#all" role="tab" aria-controls="home" aria-expanded="true">All</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" id="news-tab" data-toggle="tab" href="#news" role="tab" aria-controls="profile">News</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" id="Academics-tab" data-toggle="tab" href="#Academics" role="tab" aria-controls="Academics">Academics</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" id="athletics-tab" data-toggle="tab" href="#athletics" role="tab" aria-controls="athletics">Athletics</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" id="Arts-tab" data-toggle="tab" href="#Arts" role="tab" aria-controls="Arts">Arts</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" id="Galleries-tab" data-toggle="tab" href="#Galleries" role="tab" aria-controls="Galleries">Galleries</a>
					  </li>
					</ul>

					<div class="tab-content" id="myTabContent">
					  	<div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="home-tab">
						    <div class="newspart">
							    <div class="row">
				         			<div class="col-12 col-md-6 ">
				         				<a href="">
				         					<div class="imgcontainer">
					         					<div class="img-thumbnails">
					         					  <img src="<?php echo $path; ?>/img/ev1.jpg"> 
					         				    </div>
				         					</div>	
				         			 	</a>
				         			</div>	
				         			<div class="col-12 col-md-6 ">
				         				<a href=""><h2>Biden Project Youth Leads</h2></a>
				         				<p><span>ACADEMICS</span>  DEC. 1 2018 </p>
				         				<p class="sortcontent">
				         					Throughout the district, elementary schools invited their students’ family members to Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
				         				</p>	
				         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				         			</div>	
				         		</div>
				         		<hr>
				         		<div class="row">
				         			<div class="col-12 col-md-6 ">
				         				<a href="">
				         					<div class="imgcontainer">
					         					<div class="img-thumbnails">
					         					  <img src="<?php echo $path; ?>/img/ev2.jpg"> 
					         				    </div>
				         					</div>	
				         				</a>	
				         			</div>	
				         			<div class="col-12 col-md-6 ">
				         				<a href=""><h2>EBR School Board Recognitions at the March 21 Regular Meeting</h2></a>
				         				<p><span class="green">announcement</span>  jan. 5 2019 </p>
				         				<p class="sortcontent">
				         					Throughout the district, elementary schools invited their students’ family members to...
				         				</p>	
				         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				         			</div>	
			         			</div>
			         			<hr>
			         			<div class="row">
				         			<div class="col-12 col-md-6 ">
				         				<a href="">
				         					<div class="imgcontainer">
					         					<div class="img-thumbnails">
					         					  <img src="<?php echo $path; ?>/img/ev1.jpg"> 
					         				    </div>
				         					</div>	
				         			 	</a>
				         			</div>	
				         			<div class="col-12 col-md-6 ">
				         				<a href=""><h2>Biden Project Youth Leads</h2></a>
				         				<p><span>ACADEMICS</span>  DEC. 1 2018 </p>
				         				<p class="sortcontent">
				         					Throughout the district, elementary schools invited their students’ family members to Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
				         				</p>	
				         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				         			</div>	
				         		</div>
				         		<hr>
				         		<div class="row">
				         			<div class="col-12 col-md-6 ">
				         				<a href="">
				         					<div class="imgcontainer">
					         					<div class="img-thumbnails">
					         					  <img src="<?php echo $path; ?>/img/ev2.jpg"> 
					         				    </div>
				         					</div>	
				         				</a>	
				         			</div>	
				         			<div class="col-12 col-md-6 ">
				         				<a href=""><h2>EBR School Board Recognitions at the March 21 Regular Meeting</h2></a>
				         				<p><span class="green">announcement</span>  jan. 5 2019 </p>
				         				<p class="sortcontent">
				         					Throughout the district, elementary schools invited their students’ family members to...
				         				</p>	
				         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				         			</div>	
			         			</div>
						    </div>
						    <hr>
						    <nav aria-label="...">
						      <ul class="pagination">
						        <!-- <li class="page-item disabled">
						          <a class="page-link" href="#" tabindex="-1">PAGINATION 1</a>
						        </li> -->
						        <li class="page-item"><a class="page-link" href="#">1</a></li>
						        <li class="page-item active">
						          <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
						        </li>
						        <li class="page-item"><a class="page-link" href="#">3</a></li>
						        <li class="page-item">
						          <a class="page-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
						        </li>
						      </ul>
						    </nav>
					  	</div>
						<div class="tab-pane fade" id="news" role="tabpanel" aria-labelledby="news-tab">
							   <div class="newspart">
								    <div class="row">
					         			<div class="col-12 col-md-6">
					         				<a href="">
					         					<div class="imgcontainer">
						         					<div class="img-thumbnails">
						         					  <img src="<?php echo $path; ?>/img/ev3.jpg"> 
						         				    </div>
					         					</div>	
					         			 	</a>
					         			</div>	
					         			<div class="col-12 col-md-6">
					         				<a href=""><h2>Biden Project Youth Leads</h2></a>
					         				<p><span>ACADEMICS</span>  DEC. 1 2018 </p>
					         				<p class="sortcontent">
					         					Throughout the district, elementary schools invited their students’ family members to Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
					         				</p>	
					         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
					         			</div>	
					         		</div>
					         		<hr>
					         		<div class="row">
					         			<div class="col-12 col-md-6">
					         				<a href="">
					         					<div class="imgcontainer">
						         					<div class="img-thumbnails">
						         					  <img src="<?php echo $path; ?>/img/ev4.jpg"> 
						         				    </div>
					         					</div>	
					         				</a>	
					         			</div>	
					         			<div class="col-12 col-md-6 ">
					         				<a href=""><h2>EBR School Board Recognitions at the March 21 Regular Meeting</h2></a>
					         				<p><span class="green">announcement</span>  jan. 5 2019 </p>
					         				<p class="sortcontent">
					         					Throughout the district, elementary schools invited their students’ family members to...
					         				</p>	
					         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
					         			</div>	
				         			</div>
							    </div>
							    <hr>
							   	<nav aria-label="...">
							      <ul class="pagination">
							        <!-- <li class="page-item disabled">
							          <a class="page-link" href="#" tabindex="-1">PAGINATION 1</a>
							        </li> -->
							        <li class="page-item"><a class="page-link" href="#">1</a></li>
							        <li class="page-item active">
							          <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
							        </li>
							        <li class="page-item"><a class="page-link" href="#">3</a></li>
							        <li class="page-item">
							          <a class="page-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							        </li>
							      </ul>
							    </nav>
						</div>
						<div class="tab-pane fade" id="Academics" role="tabpanel" aria-labelledby="Academics-tab">
						    <div class="newspart">
							    <div class="row">
				         			<div class="col-12 col-md-6">
				         				<a href="">
				         					<div class="imgcontainer">
					         					<div class="img-thumbnails">
					         					  <img src="<?php echo $path; ?>/img/ev1.jpg"> 
					         				    </div>
				         					</div>	
				         			 	</a>
				         			</div>	
				         			<div class="col-12 col-md-6">
				         				<a href=""><h2>Biden Project Youth Leads</h2></a>
				         				<p><span>ACADEMICS</span>  DEC. 1 2018 </p>
				         				<p class="sortcontent">
				         					Throughout the district, elementary schools invited their students’ family members to Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
				         				</p>	
				         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				         			</div>	
				         		</div>
				         		<hr>
				         		<div class="row">
				         			<div class="col-12 col-md-6">
				         				<a href="">
				         					<div class="imgcontainer">
					         					<div class="img-thumbnails">
					         					  <img src="<?php echo $path; ?>/img/ev2.jpg"> 
					         				    </div>
				         					</div>	
				         				</a>	
				         			</div>	
				         			<div class="col-12 col-md-6 ">
				         				<a href=""><h2>EBR School Board Recognitions at the March 21 Regular Meeting</h2></a>
				         				<p><span class="green">announcement</span>  jan. 5 2019 </p>
				         				<p class="sortcontent">
				         					Throughout the district, elementary schools invited their students’ family members to...
				         				</p>	
				         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				         			</div>	
			         			</div>
						    </div>
						    <hr>
						    <nav aria-label="...">
						      <ul class="pagination">
						        <!-- <li class="page-item disabled">
						          <a class="page-link" href="#" tabindex="-1">PAGINATION 1</a>
						        </li> -->
						        <li class="page-item"><a class="page-link" href="#">1</a></li>
						        <li class="page-item active">
						          <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
						        </li>
						        <li class="page-item"><a class="page-link" href="#">3</a></li>
						        <li class="page-item">
						          <a class="page-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
						        </li>
						      </ul>
						    </nav>
					  	</div>
						<div class="tab-pane fade" id="athletics" role="tabpanel" aria-labelledby="athletics-tab">
							   <div class="newspart">
								    <div class="row">
					         			<div class="col-12 col-md-6">
					         				<a href="">
					         					<div class="imgcontainer">
						         					<div class="img-thumbnails">
						         					  <img src="<?php echo $path; ?>/img/ev3.jpg"> 
						         				    </div>
					         					</div>	
					         			 	</a>
					         			</div>	
					         			<div class="col-12 col-md-6">
					         				<a href=""><h2>Biden Project Youth Leads</h2></a>
					         				<p><span>ACADEMICS</span>  DEC. 1 2018 </p>
					         				<p class="sortcontent">
					         					Throughout the district, elementary schools invited their students’ family members to Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
					         				</p>	
					         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
					         			</div>	
					         		</div>
					         		<hr>
					         		<div class="row">
					         			<div class="col-12 col-md-6">
					         				<a href="">
					         					<div class="imgcontainer">
						         					<div class="img-thumbnails">
						         					  <img src="<?php echo $path; ?>/img/ev4.jpg"> 
						         				    </div>
					         					</div>	
					         				</a>	
					         			</div>	
					         			<div class="col-12 col-md-6 ">
					         				<a href=""><h2>EBR School Board Recognitions at the March 21 Regular Meeting</h2></a>
					         				<p><span class="green">announcement</span>  jan. 5 2019 </p>
					         				<p class="sortcontent">
					         					Throughout the district, elementary schools invited their students’ family members to...
					         				</p>	
					         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
					         			</div>	
				         			</div>
							    </div>
							    <hr>
							   	<nav aria-label="...">
							      <ul class="pagination">
							        <!-- <li class="page-item disabled">
							          <a class="page-link" href="#" tabindex="-1">PAGINATION 1</a>
							        </li> -->
							        <li class="page-item"><a class="page-link" href="#">1</a></li>
							        <li class="page-item active">
							          <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
							        </li>
							        <li class="page-item"><a class="page-link" href="#">3</a></li>
							        <li class="page-item">
							          <a class="page-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							        </li>
							      </ul>
							    </nav>
						</div>
						<div class="tab-pane fade" id="Arts" role="tabpanel" aria-labelledby="Arts-tab">
						    <div class="newspart">
							    <div class="row">
				         			<div class="col-12 col-md-6">
				         				<a href="">
				         					<div class="imgcontainer">
					         					<div class="img-thumbnails">
					         					  <img src="<?php echo $path; ?>/img/ev1.jpg"> 
					         				    </div>
				         					</div>	
				         			 	</a>
				         			</div>	
				         			<div class="col-12 col-md-6">
				         				<a href=""><h2>Biden Project Youth Leads</h2></a>
				         				<p><span>ACADEMICS</span>  DEC. 1 2018 </p>
				         				<p class="sortcontent">
				         					Throughout the district, elementary schools invited their students’ family members to Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
				         				</p>	
				         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				         			</div>	
				         		</div>
				         		<hr>
				         		<div class="row">
				         			<div class="col-12 col-md-6">
				         				<a href="">
				         					<div class="imgcontainer">
					         					<div class="img-thumbnails">
					         					  <img src="<?php echo $path; ?>/img/ev2.jpg"> 
					         				    </div>
				         					</div>	
				         				</a>	
				         			</div>	
				         			<div class="col-12 col-md-6 ">
				         				<a href=""><h2>EBR School Board Recognitions at the March 21 Regular Meeting</h2></a>
				         				<p><span class="green">announcement</span>  jan. 5 2019 </p>
				         				<p class="sortcontent">
				         					Throughout the district, elementary schools invited their students’ family members to...
				         				</p>	
				         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				         			</div>	
			         			</div>
						    </div>
						    <hr>
						    <nav aria-label="...">
						      <ul class="pagination">
						        <!-- <li class="page-item disabled">
						          <a class="page-link" href="#" tabindex="-1">PAGINATION 1</a>
						        </li> -->
						        <li class="page-item"><a class="page-link" href="#">1</a></li>
						        <li class="page-item active">
						          <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
						        </li>
						        <li class="page-item"><a class="page-link" href="#">3</a></li>
						        <li class="page-item">
						          <a class="page-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
						        </li>
						      </ul>
						    </nav>
					  	</div>
						<div class="tab-pane fade" id="Galleries" role="tabpanel" aria-labelledby="Galleries-tab">
							   <div class="newspart">
								    <div class="row">
					         			<div class="col-12 col-md-6">
					         				<a href="">
					         					<div class="imgcontainer">
						         					<div class="img-thumbnails">
						         					  <img src="<?php echo $path; ?>/img/ev3.jpg"> 
						         				    </div>
					         					</div>	
					         			 	</a>
					         			</div>	
					         			<div class="col-12 col-md-6">
					         				<a href=""><h2>Biden Project Youth Leads</h2></a>
					         				<p><span>ACADEMICS</span>  DEC. 1 2018 </p>
					         				<p class="sortcontent">
					         					Throughout the district, elementary schools invited their students’ family members to Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
					         				</p>	
					         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
					         			</div>	
					         		</div>
					         		<hr>
					         		<div class="row">
					         			<div class="col-12 col-md-6">
					         				<a href="">
					         					<div class="imgcontainer">
						         					<div class="img-thumbnails">
						         					  <img src="<?php echo $path; ?>/img/ev4.jpg"> 
						         				    </div>
					         					</div>	
					         				</a>	
					         			</div>	
					         			<div class="col-12 col-md-6 ">
					         				<a href=""><h2>EBR School Board Recognitions at the March 21 Regular Meeting</h2></a>
					         				<p><span class="green">announcement</span>  jan. 5 2019 </p>
					         				<p class="sortcontent">
					         					Throughout the district, elementary schools invited their students’ family members to...
					         				</p>	
					         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
					         			</div>	
				         			</div>
							    </div>
							    <hr>
							   	<nav aria-label="...">
							      <ul class="pagination">
							        <!-- <li class="page-item disabled">
							          <a class="page-link" href="#" tabindex="-1">PAGINATION 1</a>
							        </li> -->
							        <li class="page-item"><a class="page-link" href="#">1</a></li>
							        <li class="page-item active">
							          <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
							        </li>
							        <li class="page-item"><a class="page-link" href="#">3</a></li>
							        <li class="page-item">
							          <a class="page-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							        </li>
							      </ul>
							    </nav>
						</div>
					</div>

			    </div>	
			   
			    <div class="col-lg-4 sidebar-blue-section">
					<div class="blue-border-box one">
						<div class="ribbonpart"><label>Learn more</label></div>
						
						
						<label>Categories</label>
						<ul>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> News</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Academics</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Graphics Arts</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Announcements</a></li>
						</ul>	
						<hr>
						<label>find us follow us</label>
						<ul>
							<li><a class="f_links" href="#"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter direstory</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i> You tube</a></li>
						</ul>
						<hr>
						<ul>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> News</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Academics</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Graphics Arts</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Announcements</a></li>
						</ul>
					</div>

					<div class="eventpart">
			         	<label>Events</label>	
			         	<div class="row">
		         			<div class="col-12 ">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne1.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">jun<br><strong>1</strong></span>
										</div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 ">
		         				<a href=""><h2>I CARE PREVENTION SUMMIT</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza... and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>	

		         		<div class="row">
		         			<div class="col-12 ">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne2.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">jun<br><strong>1</strong></span>
										</div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 ">
		         				<a href=""><h2>I CARE PREVENTION SUMMIT</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza... and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>  

		         		<a href="" class="btn">see all events</a>       	
				    </div>
				</div>	

			    	
			</div>    
		</section>
		


		<section class="inner-container bottom-dark-border-part">						
				<div class="blue-border-box one">
					<div class="ribbonpart"><label>Our goal</label></div>
					<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis, reprehenderit adipisci, rem officiis ut quaerat alias illo. Asperiores perspiciatis odio fugiat rerum aliquid dolorem, quas voluptates doloribus! Expedita, porro, sequi!</p>
				</div>
			
		</section>	

		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
