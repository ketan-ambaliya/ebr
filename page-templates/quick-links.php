<?php
/**
 * Template Name: Quick Links Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		<section class="overlay breadcrumbs " style="background-image: url(<?php echo $path; ?>/img/breadcrumb.jpg);">
			<div class="container-fluid">
				<div class="row">					
					<div class="col-md-12">
					 <ul>
					 	<li><a href="">Quick Links</a></li>					 	
					 </ul>					
					</div>
				</div>
			</div>
		</section>

		<section class="inner-container paragraphpart">	
			<div class="row">
				<div class="col-md-12 text-center">
					<img src="<?php echo $path; ?>/img/link.png">  
					<label>Quick Links</label>						
				</div>	 
			</div>		
		</section>
		
		<section class="bluecontainer featuredbox-sec">			
			<div class="row">
				<div class="col-md-12 text-center">
					<img src="<?php echo $path; ?>/img/employee.png"> 
					<h2>Employees</h2>
					<p>See below for some of the links used most by our Employees.</p>
				</div>
			</div>			 	
		</section>
		

		<section class="inner-container visitlink-sec">
			<div class="row justify-content-center display-flex">
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>EBR BEnefits</h3>
						<p>Employee benefits website</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>HELPDESK</h3>
						<p>IT Helpdesk Tickets</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>ESCHOOL SOLUTIONS</h3>
						<p>Substitute System</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>PAYROLL LOGIN</h3>
						<p>Employee Self -Service Login</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>EMPLOYEE EMAIL LOGIN</h3>
						<p>Web Access to EMAIL</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>PASS</h3>
						<p>Password & Account Self -Service</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>School Walk-Through</h3>
						<p>Form Request for Accountability & School Evaluations</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>GUIDEBOOK</h3>
						<p>District Professional Development Events & Resources</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>CAMPUS</h3>
						<p>Classroom Management Tool</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>JCAMPUS</h3>
						<p>Staff Info Portal</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>GOSIGNMEUP</h3>
						<p>Browse Course Offerings</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>CANVAS</h3>
						<p>Teacher Login</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>REQUEST SUPERINTENDENT SIGNATURE</h3>
						<p>Form Request</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>MEDIA COVERAGE Request</h3>
						<p>Form Request for Coverage</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>ONCOURSE</h3>
						<p>Teacher site login to system for Education</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>APPLY FOR GRANTS</h3>
						<p>Apply for a grant here</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
			</div>	
		</section>

		<section class="bluecontainer featuredbox-sec">			
			<div class="row">
				<div class="col-md-12 text-center">
					<img src="<?php echo $path; ?>/img/employee.png"> 
					<h2>Parents/Students</h2>
					<p>See below for some of the links used most by our students and parents.</p>
				</div>
			</div>			 	
		</section>

		<section class="inner-container visitlink-sec">
			<div class="row justify-content-center display-flex">
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>School & Bus Route</h3>
						<p>Locate the proper school bus & School school routes in your area</p>				
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>ONLINE SCHOOL PAYMENTS</h3>
						<p>(OSP) Portal for School fees and other payments</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>jcampus</h3>
						<p>Student Progress Center</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<h3>TRANSPORATION</h3>
						<p>For Transportation Questions you can call 225-226-3784</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
			</div>
		</section>




		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
