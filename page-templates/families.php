<?php
/**
 * Template Name: Families page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		<!-- <section class="overlay breadcrumbs " style="background-image: url(<?php echo $path; ?>/img/breadcrumb.jpg);">
			<div class="container-fluid">
				<div class="row">					
					<div class="col-md-12">
					 <ul>					 	
					 	<li><a href="">Families</a></li>
					 </ul>						
					</div>
				</div>
			</div>
		</section> -->

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<label>Families</label>						
						<p>See below for the main information you will need from the Communications department. </p>
					</div>	
				</div>		
		</section>

		<section class="parallaxpart threebox" style="background-image: url(<?php echo $path; ?>/img/banner2.jpg);">
			<div class="inner-container">
				<div class="row justify-content-center">
					<div class="col-md-6 col-lg-4">
						<div class="greenbox">
							<h2> Enroll a student </h2>
							<hr>				    
							<p class="text-center">Find out more about the enrollment process and the choices you have for your child’s education. </p>
							<a href="" class="btn">Learn more</a>						   
						</div>    
					</div>	
						
					<div class="col-md-6 col-lg-4">
						<div class="greenbox">
							<h2> Schools </h2>
							<hr>				    
							<p class="text-center">View our interactive map of schools and locate the nearest school to you.</p>
							<a href="" class="btn">Learn more</a>						   
						</div>    
					</div>

					<div class="col-md-6 col-lg-4">
						<div class="greenbox">
							<h2> Get Involved </h2>
							<hr>				    
							<p class="text-center">Learn more about Volunteers in Public Schools (VIPS) and other ways to get involved in EBRPSS.</p>
							<a href="" class="btn">Learn more</a>						   
						</div>    
					</div>

					<div class="col-md-6 col-lg-4">
						<div class="greenbox">
							<h2> Bus Routes </h2>
							<hr>				    
							<p class="text-center">Click the button to access our School Bus route finder.</p>
							<a href="" class="btn">Read more</a>						   
						</div>    
					</div>

					<div class="col-md-6 col-lg-4">
						<div class="greenbox">
							<h2> Lunch menus </h2>
							<hr>				    
							<p class="text-center">Visit the Lunch menus from our Child Nutrition Department.</p>
							<a href="" class="btn">Learn more</a>						   
						</div>    
					</div>

					<div class="col-md-6 col-lg-4">
						<div class="greenbox">
							<h2> Research Tools </h2>
							<hr>				    
							<p class="text-center">Check out our list of online research resources for students, parents and families. </p>
							<a href="" class="btn">Learn more</a>						   
						</div>    
					</div>

				</div>	
			</div>
		</section>

		<section class="inner-container communicationpart">	
			<div class="row">	
			    <div class="col-lg-8">
			    	<h2>NEW TO EBRPSS? WELCOME TO OUR SCHOOLS</h2>	
			    	<p>Welcome to the East Baton Rouge Parish School system (EBRPSS), home to eight U.S. Blue Ribbon Schools, a nationally renowned magnet program and some of the best high schools in this country, according to U.S. News and World Report.
			    	</p>

			    	<div class="yellowborderbox one">
			    		<div class="titlepart">
			    			<h2>RESOURCES FOR PARENTS</h2>
			    		</div>	
			    		<div class="tab tab2">
								<div class="tabHeader">
									<ul>
								      <li class="active">Associations</li>
								      <li>Education</li>
								      <li>Entertainment</li>
								      <li>Internet Safety</li>
								      <li>Hippy Program</li>
								      <li>Other</li>
								    </ul>
								</div>
								<div class="tabContent">
									<div class="tabItem active">										
										<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		FAMILIIES  HELPING FAMILIES OF GREATER BATON ROUGE, INC.</a>
							       		<p class="shortdetails">Information, education, training, referrals and one-to-one support for persons with disabilities and their families.</p>

							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		ATTENTION DEFICIT DISORDER (ADD) RESOURCES</a>
							       		<p class="shortdetails">Read over 100 articles written by national ADHD authorities, view the national ADHD directory an, list of service providers and more. </p>

							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		CHILDREN AND ADULTS WITH ATTENTION-DEFICIT/HYPERACTIVITY DISORDER (CHADD)</a>
							       		<p class="shortdetails">Learn more about the national organization for Children and Adults with Attention Deficit/Hyperactivity Disorders.</p>
							       		
							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		THE LOUISIANA CAPITAL AREA CHAPTER OF CHADD</a>
							       		<p class="shortdetails">Learn more about the local chapter of CHADD. To contact the local coordinator, Belynda L. Gauthier, call 225-261-0613.</p>

							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		THE DYSLEXIA ASSOCIATION OF GREATER BATON ROUGE (DAGBR)</a>
							       		<p class="shortdetails">Find out more about Brighten School, the DAGBR’s school for students with language-based learning disabilities.</p>

								    </div>
								    <div class="tabItem">								     
							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		ATTENTION DEFICIT DISORDER (ADD) RESOURCES</a>
							       		<p class="shortdetails">Read over 100 articles written by national ADHD authorities, view the national ADHD directory an, list of service providers and more. </p>

							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		CHILDREN AND ADULTS WITH ATTENTION-DEFICIT/HYPERACTIVITY DISORDER (CHADD)</a>
							       		<p class="shortdetails">Learn more about the national organization for Children and Adults with Attention Deficit/Hyperactivity Disorders.</p>
							       		
							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		THE LOUISIANA CAPITAL AREA CHAPTER OF CHADD</a>
							       		<p class="shortdetails">Learn more about the local chapter of CHADD. To contact the local coordinator, Belynda L. Gauthier, call 225-261-0613.</p>

							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		THE DYSLEXIA ASSOCIATION OF GREATER BATON ROUGE (DAGBR)</a>
							       		<p class="shortdetails">Find out more about Brighten School, the DAGBR’s school for students with language-based learning disabilities.</p>
								    </div>

								    <div class="tabItem">
								      	<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		CHILDREN AND ADULTS WITH ATTENTION-DEFICIT/HYPERACTIVITY DISORDER (CHADD)</a>
							       		<p class="shortdetails">Learn more about the national organization for Children and Adults with Attention Deficit/Hyperactivity Disorders.</p>
							       		
							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		THE LOUISIANA CAPITAL AREA CHAPTER OF CHADD</a>
							       		<p class="shortdetails">Learn more about the local chapter of CHADD. To contact the local coordinator, Belynda L. Gauthier, call 225-261-0613.</p>

							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		THE DYSLEXIA ASSOCIATION OF GREATER BATON ROUGE (DAGBR)</a>
							       		<p class="shortdetails">Find out more about Brighten School, the DAGBR’s school for students with language-based learning disabilities.</p>
								    </div>

								    <div class="tabItem">								     
							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		ATTENTION DEFICIT DISORDER (ADD) RESOURCES</a>
							       		<p class="shortdetails">Read over 100 articles written by national ADHD authorities, view the national ADHD directory an, list of service providers and more. </p>

							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		CHILDREN AND ADULTS WITH ATTENTION-DEFICIT/HYPERACTIVITY DISORDER (CHADD)</a>
							       		<p class="shortdetails">Learn more about the national organization for Children and Adults with Attention Deficit/Hyperactivity Disorders.</p>
							       		
							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		THE LOUISIANA CAPITAL AREA CHAPTER OF CHADD</a>
							       		<p class="shortdetails">Learn more about the local chapter of CHADD. To contact the local coordinator, Belynda L. Gauthier, call 225-261-0613.</p>

							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		THE DYSLEXIA ASSOCIATION OF GREATER BATON ROUGE (DAGBR)</a>
							       		<p class="shortdetails">Find out more about Brighten School, the DAGBR’s school for students with language-based learning disabilities.</p>
								    </div>

								    <div class="tabItem">										
										<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		FAMILIIES  HELPING FAMILIES OF GREATER BATON ROUGE, INC.</a>
							       		<p class="shortdetails">Information, education, training, referrals and one-to-one support for persons with disabilities and their families.</p>

							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		ATTENTION DEFICIT DISORDER (ADD) RESOURCES</a>
							       		<p class="shortdetails">Read over 100 articles written by national ADHD authorities, view the national ADHD directory an, list of service providers and more. </p>

							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		CHILDREN AND ADULTS WITH ATTENTION-DEFICIT/HYPERACTIVITY DISORDER (CHADD)</a>
							       		<p class="shortdetails">Learn more about the national organization for Children and Adults with Attention Deficit/Hyperactivity Disorders.</p>
							       		
							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		THE LOUISIANA CAPITAL AREA CHAPTER OF CHADD</a>
							       		<p class="shortdetails">Learn more about the local chapter of CHADD. To contact the local coordinator, Belynda L. Gauthier, call 225-261-0613.</p>

							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		THE DYSLEXIA ASSOCIATION OF GREATER BATON ROUGE (DAGBR)</a>
							       		<p class="shortdetails">Find out more about Brighten School, the DAGBR’s school for students with language-based learning disabilities.</p>
								    </div>

								    <div class="tabItem">								     
							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		ATTENTION DEFICIT DISORDER (ADD) RESOURCES</a>
							       		<p class="shortdetails">Read over 100 articles written by national ADHD authorities, view the national ADHD directory an, list of service providers and more. </p>

							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		CHILDREN AND ADULTS WITH ATTENTION-DEFICIT/HYPERACTIVITY DISORDER (CHADD)</a>
							       		<p class="shortdetails">Learn more about the national organization for Children and Adults with Attention Deficit/Hyperactivity Disorders.</p>
							       		
							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		THE LOUISIANA CAPITAL AREA CHAPTER OF CHADD</a>
							       		<p class="shortdetails">Learn more about the local chapter of CHADD. To contact the local coordinator, Belynda L. Gauthier, call 225-261-0613.</p>

							       		<a href="#">
							       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							       		THE DYSLEXIA ASSOCIATION OF GREATER BATON ROUGE (DAGBR)</a>
							       		<p class="shortdetails">Find out more about Brighten School, the DAGBR’s school for students with language-based learning disabilities.</p>
								    </div>
								</div>
						</div>
			    	</div>	

			    </div>
			    <div class="col-lg-4 sidebar-blue-section">
					<div class="blue-border-box one">
						<div class="ribbonpart"><label>Other important info</label></div>
						
						<label>Student & Family Quicklinks:</label>
						<ul>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Online Systems</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Medical Forms</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Attendance Zones</a></li>
						</ul>	
						<hr>
						<label>Relevant Departments:</label>
						<ul>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Child Nutrition</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Health & Intervention</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Contact direstory</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Hospital Homebound</a></li>
						</ul>
						<hr>
						<label>You may also interested in:</label>
						<ul>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> All form</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> all policies</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Contact direstory</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> All departments</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> all programs</a></li>
						</ul>
					</div>
			    </div>	
			</div>    
		</section>
		
		<section class="inner-container newspart">			
	         <div class="ss-style-triangles news">		   
	         	<h2>What is happening in EBr?</h2>	
	         </div>	   
	        <div class="newspart">
		        <div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Recent news</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link myborder"><a href="">see all</a></div>
		         	</div>		
		        </div>

		         <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-7 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev1.jpg"> 
			         				    </div>
		         					</div>	
		         			 	</a>
		         			</div>	
		         			<div class="col-12 col-md-5 spacer">
		         				<a href=""><h2>Biden Project Youth Leads</h2></a>
		         				<p><span>ACADEMICS</span>  DEC. 1 2018 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to enjoy a special Thanksgiving...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-7 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev2.jpg"> 
			         				    </div>
		         					</div>	
		         				</a>	
		         			</div>	
		         			<div class="col-12 col-md-5 spacer">
		         				<a href=""><h2>EBR School Board Recognitions at the March 21 Regular Meeting</h2></a>
		         				<p><span class="green">announcement</span>  jan. 5 2019 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-7 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev3.jpg"> 
			         				    </div>
		         					</div>	
		         				</a>	
		         			</div>	
		         			<div class="col-12 col-md-5 spacer">
		         				<a href=""><h2>JANUARY 2019 BOARD RECOGNITIONS</h2></a>
		         				<p><span class="blue">news</span>  Jan . 5 2019 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-7 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev4.jpg"> 
			         				    </div>
			         				</div>	
			         			</a>	
		         			</div>	
		         			<div class="col-12 col-md-5 spacer">
		         				<a href=""><h2>THANKSGIVING 2018 PHOTO GALLERY</h2></a>
		         				<p><span>acadamics</span>  dec. 1 2018 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to enjoy a special Thanksgiving...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link"><a href="">see all</a></div>
		         	</div>
		         </div>

	     	</div>

	     	<div class="eventpart myborder">
	     		<div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Events</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link"><a href="">see all</a></div>
		         	</div>	
		        </div>

		        <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-7 spacer">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne1.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">jun<br><strong>1</strong></span>
										</div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 col-md-5 spacer">
		         				<a href=""><h2>I CARE PREVENTION SUMMIT</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza... and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-7 spacer">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne2.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">Nov<br><strong>7</strong></span>
										</div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 col-md-5 spacer">
		         				<a href=""><h2>EARLY CHILDHOOD EXTRAVAGANZA</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link myborder"><a href="">see all</a></div>
		         	</div>
		         </div>
	     	</div>
		</section>

		<?php echo do_shortcode("[shortcode category='commonpages']");?>

		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
