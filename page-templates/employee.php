<?php
/**
 * Template Name: employee page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<label>Employees</label>
						<h3>The place for Employees Infromation</h3>						
						<p>See below for the main information you will need from the Communications department. </p>
					</div>	
				</div>		
		</section>

		<section class="parallaxpart threebox" style="background-image: url(<?php echo $path; ?>/img/banner2.jpg);">
			<div class="row justify-content-center">
				<div class="col-md-6 col-lg-4">
					<div class="greenbox">
						<h2> PAYROLL AND BENEFITS </h2>
						<hr>				    
						<p class="text-center">Find out more about accessing your employee payroll and benefits information.</p>
						<a href="" class="btn">Learn more</a>						   
						</div>    
					</div>	
					
					<div class="col-md-6 col-lg-4">
					<div class="greenbox">
						<h2> EMPLOYEE EMAIL </h2>
						<hr>				    
						<p class="text-center">Sign in and access your official employee email account. </p>
						<a href="" class="btn">Learn more</a>						   
						</div>    
					</div>

					<div class="col-md-6 col-lg-4">
					<div class="greenbox">
						<h2> SUBSTITUTE SYSTEM </h2>
						<hr>				    
						<p class="text-center">Login to the eSchool Solutions system for substitutes. </p>
						<a href="" class="btn">Learn more</a>						   
						</div>    
					</div>

					<div class="col-md-6 col-lg-4">
					<div class="greenbox">
						<h2> ERO LOGIN </h2>
						<hr>				    
						<p class="text-center">Click the button to login and access your employee electronic registrar online,</p>
						<a href="" class="btn">Read more</a>						   
						</div>    
					</div>

					<div class="col-md-6 col-lg-4">
					<div class="greenbox">
						<h2> RETIREE INFORMATION
 </h2>
						<hr>				    
						<p class="text-center">Find out the information you need for retiring.</p>
						<a href="" class="btn">Learn more</a>						   
						</div>    
					</div>

					<div class="col-md-6 col-lg-4">
					<div class="greenbox">
						<h2> APPLY FOR  GRANT </h2>
						<hr>				    
						<p class="text-center">Apply for a grant here.</p>
						<a href="" class="btn">Learn more</a>						   
						</div>    
					</div>

				</div>	
					
			</div>
		</section>

		<section class="inner-container communicationpart">	
			<div class="row">	
			    <div class="col-lg-8">
			    	<h2>EMPLOYEE CENTRAL</h2>	
			    	<p>East Baton Rouge Parish School System is an equal opportunity employer. EBRPSS does not discriminate on the basis of race, color, national origin, gender, age, or qualified disability.
			    	</p>

			    	<div class="yellowborderbox one">
			    		<div class="titlepart">
			    			<h2>IMPORTANT EMPLOYEE CONTACTS</h2>
			    		</div>	
			    		<div class="tabItem">	
			    			<div class="row">
				    			<div class="col-md-9">
				    				<a href="#">DESTINY</a>
									<p class="shortdetails">For assistance with the Destiny platform, click the button to email support.</p>
								</div>
								<div class="col-md-3 text-center">	
									<a href="" class=" btn">Email</a>
								</div>
							</div>
							<hr>
							<div class="row">
				    			<div class="col-md-9">
				    				<a href="#">EMPLOYEE EMAILS</a>
									<p class="shortdetails">For technical support with employee email accounts, call the help desk at 225-922-5524 for assistance.</p>
								</div>
								<div class="col-md-3 text-center">	
									<!-- <a href="" class=" btn">Email</a> -->
								</div>
							</div>
							<hr>
							<div class="row">
				    			<div class="col-md-9">
				    				<a href="#">eREGISTRATION (ERO)</a>
									<p class="shortdetails">For assistance with the ERO platform, click the button to email support.</p>
								</div>
								<div class="col-md-3 text-center">	
									<a href="" class=" btn">Email</a>
								</div>
							</div>
							<hr>
							<div class="row">
				    			<div class="col-md-9">
				    				<a href="#">NOVELL</a>
									<p class="shortdetails">For assistance with logging in to the Novell platform call the help desk at 225-922-5524 for assistance.</p>
								</div>
								<div class="col-md-3 text-center">	
									<!-- <a href="" class=" btn">Email</a> -->
								</div>
							</div>
							<hr>
							<div class="row">
				    			<div class="col-md-9">
				    				<a href="#">PAYROLL LOGIN</a>
									<p class="shortdetails">For assistance with logging in to  your Payroll information, call the help desk at 225-922-5524 for assistance.</p>
								</div>
								<div class="col-md-3 text-center">	
									<!-- <a href="" class=" btn">Email</a> -->
								</div>
							</div>
							<hr>
							<div class="row">
				    			<div class="col-md-9">
				    				<a href="#">SUBSTITUTE SYSTEM</a>
									<p class="shortdetails">For assistance with logging in to the substitute system, click the button below email support.</p>
								</div>
								<div class="col-md-3 text-center">	
									<a href="" class=" btn">Email</a>
								</div>
							</div>
							<hr>
							<div class="row">
				    			<div class="col-md-9">
				    				<a href="#">TAC (TEACHER ACCESS CENTER)</a>
									<p class="shortdetails">For assistance with logging in to the Teacher Access Center, click the button to email support.</p>
								</div>
								<div class="col-md-3 text-center">	
									<a href="" class=" btn">Email</a>
								</div>
							</div>
							<hr>
							<div class="row">
				    			<div class="col-md-9">
				    				<a href="#">TEACHER/SCHOOL WEBSITES</a>
									<p class="shortdetails">For assistance or questions about the district, school and teacher websites, click the button to email our webmaster.</p>
								</div>
								<div class="col-md-3 text-center">	
									<a href="" class=" btn">Email</a>
								</div>
							</div>
						</div>	
			    	</div>	

			    </div>
			    <div class="col-lg-4 sidebar-blue-section">
					<div class="blue-border-box one">
						<div class="ribbonpart"><label>Other important info</label></div>
						
						<label>Employee Resources:</label>
						<ul>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> District Master Calendar</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> TeachBR</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Job Opportunities</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Payroll login</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> PASS</a></li>
						</ul>	
						<hr>
						<label>Relevant Departments:</label>
						<ul>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Adult Education</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Human Resources</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Information Technology</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Instructional Technology</a></li>
						</ul>
						<hr>
						<label>You may also interested in:</label>
						<ul>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> All form</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> all policies</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Contact direstory</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> All departments</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> all programs</a></li>
						</ul>
					</div>
			    </div>	
			</div>    
		</section>

		<?php echo do_shortcode("[shortcode category='commonpages']");?>

		<section class="inner-container newspart">			
	         <div class="ss-style-triangles news">		   
	         	<h2>What is happening in EBr?</h2>	
	         </div>	   
	        <div class="newspart">
		        <div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Recent news</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link myborder"><a href="">see all</a></div>
		         	</div>		
		        </div>

		         <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev1.jpg"> 
			         				    </div>
		         					</div>	
		         			 	</a>
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>Biden Project Youth Leads</h2></a>
		         				<p><span>ACADEMICS</span>  DEC. 1 2018 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to enjoy a special Thanksgiving...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev2.jpg"> 
			         				    </div>
		         					</div>	
		         				</a>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>EBR School Board Recognitions at the March 21 Regular Meeting</h2></a>
		         				<p><span class="green">announcement</span>  jan. 5 2019 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev3.jpg"> 
			         				    </div>
		         					</div>	
		         				</a>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>JANUARY 2019 BOARD RECOGNITIONS</h2></a>
		         				<p><span class="blue">news</span>  Jan . 5 2019 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev4.jpg"> 
			         				    </div>
			         				</div>	
			         			</a>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>THANKSGIVING 2018 PHOTO GALLERY</h2></a>
		         				<p><span>acadamics</span>  dec. 1 2018 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to enjoy a special Thanksgiving...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link"><a href="">see all</a></div>
		         	</div>
		         </div>

	     	</div>

	     	<div class="eventpart myborder">
	     		<div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Events</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link"><a href="">see all</a></div>
		         	</div>	
		        </div>

		        <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne1.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">jun<br><strong>1</strong></span>
										</div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>I CARE PREVENTION SUMMIT</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza... and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne2.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">Nov<br><strong>7</strong></span>
										</div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>EARLY CHILDHOOD EXTRAVAGANZA</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link myborder"><a href="">see all</a></div>
		         	</div>
		         </div>
	     	</div>
		</section>


		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
