<?php
/**
 * Template Name: about us Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
<!-- 		<section class="overlay breadcrumbs " style="background-image: url(<?php echo $path; ?>/img/breadcrumb.jpg);">
	<div class="container-fluid">
		<div class="row">					
			<div class="col-md-12">
			 <ul>
			 	<li><a href="">About</a></li>
			 	<li><a href="">school board</a></li>
			 </ul>					
			</div>
		</div>
	</div>
</section> -->

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<label>school board</label>
						<h3>Board Members and Board Meeting Info</h3>
						<p>Board meetings are held at the School Board Office the third Thursday of each month at 5:00pm. All meetings are open to the public. Copies of “Citizen’s Guide to the Conduct of EBR School Board Meetings” are available from the receptionist in the lobby of the School Board Office.</p>
						<a href="" class="btn">see meeting agendas</a>
					</div>	
				</div>		
		</section>

		<section class="inner-container blue-section">
			<div class="blue-border-box" id="one">
				<div class="ribbonpart"><label>2019 board meeting dates</label></div>
				<p>All meetings are open to the public and begin at 5:00pm every Third Thursday of the month.</p>
				 <div class="row justify-content-center">
					<div class="col-md-4">
						<label>January</label>
						<h6>January 17, 2019</h6>
						<h6><strong>Special Date:</strong> January 18, 2019</h6>
					</div>	
					<div class="col-md-4">
						<label>February</label>
						<h6>February 21, 2019</h6>						
					</div>	
					<div class="col-md-4">
						<label>March</label>
						<h6>March 21, 2019</h6>
					</div>	
				</div>	
				<hr class="d-none d-md-block">
				<div class="row justify-content-center">
					<div class="col-md-4">
						<label>April</label>
						<h6>April 18, 2019</h6>						
					</div>	
					<div class="col-md-4">
						<label>May</label>
						<h6>May 16, 2019</h6>						
					</div>	
					<div class="col-md-4">
						<label>June</label>
						<h6>June 20, 2019</h6>
						<h6><strong>Special Date:</strong> June 21, 2019</h6>
					</div>	
				</div>	
			</div>
		</section>
		
		<section class="yellowpart">
			<div class="content-conainer">
				<div class="row justify-content-center">
					<div class="col-md-6">
						<div class="darkshade">
						<h2 class="no-background">
							<span>Board docs</span>					
					    </h2>
					    <a href="">view</a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="darkshade">
						<h2 class="no-background">
							<span>District map</span>					
					    </h2>
					    <a href="">view</a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="darkshade">
						<h2 class="no-background">
							<span>VIDEOS & TV SCHEDULE</span>					
					    </h2>
					    <a href="">view</a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="darkshade">
						<h2 class="no-background">
							<span>Archives</span>					
					    </h2>
					    <a href="">view</a>
						</div>
					</div>
			</div>
		</section>

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<label>MEET THE EAST BATON ROUGE PARISH SCHOOL BOARD</label>						
						<p class="subtitle">Get to know the current members of the East Baton Rouge Parish School Board.</p>
					</div>	
				</div>		
		</section>

		<section class="inner-container">
			<div class="row justify-content-center">
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<img src="<?php echo $path; ?>/img/board-member.png">
						<div class="ribbonpart"><label>MARK BELLUE</label></div>
						<span>District 1</span>
						<a href="#">mbellue@ebrschools.org</a>
						<a href="#detail1" class="btn fancybox">meet them</a>
						<div id="detail1" style="display:none">
							<?php echo do_shortcode("[contact-form-7 id='92' title='Meet them form']");?>
						</div>
					</div>	
				</div>
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<img src="<?php echo $path; ?>/img/board-member2.png">
						<div class="ribbonpart"><label>DADRIUS LANUS</label></div>
						<span>District 2</span>
						<a href="#">dlanus@ebrschools.org</a>
						<a href="#detail2" class="btn fancybox">meet them</a>
						<div id="detail2" style="display:none">
							<div class="team-container">
							 <div class="row">
							 	<div class="col-md-4">
							 		<img src="<?php echo $path; ?>/img/board-member2.png">
							 	</div>
							 	<div class="col-md-8">	
							 		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore atque optio, praesentium itaque dolorem distinctio magnam, cum voluptatum nemo at ex perspiciatis saepe sequi omnis aliquid eligendi, voluptatem ut impedit.</p>
							 	</div>	
							 	<div class="col-md-12">
							 		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, doloremque libero neque molestias odit atque, praesentium necessitatibus labore, iure non facere! Eos obcaecati, cum fugiat deleniti ab saepe quibusdam dolorem?
							 	</div>
							 </div>	
							</div>
						</div>
					</div>	
				</div>
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<img src="<?php echo $path; ?>/img/board-member3.png">
						<div class="ribbonpart"><label>TRAMELLE HOWARD</label></div>
						<span>District 3</span>
						<a href="#">thoward@ebrschools.org</a>
						<a href="#" class="btn">meet them</a>
					</div>	
				</div>	
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<img src="<?php echo $path; ?>/img/board-member.png">
						<div class="ribbonpart"><label>MARK BELLUE</label></div>
						<span>District 1</span>
						<a href="#">mbellue@ebrschools.org</a>
						<a href="#" class="btn">meet them</a>
					</div>	
				</div>
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<img src="<?php echo $path; ?>/img/board-member.png">
						<div class="ribbonpart"><label>MARK BELLUE</label></div>
						<span>District 1</span>
						<a href="#">mbellue@ebrschools.org</a>
						<a href="#" class="btn">meet them</a>
					</div>	
				</div>
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<img src="<?php echo $path; ?>/img/board-member.png">
						<div class="ribbonpart"><label>MARK BELLUE</label></div>
						<span>District 1</span>
						<a href="#">mbellue@ebrschools.org</a>
						<a href="#" class="btn">meet them</a>
					</div>	
				</div>
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<img src="<?php echo $path; ?>/img/board-member.png">
						<div class="ribbonpart"><label>MARK BELLUE</label></div>
						<span>District 1</span>
						<a href="#">mbellue@ebrschools.org</a>
						<a href="#" class="btn">meet them</a>
					</div>	
				</div>
			</div>	
		</section>

<!-- <div class="menu">
	<ul>
		<li><a href="">Menu 1</a></li>
		<li><a href="">Menu 2</a></li>
		<li ><a href="">Menu 3</a></li>
		<li><a href="">Menu 4</a></li>
		<li class="active"><a href="">Menu 5</a></li>
	</ul>
</div> -->

		

		<section class="selectionpart">
			<div class="title">learn more about our:</div>			
		<div class="bottom_menu">
			<ul>
				<li><a href="">Leadership</a></li>
				<li><a href="">Story</a></li>
				<li class="active"><a href="">board</a></li>
				<li><a href="">policies</a></li>
				<li><a href="">communication</a></li>
			</ul>
		</div>
		</section>



		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
