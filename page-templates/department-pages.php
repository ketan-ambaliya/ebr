<?php
/**
 * Template Name: Department Pages
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>
<div class="wrapper" id="full-width-page-wrapper">
<?php if( have_rows('flexible_content_sections') ): 
		while ( have_rows('flexible_content_sections') ) : the_row();

		if( get_row_layout() == 'parallax_green_boxes' ): 
			$background_image = get_sub_field('background_image'); ?>
		
		<section class="parallaxpart threebox" style="background-image: url(<?php echo $background_image['url']?>);"> 	<div class="inner-container">
			<div class="container-fluid no-padding">
			<div class="row justify-content-center display-flex">
				<?php if( have_rows('boxs') ): 
				while ( have_rows('boxs') ) : the_row();
				$box_title = get_sub_field('box_title'); 
				$box_description = get_sub_field('box_description'); 
				$button_text = get_sub_field('button_text'); 
				$button_url = get_sub_field('button_url'); ?>
				<div class="col-md-6 col-lg-4">
					<div class="greenbox">
						<?php if( !empty($box_title)):?><h2> <?php echo $box_title; ?> </h2><?php endif;?>
						<hr>				    
						<?php if( !empty($box_description)):?><p class="text-center"><?php echo $box_description; ?></p><?php endif;?>
						<?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>					   
					</div>    
				</div>	
				<?php endwhile; endif;?>	
			</div>		
			</div>
		</div>
		</section>

		<?php elseif( get_row_layout() == 'department_content'):?>

		<section class="inner-container communicationpart">	
			<div class="row">
			<?php $content_title = get_sub_field('content_title');
		    			  $content_or_accordion = get_sub_field('content_or_accordion');
		    			  $content_box = get_sub_field('content_box');?>

		    	<?php if($content_or_accordion == "Content Section"){?>

			   		<div class="col-lg-8">	
		    		<?php if( !empty($content_title)):?><h2><?php echo $content_title; ?></h2><?php endif;?>
		    	
			    		<?php if( !empty($content_box)):?><?php echo $content_box; ?><?php endif;?>
			    	</div>

			    <?php } elseif($content_or_accordion == "Content With Magazine"){?>
			    	<div class="col-lg-5">
			    		<?php $m_content_box = get_sub_field('m_content_box');?>
			    	<?php if( !empty($content_title)):?><h2><?php echo $content_title; ?></h2><?php endif;?>	
			    	<?php if( !empty($m_content_box)):?><?php echo $m_content_box; ?><?php endif;?>

			   		</div>	
			    	<div class="col-lg-3 text-center">
			    	
			    	 <?php $magazine_image = get_sub_field('magazine_image');
			    	  $download_button_text = get_sub_field('download_button_text');
			    	  $download_button_url = get_sub_field('download_button_url');?>
			    	<?php if( !empty($magazine_image)):?><img src="<?php echo $magazine_image['url']; ?>"><?php endif;?>
			    	<?php if( !empty($download_button_url)):?><a href="<?php echo $download_button_url; ?>" class="btn" target="_blank"><?php echo $download_button_text; ?></a><?php endif;?>
			    	</div>
	    			<?php } else {?>
	    				<div class="col-lg-8">
	    					<?php if( !empty($content_title)):?><h2><?php echo $content_title; ?></h2><?php endif;?>
	    				  <div class="accordianpart">
		    			  <div id="accordion" class="accordion schools-accordion">
							<div class="card mb-0">
								<?php if( have_rows('accordion_section') ): $i=1;
								while ( have_rows('accordion_section') ) : the_row();
									$title_icon = get_sub_field('title_icon');
		    			 			$accordion_title = get_sub_field('accordion_title');
		    			  			$accordion_content = get_sub_field('accordion_content');?>
									<div class="card-header collapsed" data-toggle="collapse" href="#collapse<?php echo $i;?>">
										<div class="svgsection"><img src="<?php echo $title_icon['url'];?>" alt="<?php echo $title_icon['alt'];?>" class="svg" id="logo"/></div>
										<a class="card-title">
											<?php echo $accordion_title;?>
										</a>
									</div>
									<div id="collapse<?php echo $i;?>" class="card-body collapse" data-parent="#accordion">
										<div class="card-body-panel">
											<div class="row justify-content-center">
												<div class="col-md-12">
													<?php echo $accordion_content;?>
												</div>
											</div>
											
										</div>
									</div>
								<?php $i++; endwhile; endif;?>
							</div>
						  </div>
						  </div>
						  </div>	
	    			<?php } ?>
			    
			   
			    <div class="col-lg-4 sidebar-blue-section">
					<div class="blue-border-box one">
						<?php $sidebar_title = get_sub_field('sidebar_title');

						if( !empty($sidebar_title)):?><div class="ribbonpart"><label><?php echo $sidebar_title; ?></label></div><?php endif;?>
						
						<label>Contact:</label>
						<?php $p_category_id = get_sub_field('department_contact_category');
			    			  $terms = get_term($p_category_id); 
					     	  $p_category_name = $terms->name;

			       			  $args = array( 'post_type' => 'department-contacts', 'posts_per_page' => -1, 'category_name' => $p_category_name, 'orderby' => 'DESC');

			        		  $loop = new WP_Query( $args );
			        		  if ( $loop->have_posts() ) {
			       			  while ( $loop->have_posts() ) : $loop->the_post();
			       			  $name = get_field('name');
			       			  $designation = get_field('designation');
			       			  $fax = get_field('fax');
			       			  $address = get_field('address');
			       			  $address2 = get_field('address_2');
			       			  $map_link = get_field('map_link');
			       			  $dispatch_hours = get_field('dispatch_hours');
			       			  ?>
			       		<?php if( !empty($name) || !empty($designation) || !empty($address)):?>
					    <p><span><?php if( !empty($name)):?><?php echo $name;?>,<?php endif;?></span> <span><?php echo $designation;?><?php if( !empty($designation)):?>,<?php endif;?></span>
						<span><?php echo $address;?></span>
						<span><?php echo $address2;?></span></p>
						<?php endif;?>
						<div class="view">
							
							<?php if(  !empty($address)):?><a href="https://www.google.com/maps/place/<?php echo $address; ?><?php echo $address2;?>" target="_blank" class="schoolviewmap"><?php _e('[view map]','roots'); ?></a>	<?php endif;?>							
								<div class="row contact">
									<div class="col-md-12 col-lg-12 col-xl-6 no-padding">
										<?php if( have_rows('phone_number') ): $i=1;
										while ( have_rows('phone_number') ) : the_row();
										 $phone = get_sub_field('phone'); ?>

										<?php if( !empty($phone)):?>
											<a href="tel:<?php echo $phone;?>">
												<?php if($i == 1){?><strong>Ph:</strong><?php } ?>
												<?php if($i != 1){?>or<?php } ?>
												<?php echo $phone;?></a>   
										<?php endif;?>
		

										<?php $i++; endwhile; endif;?>
									</div>
									<div class="col-md-12 col-lg-12 col-xl-6 no-padding">
										<?php if( !empty($fax)):?><a><strong>fax:</strong> <?php echo $fax;?></a>  <?php endif;?>
									</div>										
								</div>												
						</div>
		         		<?php if( !empty($dispatch_hours)):?><p class="dispatch"><strong>Dispatch Hours:</strong> <?php echo $dispatch_hours;?></p><?php endif;?>
			   			<?php  wp_reset_query(); endwhile; 
			   		} else {
						echo "No Contact Found";
					}?>
						
						<hr>
						<?php $quicklinks_title = get_sub_field('quicklinks_title');?>
						<?php if( !empty($quicklinks_title)):?><label> <?php echo $quicklinks_title; ?> </label><?php endif;?>
						<ul>
							<?php if( have_rows('links') ):
								while ( have_rows('links') ) : the_row();
								$link_text = get_sub_field('link_text');
								$link_url = get_sub_field('link_url');?>
								<?php if( !empty($link_url)):?><li><a class="f_links" href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a></li><?php endif;?>
							<?php endwhile; endif;?>
						</ul>
						<?php $show_you_may_also_interested_section = get_sub_field('show_you_may_also_interested_section');

						if($show_you_may_also_interested_section == 'Yes')	{?>	
							<hr>
							<?php $interested_in_title = get_field('interested_in_title', 'option');?>
							<?php if( !empty($interested_in_title)):?><label> <?php echo $interested_in_title; ?> </label><?php endif;?>
							<ul>
								<?php if( have_rows('interested_in_links', 'option') ):
									while ( have_rows('interested_in_links', 'option') ) : the_row();
									$link_text = get_sub_field('link_text', 'option');
									$link_url = get_sub_field('link_url', 'option');?>
								<?php if( !empty($link_url)):?><li><a class="f_links" href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a></li><?php endif;?>
								<?php endwhile; endif;?>
							</ul>
						<?php }?>
					</div>
			    </div>	
			</div>    
		</section>
		
		<?php elseif( get_row_layout() == 'news_events'):?>

		<section class="inner-container newspart">	
		<?php $section_title = get_sub_field('section_title');?>	
	         <?php if( !empty($section_title)):?>
		         <div class="ss-style-triangles news">		   
		         	<h2><?php echo $section_title; ?> </h2>	
		         </div>	 
	         <?php endif;?>	
	         <?php $p_category_id1 = get_sub_field('select_news_category');
	         $terms = get_term($p_category_id1); 
			 $p_category_name1 = $terms->name; 

			 $query = new WP_Query(array('post_type' => 'news','category_name' => $p_category_name1));
				if( $query->have_posts() ){
				      $p_category_name1 = $terms->name; 
				} else {
				     $p_category_name1 = ''; 
				}?> 

	         <?php if( $p_category_name1 != 'Uncategorized') {?>
	        <div class="newspart">
		        <div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Recent news</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link myborder"><a href="<?php echo esc_url( home_url( '/' ) ); ?>news">see all</a></div>
		         	</div>		
		        </div>

		         <div class="row">
         			<?php 			     	  
	       			  $args = array( 'post_type' => 'news', 'posts_per_page' => 4, 'category_name' => $p_category_name1, 'orderby' => 'DESC');

	        		  $loop = new WP_Query( $args );
	        		  if ( $loop->have_posts() ) {
	       			  while ( $loop->have_posts() ) : $loop->the_post();?>
	       		
						<div class="col-12 col-md-12 col-lg-12 col-xl-6">
		         		<div class="row">
		         			<div class="col-12 col-md-8 spacer">
		         				<a href="<?php the_permalink();?>">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         						<?php	if ( has_post_thumbnail() ) {  
								                        	the_post_thumbnail();  
								          		}elseif( get_theme_mod( 'news_default_image' ) ){
								          						 
					          						echo '<img src="' .get_theme_mod( 'news_default_image' ).'" />';
													} else {

					          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
								          					
								          			}
								          ?>
			         				    </div>
		         					</div>	
		         			 	</a>
		         			</div>	
		         			<div class="col-12 col-md-4 spacer">
		         				<a href="<?php the_permalink();?>"><h2><?php the_title();?></h2></a>
		         				<?php $tags =  get_the_tags();?>
		         				<p><span style="color: <?php the_field('tag_color',  'term_'.$tags[0]->term_id); ?>;"><?php  echo $tags[0]->name; ?></span>  <?php echo get_the_date('M. j Y');?></p>
		         				<p class="sortcontent">
		         					<?php $content = get_the_content();
            						$content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,116); ?>...
		         				</p>	
		         				<a href="<?php the_permalink();?>" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         		</div>	

			   		<?php  wp_reset_query(); endwhile; 
			   		} else {
						echo "<div class='col-12'>No News Found</div>";
					}?>

		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link"><a href="<?php echo esc_url( home_url( '/' ) ); ?>news">see all</a></div>
		         	</div>
		         </div>

	     	</div>
	    	 <?php } ?>
	    	 <?php $p_category_id2 = get_sub_field('select_events_category');
	         $terms = get_term($p_category_id2); 
			 $p_category_name2 = $terms->name;
			  $query = new WP_Query(array('post_type' => 'tribe_events','category_name' => $p_category_name2));
				if( $query->have_posts() ){
				      $p_category_name2 = $terms->name; 
				} else {
				     $p_category_name2 = ''; 
				}?> 

	         <?php if($p_category_name2 != 'Uncategorized') {?>

	     	<div class="eventpart myborder">
	     		<div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Events</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link"><a href="<?php echo esc_url( home_url( '/' ) ); ?>events">see all</a></div>
		         	</div>	
		        </div>

		        <div class="row">
		        	<?php 			     	  
	       			  $args = array( 'post_type' => 'tribe_events', 'posts_per_page' => 2, 'category_name' => $p_category_name2, 'orderby' => 'DESC');

	        		  $loop = new WP_Query( $args );
	        		  if ( $loop->have_posts() ) {
	       			  while ( $loop->have_posts() ) : $loop->the_post();?>
			         	<div class="col-12 col-md-12 col-lg-12 col-xl-6">
			         		<div class="row">
			         			<div class="col-12 col-md-8 spacer">
			         				<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					 <?php	if ( has_post_thumbnail() ) {  
								                        	the_post_thumbnail();  
								          		}elseif( get_theme_mod( 'event_default_image' ) ){
								          						 
					          						echo '<img src="' .get_theme_mod( 'event_default_image' ).'" />';
													} else {

					          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
								          					
								          			}
								          ?>  
			         					  <div class="ribbon">
											  <span class="ribbon2">
											  	<?php echo tribe_get_start_date( $post, false, 'M' );?> <br><strong><?php echo tribe_get_start_date( $event, false, 'j' );?> </strong></span>
											</div>
			         				    </div>
			         				</div>	
			         			</div>	
			         			<div class="col-12 col-md-4 spacer">
			         				<a href="<?php the_permalink();?>"><h2><?php the_title();?></h2></a>		        				
			         				<p class="sortcontent">
			         					<?php $content = get_the_content();
	                    						$content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,130); ?>...
			         				</p>	
			         				<a href="<?php the_permalink();?>" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
			         			</div>	
			         		</div>
			         	</div>

			   		<?php  wp_reset_query(); endwhile; 
			   		} else {
						echo "<div class='col-12'>No Events Found</div>";
					}?>
		         			
		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link myborder"><a href="<?php echo esc_url( home_url( '/' ) ); ?>events">see all</a></div>
		         	</div>
		         </div>
	     	</div>
	     	<?php } ?>
		</section>

		<?php elseif( get_row_layout() == 'our_mission'):?>

		<section class="inner-container bottom-dark-border-part">						
				<div class="blue-border-box one">
					<?php $title = get_sub_field('title');
		    			  $description = get_sub_field('description');?>
					<?php if( !empty($title)):?><div class="ribbonpart"><label><?php echo $title; ?></label></div><?php endif;?>
					<?php if( !empty($description)):?><p class="text-center"><?php echo $description; ?></p><?php endif;?>
				</div>
		</section>	

		<?php endif; ?>
<?php 
    endwhile;
else :
   echo 'No Row Found';
endif;
?>
		<div class="clerfix"></div>
	</div><!-- #content -->
</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
