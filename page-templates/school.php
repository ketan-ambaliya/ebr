<?php
/**
 * Template Name: School Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
$url = home_url();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
<!-- 		<section class="overlay breadcrumbs " style="background-image: url(<?php echo $path; ?>/img/breadcrumb.jpg);">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
			 <ul>
			 	<li><a href="">schools</a></li>
			 </ul>
			</div>
		</div>
	</div>
</section> -->

		<section class="inner-container paragraphpart">
				<div class="row">
					<div class="col-md-12 text-center">
						<img src="<?php echo $path; ?>/img/sducation-cap-gren.png" />
						<label>schools</label>
						<p>Search our interactive map or select a category from the drop downs below. </p>
					</div>
				</div>
		</section>

		<section class="inner-container blue-section">
			<div class="blue-border-box" id="one">
				<div class="ribbonpart"><label>search our interactive map</label></div>
				 <div class="row justify-content-center">
					<div class="col-md-12 text-center">
						<h2>Coming Soon</h2>
					</div>
				</div>
			</div>
		</section>


		<section class="inner-container accordianpart">
			<div id="accordion" class="accordion schools-accordion">
				<div class="card mb-0">
					<!-- accordion 1 -->
					<div class="card-header collapsed" data-toggle="collapse" href="#collapseOne">
						<div class="svgsection">
							<img src="<?php echo $path; ?>/img/abc-block.svg" class="svg" id="logo" />
						</div>
						<a class="card-title">
							early childhood schools
						</a>
					</div>
					<div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
						<div class="card-body-panel">
							<div class="row justify-content-center">
								<div class="col-md-12">
									<h4>SIXTH - EIGHTH Grade</h4>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> <a href="tel:225.922.5400">225.272.0540</a>   <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="<?php echo $url; ?>/school-details" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> <a href="tel:225.922.5400">225.272.0540</a>   <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="<?php echo $url; ?>/school-details" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> <a href="tel:225.922.5400">225.272.0540</a>   <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="<?php echo $url; ?>/school-details" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> <a href="tel:225.922.5400">225.272.0540</a>   <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="<?php echo $url; ?>/school-details" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- End accordion 1 -->
					<!-- accordion 2 -->

					<div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
						<div class="svgsection">
							<img src="<?php echo $path; ?>/img/board-1.svg" class="svg" id="logo" />
						</div>
						<a class="card-title">elementary schools</a>
					</div>
					<div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
						<div class="card-body-panel">
							<div class="row justify-content-center">
								<div class="col-md-12">
									<h4>SIXTH - EIGHTH Grade</h4>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--end accordion 2 -->

					<!--accordion 3 -->
					<div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
						<div class="svgsection">
							<img src="<?php echo $path; ?>/img/book.svg" class="svg" id="logo" />
						</div>
						<a class="card-title">middle schools</a>
					</div>
					<div id="collapseThree" class="card-body collapse" data-parent="#accordion" >
						<div class="card-body-panel">
							<div class="row justify-content-center">
								<div class="col-md-12">
									<h4>SIXTH - EIGHTH Grade</h4>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--end accordion 3 -->

					<!--accordion 4 -->
					<div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
						<div class="svgsection">
							<img src="<?php echo $path; ?>/img/education-cap.svg" class="svg" id="logo" />
						</div>
						<a class="card-title">HIGH SCHOOLS</a>
					</div>
					<div id="collapseFour" class="card-body collapse" data-parent="#accordion" >
						<div class="card-body-panel">
							<div class="row justify-content-center">
								<div class="col-md-12">
									<h4>SIXTH - EIGHTH Grade</h4>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--End accordion 4 -->
					<!--accordion 5 -->
					<div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
						<div class="svgsection">
							<img src="<?php echo $path; ?>/img/icon1.svg" class="svg" id="logo" />
						</div>
						<a class="card-title">EARLY CHILDHOOD PROGRAM FORMS</a>
					</div>
					<div id="collapseFive" class="card-body collapse" data-parent="#accordion" >
						<div class="card-body-panel">
							<div class="row justify-content-center">
								<div class="col-md-12">
									<h4>SIXTH - EIGHTH Grade</h4>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
										<div class="row align-items-end">
											<div class="col-md-9">
												<p class="address">
													1225 Sharp Road. Baton Rouge, LA 70815
												</p>

												<p>
													<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
												</p>
											</div>
											<div class="col-md-3">
												<a href="#" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--end accordion 5 -->
				</div>
				</div>
		</section>

		<section class="bluecontainer map-section">
			<div class="inner-container">
				<div class="row">
					<div class="col-md-12 text-center">
						<h2>WANT TO SEARCH ANOTHER WAY?</h2>
						<p>Click below to find your EBRPSS attendance zone and bus route. </p>
					</div>
				</div>
				<div class="row align-items-center justify-content-center">
					<div class="col-md-3 text-center no-padding">

						<h4>LOCATE MY ATTENDANCE ZONE</h4>
						<a href="#" class="btn">Find My Zone</a>

					</div>
					<div class="col-md-6 text-center no-padding">
						<img src="<?php echo $path; ?>/img/map.png" />
					</div>
					<div class="col-md-3 text-center">
						<h4>TRACK <br>MY BUS</h4>
						<a href="#" class="btn">DOWNLOAD APP</a>
					</div>
				</div>
			</div>
		</section>

		<section class="did-you-konw-section">	
		<div class="inner-container">	
				<div class="row">
					<div class="col-md-8">
						<h2 class="no-background"><span>DID YOU KNOW?</span></h2>
						<h3>Positive Social Interaction</h3>
						<p>
							The East Baton Rouge Parish School System is the SECOND LARGEST school district in the state of Louisiana and is nationally recognized year-round for its achievements and accomplishments.
						</p>
						<a href="#" class="btn">learn more</a>
					</div>
					<div class="col-md-4">
						<div class="image-thum-block">
							<img src="<?php echo $path; ?>/img/ev1.jpg" alt="" class="primary-image">
							<img src="<?php echo $path; ?>/img/ev3.jpg" alt="" class="secondary-image">
						</div>
					</div>
				</div>
				</div>			
		</section>

		<section class="did-you-konw-section blue_bg" style="background-color: #002e59;">	
		<div class="inner-container">		
				<div class="row">
					<div class="col-md-8">
						<h2 class="no-background"><span>DID YOU KNOW?</span></h2>
						<h3>Positive Social Interaction</h3>
						<p>
							The East Baton Rouge Parish School System is the SECOND LARGEST school district in the state of Louisiana and is nationally recognized year-round for its achievements and accomplishments.
						</p>
						<a href="#" class="btn">learn more</a>
					</div>
					<div class="col-md-4">
						<div class="image-thum-block">
							<img src="<?php echo $path; ?>/img/ev1.jpg" alt="" class="primary-image">
							<img src="<?php echo $path; ?>/img/ev3.jpg" alt="" class="secondary-image">
						</div>
					</div>
				</div>	
				</div>		
		</section>

		<section class="parallaxpart" style="background-image: url(<?php echo $path; ?>/img/banner2.jpg);">
			<div class="ribbonpart"><label>Join Our EBR family by...</label></div>
			<div class="inner-container">
				<div class="row justify-content-center display-flex">
					<div class="col-md-6">
						<div class="greenbox">
							<h2 class="no-background"> <span>VOLUNTEERING </span></h2>
							<div class="row">
								<div class="col-lg-8">
									<p>Join the family of elite students at EBRPRSS</p>
								</div>
								<div class="col-lg-4 d-flex align-items-end">
									<a href="" class="btn rightside">enroll now</a>
								</div>	
							</div>					   
						</div>    
					</div>	
					
					<div class="col-md-6">
						<div class="greenbox">
							<h2 class="no-background"> <span>SUPPORTING </span></h2>
							<div class="row">
								<div class="col-lg-8">
									<p>Being your career in the second largest school district in the state.</p>
								</div>
								<div class="col-lg-4 d-flex align-items-end">
									<a href="" class="btn rightside">Apply now</a>
								</div>
							</div>								   
						</div>    
					</div>
				</div>	
			</div>			
		</section>

		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php

						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->



	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
