<?php
/**
 * Template Name: Contact directory
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<img src="<?php echo $path; ?>/img/open-book.png" />
						<label>Contact Directory</label>
						<h3>Reach Out</h3>	
						<p>See below for the main information you will need from the Communications department. </p>
						<strong>225-922-5400</strong>
						<div class="text-center">
							<ul class="social">
						    	<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						    	<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						    	<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
						    	<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					    	</ul>
						</div>
					</div>	
				</div>		
		</section>

		

		<section class="inner-container communicationpart">	
			<div class="row">	
			    <div class="col-md-12">
			    	<div class="yellowborderbox one">
			    		<div class="titlepart">
			    			<h2>IMPORTANT Address and phone numbers</h2>
			    		</div>	
			    		<div class="directorypart">
			    			<div class="row justify-content-center">
			    				<div class="col-md-4">
			    					<label>504 coordinator</label>
			    					<p>1225 sharp road, Baton Rouge, La 70815</p>
			    					<p><strong>PH:</strong> 225.720.0540 <strong>Fax:</strong> 225.272.0195</p>

			    					<label>Accountability and assessment</label>
			    					<p>1225 sharp road, Baton Rouge, La 70815</p>
			    					<p><strong>PH:</strong> 225.720.0540 <strong>Fax:</strong> 225.272.0195</p>

			    					<label>Adult & Continuing Education</label>
			    					<p>1225 sharp road, Baton Rouge, La 70815</p>
			    					<p><strong>PH:</strong> 225.720.0540 <strong>Fax:</strong> 225.272.0195</p>

			    					<label>Business operations</label>
			    					<p>1225 sharp road, Baton Rouge, La 70815</p>
			    					<p><strong>PH:</strong> 225.720.0540 <strong>Fax:</strong> 225.272.0195</p>
			    				</div>
			    				<div class="col-md-4">
			    					<label>career & Technical Education</label>
			    					<p>1225 sharp road, Baton Rouge, La 70815</p>
			    					<p><strong>PH:</strong> 225.720.0540 <strong>Fax:</strong> 225.272.0195</p>

			    					<label>Elementry school program</label>
			    					<p>1225 sharp road, Baton Rouge, La 70815</p>
			    					<p><strong>PH:</strong> 225.720.0540 <strong>Fax:</strong> 225.272.0195</p>

			    					<label>Fedral program</label>
			    					<p>1225 sharp road, Baton Rouge, La 70815</p>
			    					<p><strong>PH:</strong> 225.720.0540 <strong>Fax:</strong> 225.272.0195</p>

			    					<label>gifter & talented programs</label>
			    					<p>1225 sharp road, Baton Rouge, La 70815</p>
			    					<p><strong>PH:</strong> 225.720.0540 <strong>Fax:</strong> 225.272.0195</p>
			    				</div>
			    				<div class="col-md-4">
			    					<label>health centers in school</label>
			    					<p>1225 sharp road, Baton Rouge, La 70815</p>
			    					<p><strong>PH:</strong> 225.720.0540 <strong>Fax:</strong> 225.272.0195</p>

			    					<label>I care</label>
			    					<p>1225 sharp road, Baton Rouge, La 70815</p>
			    					<p><strong>PH:</strong> 225.720.0540 <strong>Fax:</strong> 225.272.0195</p>

			    					<label>Jrotc</label>
			    					<p>1225 sharp road, Baton Rouge, La 70815</p>
			    					<p><strong>PH:</strong> 225.720.0540 <strong>Fax:</strong> 225.272.0195</p>

			    					<label>Magnet programs</label>
			    					<p>1225 sharp road, Baton Rouge, La 70815</p>
			    					<p><strong>PH:</strong> 225.720.0540 <strong>Fax:</strong> 225.272.0195</p>
			    				</div>	
			    				
			    			</div>	
			    		</div>
			    	</div>	
			    </div>			   
			</div>    
		</section>
	
		<section class="gmap text-center" style="background-image: url(<?php echo $path; ?>/img/contact-map.jpg);">
			<div class="ribbonpart"><label>Central administrative office</label></div>
		</section>	
		
		<section class="parallaxpart" style="background-image: url(<?php echo $path; ?>/img/banner2.jpg);">
			<div class="row justify-content-center">
					<div class="col-md-6">
						<div class="greenbox">
							<h2 class="no-background"> <span>VOLUNTEERING </span></h2>
							<div class="row">
								<div class="col-lg-8">
									<p>Join the family of elite students at EBRPRSS</p>
								</div>
								<div class="col-lg-4 d-flex align-items-end">
									<a href="" class="btn rightside">enroll now</a>
								</div>	
							</div>					   
						</div>    
					</div>	
					
					<div class="col-md-6">
						<div class="greenbox">
							<h2 class="no-background"> <span>SUPPORTING </span></h2>
							<div class="row">
								<div class="col-lg-8">
									<p>Being your career in the second largest school district in the state.</p>
								</div>
								<div class="col-lg-4 d-flex align-items-end">
									<a href="" class="btn rightside">Apply now</a>
								</div>
							</div>								   
						</div>    
					</div>
				</div>	
					
			</div>
		</section>
		
		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
