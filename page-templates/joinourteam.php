<?php
/**
 * Template Name: Join our Team
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		<section class="overlay breadcrumbs " style="background-image: url(<?php echo $path; ?>/img/breadcrumb.jpg);">
			<div class="container-fluid">
				<div class="row">					
					<div class="col-md-12">
					 <ul>					 	
					 	<li><a href="">Join our team</a></li>
					 </ul>					
					</div>
				</div>
			</div>
		</section>

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<label>Join our team</label>
						<h3>Become a Part of the Community</h3>
						<p>To view the current job openings available, select the category you are interested in below.</p>
					</div>	
				</div>		
		</section>

		<section class="inner-container join-our-team">	
			<div class="row">	
			    <div class="col-lg-8">
			    	<ul class="linkpart">
			    		<li><a href="#"> VIEW ALL OPEN POSITIONS <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> ADMINISTRATION <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> ATHLETICS <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> CAREER & TECHNICAL EDUCATION <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> CHILD NUTRITION <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> ELEMENTARY SCHOOL TEACHING <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> EXCEPTIONAL STUDENT SERVICES <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> HIGH SCHOOL TEACHING <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> MATH FOR PROFESSIONALS PROGRAM <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> MIDDLE SCHOOL TEACHING <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> PARAPROFESSIONALS <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> SECRETARIAL/CLERICAL <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> TRANSPORTATION <span class="link-arrow"></span></a></li>
			    	</ul>
			    <section class="ribbontitle">
					 <div class="row">
				         	<div class="col-12 text-center">
				         		<label>learn more about ebr paris</label>
				         	</div>		         		
				        </div>
				        <iframe width="100%" height="500px" src="https://www.youtube.com/embed/SqGlbqfTzyk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</section>		
			    </div>	
			    <div class="col-lg-4 sidebar-blue-section">
					<div class="blue-border-box one">
						<div class="ribbonpart"><label>INTERESTING FACTS</label></div>
						<h3>Fun Facts About Our Parish Schools</h3>
						<h2>Did  you know?</h2>
						<div id="microsoftcounter">
							<div class="iconpart">
								<img src="<?php echo $path; ?>/img/board-green.png">
							</div>	
							<div class="counterpart">
					        	<div class="count" data-count="41000">0</div><div class="extention">+</div>
					        <label>Students</label>
					        </div>

					        <div class="iconpart">
								<img src="<?php echo $path; ?>/img/tag-green.png">
							</div>	
							<div class="counterpart">
					        	<div class="count">2nd</div>
					        <label>Largest Public School System in the state</label>
					        </div>
					        <div class="iconpart">
								<img src="<?php echo $path; ?>/img/family-green.png">
							</div>	
							<div class="counterpart">
					        	<div class="count" data-count="58000">0</div><div class="extention">+</div>
					        <label>Full time emplyees</label>
					        </div>

						</div>	
					</div>
			    </div>	
			</div>    
		</section>
		
		<?php echo do_shortcode("[shortcode category='join our team']");?> <!-- image size must be 825px x 550px -->
		
	
		<section class="selectionpart">
			<div class="title">Other popular pages</div>			
		<div class="bottom_menu">
			<ul>
				<li class="active"><a href=""><i class="fa fa-book" aria-hidden="true"></i> Join our team</a></li>
				<li><a href=""><i class="fa fa-users" aria-hidden="true"></i> Get involved</a></li>
				<li><a href=""><i class="fa fa-graduation-cap" aria-hidden="true"></i> Enroll now</a></li>
				<li><a href=""><i class="fa fa-bus" aria-hidden="true"></i> Transportation</a></li>
			</ul>
		</div>
		</section>

		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
