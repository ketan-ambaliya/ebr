<?php
/**
 * Template Name: events page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		<!-- <section class="overlay breadcrumbs " style="background-image: url(<?php echo $path; ?>/img/breadcrumb-ev.jpg);">
			<div class="container-fluid">
				<div class="row">					
					<div class="col-md-12">
					 <ul>
					 	<li><a href="">Events</a></li>
					 </ul>						
					</div>
				</div>
			</div>
		</section> -->

	
		

		<section class="inner-container news_content">	
			<div class="row">	
			    <div class="col-lg-8">
			    	
			    <div class="eventpart"> 
		         		<div class="row">
		         			<div class="col-12 spacer">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne1.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">jun<br><strong>1</strong></span>
									  </div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 spacer">
		         				<a href=""><h2>I CARE PREVENTION SUMMIT</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza... and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		     
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link myborder"><a href="">see all</a></div>
		         	</div>
		         </div>
	     	</div>
					

			    </div>	
			   
			    <div class="col-lg-4 sidebar-blue-section">
					<div class="blue-border-box one">
						<div class="ribbonpart"><label>Learn more</label></div>
						
						<label>find us follow us</label>
						<ul>
							<li><a class="f_links" href="#"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter direstory</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i> You tube</a></li>
						</ul>
						<hr>
						<ul>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> News</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Academics</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Graphics Arts</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Announcements</a></li>
						</ul>
					</div>

				</div>	

			    	
			</div>    
		</section>
		
		<section class="inner-container newspart">
			<div class="ss-style-triangles news">		   
	         	<h2>Other events you may be interested in...</h2>	
	         </div>	
			<div class="eventpart">
	     		<div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Events</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link"><a href="">see all</a></div>
		         	</div>	
		        </div>

		        <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-7 spacer">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne1.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">jun<br><strong>1</strong></span>
										</div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 col-md-5 spacer">
		         				<a href=""><h2>I CARE PREVENTION SUMMIT</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza... and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-7 spacer">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne2.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">Nov<br><strong>7</strong></span>
										</div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 col-md-5 spacer">
		         				<a href=""><h2>EARLY CHILDHOOD EXTRAVAGANZA</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link myborder"><a href="">see all</a></div>
		         	</div>
		         </div>
	     	</div>
		</section>



		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
