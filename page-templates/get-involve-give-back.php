<?php
/**
 * Template Name: Give back get involve
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<label>Give back</label>
						<h3>A way to get involved</h3>
						<p>Learn more about all the way you can give back to EBRPSS</p>
					</div>	
				</div>		
		</section>

		<section class="inner-container join-our-team communicationpart">	
			<div class="row">	
			    <div class="col-lg-8">
			    	<h2>Donate the foundation</h2>	
			    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ducimus amet asperiores explicabo iste odio quibusdam molestias fuga deleniti doloribus expedita modi libero, ipsum consequatur nisi ea officia accusamus vel? <br> <br>
			    	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, eaque repellendus quas, ea aliquid quae at veniam harum? Quis possimus tempora perferendis, culpa praesentium? Maxime recusandae expedita distinctio, minima nisi. <br> <br>
			    	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, eaque repellendus quas, ea aliquid quae at veniam harum? Quis possimus tempora perferendis, culpa praesentium? Maxime recusandae expedita distinctio, minima nisi.
			    	</p>
			    	<a href="" class="btn">Visit the foundation's website</a>

					<div class="short-contentpart">
						<h2>Learn more about all the way to give</h2>
						<p>Click below to donate online, see upcoming events, find out about foundation grants and more.</p>
					</div>	

			    	<ul class="linkpart">
			    		<li><a href="#"> Donate online  to the foundation <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> grant opportunities <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> upcoming events <span class="link-arrow"></span></a></li>
			    		<li><a href="#"> stem volunteer sign-up <span class="link-arrow"></span></a></li>
			    	</ul>	
			    </div>	
			    <div class="col-lg-4 sidebar-blue-section">
					<div class="blue-border-box one">
						<div class="ribbonpart"><label>INTERESTING FACTS</label></div>
						<h3>Fun Facts About Our Parish Schools</h3>
						<h2>Did  you know?</h2>
						<div id="microsoftcounter">
							<div class="iconpart">
								<img src="<?php echo $path; ?>/img/board-green.png">
							</div>	
							<div class="counterpart">
					        	<div class="count" data-count="41000">0</div><div class="extention">+</div>
					        <label>Students</label>
					        </div>

					        <div class="iconpart">
								<img src="<?php echo $path; ?>/img/tag-green.png">
							</div>	
							<div class="counterpart">
					        	<div class="count">2nd</div>
					        <label>Largest Public School System in the state</label>
					        </div>
					        <div class="iconpart">
								<img src="<?php echo $path; ?>/img/family-green.png">
							</div>	
							<div class="counterpart">
					        	<div class="count" data-count="58000">0</div><div class="extention">+</div>
					        <label>Largest Public School System in the state</label>
					        </div>

						</div>	
					</div>
			    </div>	
			</div>    
		</section>
		
		<section class="inner-container newspart">			
	         <div class="ss-style-triangles news">		   
	         	<h2>What is happening in EBr?</h2>	
	         </div>	   
	        <div class="newspart">
		        <div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Recent news</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link myborder"><a href="">see all</a></div>
		         	</div>		
		        </div>

		         <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev1.jpg"> 
			         				    </div>
		         					</div>	
		         			 	</a>
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>Biden Project Youth Leads</h2></a>
		         				<p><span>ACADEMICS</span>  DEC. 1 2018 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to enjoy a special Thanksgiving...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev2.jpg"> 
			         				    </div>
		         					</div>	
		         				</a>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>EBR School Board Recognitions at the March 21 Regular Meeting</h2></a>
		         				<p><span class="green">announcement</span>  jan. 5 2019 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev3.jpg"> 
			         				    </div>
		         					</div>	
		         				</a>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>JANUARY 2019 BOARD RECOGNITIONS</h2></a>
		         				<p><span class="blue">news</span>  Jan . 5 2019 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev4.jpg"> 
			         				    </div>
			         				</div>	
			         			</a>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>THANKSGIVING 2018 PHOTO GALLERY</h2></a>
		         				<p><span>acadamics</span>  dec. 1 2018 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to enjoy a special Thanksgiving...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link"><a href="">see all</a></div>
		         	</div>
		         </div>

	     	</div>

	     	<div class="eventpart myborder">
	     		<div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Events</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link"><a href="">see all</a></div>
		         	</div>	
		        </div>

		        <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne1.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">jun<br><strong>1</strong></span>
										</div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>I CARE PREVENTION SUMMIT</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza... and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne2.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">Nov<br><strong>7</strong></span>
										</div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>EARLY CHILDHOOD EXTRAVAGANZA</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link myborder"><a href="">see all</a></div>
		         	</div>
		         </div>
	     	</div>
		</section>
		
		<section class="selectionpart">
			<div class="title">I want to get involved by...</div>			
			<div class="bottom_menu">
				<ul>
					<li><a href=""><i class="fa fa-book" aria-hidden="true"></i> volunteering</a></li>
					<li><a href=""><i class="fa fa-users" aria-hidden="true"></i> supporting</a></li>
					<li class="active"><a href=""><i class="fa fa-graduation-cap" aria-hidden="true"></i> giving back</a></li>					
				</ul>
			</div>
		</section>

		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
