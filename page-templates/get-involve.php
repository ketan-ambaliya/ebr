<?php
/**
 * Template Name: Get involve page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		<section class="overlay breadcrumbs " style="background-image: url(<?php echo $path; ?>/img/breadcrumb.jpg);">
			<div class="container-fluid">
				<div class="row">					
					<div class="col-md-12">
					 <ul>
					 	<li><a href="">About</a></li>
					 	<li><a href="">Get involved</a></li>
					 </ul>						
					</div>
				</div>
			</div>
		</section>

		<section class="inner-container paragraphpart m_b_50">	
				<div class="row">
					<div class="col-md-12 text-center">
						<label>Get involved</label>						
						<h3>Make a Difference</h3>
						<p>There is more than one way to give back to the East Baton Rouge Parish School System.</p>
					</div>	
				</div>		
		</section>

		<section class="parallaxpart threebox" style="background-image: url(<?php echo $path; ?>/img/banner2.jpg);">
			<div class="ribbonpart"><label>I want to get involved by...</label></div>
			<div class="inner-container">
			<div class="row justify-content-center display-flex">
					<div class="col-md-6 col-lg-4">
						<div class="greenbox">
							<h2 class="no-background"> <span>VOLUNTEERING </span></h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum temporibus labore cumque sed sint cum quasi excepturi alias praesentium nihil illum</p>
							<a href="" class="btn rightside">Learn more</a>						   
						</div>    
					</div>	
					
					<div class="col-md-6 col-lg-4">
						<div class="greenbox">
							<h2 class="no-background"> <span>SUPPORTING </span></h2>
							<p>See some of the upcoming events that you can attend with us.</p>
							<a href="" class="btn rightside">Learn more</a>						   
						</div>    
					</div>

					<div class="col-md-6 col-lg-4">
						<div class="greenbox">
							<h2 class="no-background"> <span>GIVING BACK </span></h2>
							<p>Support our school system by donating to our foundation.</p>
							<a href="" class="btn rightside">Learn more</a>						   
						</div>   
					</div>				

				</div>						
			</div>
		</section>

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<label>IN ACTION</label>
						<p>See some of the events our community members like you have attended to help make a difference!
						</p>
					</div>	
				</div>		
		</section>
		
		<div class="inner-container fancy-gallery-part">	
			<div class="container-fluid">		
				<div class="row justify-content-center">				
			        <div class="col-md-3 text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                    <!-- <div class='text-right'>
			                        <small class='text-muted'>Image Title</small>
			                    </div> -->
			                </a>
			            </div> 	           
			    	</div> 
			    	<div class="col-md-3 text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                </a>
			            </div> 	           
			    	</div>
			    	<div class="col-md-3 text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                </a>
			            </div> 	           
			    	</div> 
			    	<div class="col-md-3 text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                </a>
			            </div> 	           
			    	</div>
			    	<div class="col-md-3 text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                </a>
			            </div> 	           
			    	</div> 
			    	<div class="col-md-3 text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                </a>
			            </div> 	           
			    	</div>
			    	<div class="col-md-3 text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                </a>
			            </div> 	           
			    	</div>
			    	<div class="col-md-3 text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                </a>
			            </div> 	           
			    	</div>

			    	<div class="col-md-3 d-none text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/ev2.jpg">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/ev2.jpg" />	
			                </a>
			            </div> 	           
			    	</div> 
			    	<div class="col-md-3 d-none text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                </a>
			            </div> 	           
			    	</div>
			    	<div class="col-md-3 d-none text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                </a>
			            </div> 	           
			    	</div> 
			    	<div class="col-md-3 d-none text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                </a>
			            </div> 	           
			    	</div>
			    	<div class="col-md-3 d-none text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                </a>
			            </div> 	           
			    	</div> 
			    	<div class="col-md-3 d-none text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                </a>
			            </div> 	           
			    	</div>
			    	<div class="col-md-3 d-none text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                </a>
			            </div> 	           
			    	</div>
			    	<div class="col-md-3 d-none text-center">	
			            <div class="gallery">	            	
			                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $path; ?>/img/fancy-box-img.png">
			                    <img class="img-responsive" alt="" src="<?php echo $path; ?>/img/fancy-box-img.png" />	
			                </a>
			            </div> 	           
			    	</div>

				</div> 
			</div>
		</div>	

		<section class="selectionpart">
			<div class="title">Other popular pages</div>			
			<div class="bottom_menu">
				<ul>
					<li><a href=""><i class="fa fa-book" aria-hidden="true"></i> Join our team</a></li>
					<li class="active"><a href=""><i class="fa fa-users" aria-hidden="true"></i> Get involved</a></li>
					<li><a href=""><i class="fa fa-graduation-cap" aria-hidden="true"></i> Enroll now</a></li>
					<li><a href=""><i class="fa fa-bus" aria-hidden="true"></i> Transportation</a></li>
				</ul>
			</div>
		</section>

		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
