<?php
/**
 * Template Name: Programs Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<img src="<?php echo $path; ?>/img/link.png">  
						<label>programs</label>	
						<h3>A list of EBR Programs</h3>					
					</div>	
				</div>		
		</section>
		

		

		<section class="inner-container visitlink-sec">
			<div class="row justify-content-center">
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<img src="<?php echo $path; ?>/img/icon2.svg" class="svg" id="logo">
						<h3>EBR BEnefits</h3>
						<p>Employee benefits website</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<img src="<?php echo $path; ?>/img/briefcase.svg" class="svg" id="logo">
						<h3>EBR BEnefits</h3>
						<p>Employee benefits website</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<img src="<?php echo $path; ?>/img/icon3.svg" class="svg" id="logo">
						<h3>EBR BEnefits</h3>
						<p>Employee benefits website</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<img src="<?php echo $path; ?>/img/icon2.svg" class="svg" id="logo">
						<h3>EBR BEnefits</h3>
						<p>Employee benefits website</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<img src="<?php echo $path; ?>/img/icon2.svg" class="svg" id="logo">
						<h3>EBR BEnefits</h3>
						<p>Employee benefits website</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<img src="<?php echo $path; ?>/img/icon2.svg" class="svg" id="logo">
						<h3>EBR BEnefits</h3>
						<p>Employee benefits website</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<img src="<?php echo $path; ?>/img/icon2.svg" class="svg" id="logo">
						<h3>EBR BEnefits</h3>
						<p>Employee benefits website</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>
				<div class="col-md-3 text-center">
					<div class="visitlinkdiv">
						<img src="<?php echo $path; ?>/img/icon2.svg" class="svg" id="logo">
						<h3>EBR BEnefits</h3>
						<p>Employee benefits website</p>						
						<a href="#" class="btn">Visit link</a>
					</div>	
				</div>

				
			</div>	
		</section>

<!-- <div class="menu">
	<ul>
		<li><a href="">Menu 1</a></li>
		<li><a href="">Menu 2</a></li>
		<li ><a href="">Menu 3</a></li>
		<li><a href="">Menu 4</a></li>
		<li class="active"><a href="">Menu 5</a></li>
	</ul>
</div> -->

		<section class="selectionpart">
			<div class="title">learn more about our:</div>			
			<div class="bottom_menu">
				<ul>
					<li class="active"><a href="">all Programs</a></li>
					<li><a href="">all department</a></li>				
				</ul>
			</div>
		</section>



		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
