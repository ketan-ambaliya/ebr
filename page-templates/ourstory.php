<?php
/**
 * Template Name: ourstory Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		<!-- <section class="overlay breadcrumbs " style="background-image: url(<?php echo $path; ?>/img/breadcrumb.jpg);">
			<div class="container-fluid">
				<div class="row">					
					<div class="col-md-12">
					 <ul>
					 	<li><a href="">School</a></li>
					 	<li><a href="">Our story</a></li>
					 </ul>					
					</div>
				</div>
			</div>
		</section> -->

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<label>OUR STORY</label>
						<h3>The East Baton Rouge Parish School System</h3>
					</div>	
				</div>		
		</section>
		<section class="overlapcontent" style="background-image: url(<?php echo $path; ?>/img/banner1.jpg);">				
		</section>	
		<div class="overlaptextarea">
			<div class="overalappart inner-container">
				<p>Welcome to the East Baton Rouge Parish School System (EBRPSS), home to nine U.S. Blue Ribbon Schools, a nationally renowned Magnet Program and some of the best high schools in the country, according to U.S. News & World Report. We are currently the second largest public school system in the state, serving more than 42,000 students.Through the hard work and dedication of our 6,250 employees, of which 3,757 are teachers, our district continues to show a strong and steady increase in its average District Performance Score (DPS).</p>				
			</div>
		</div>

		<section class="yellowadvance yfixed">
			<div class="content-conainer">
				<div class="row justify-content-center">
					<div class="col-md-6">
						<div class="darkshade">
						<h2 class="no-background">
							<span>OUR MISSION</span>					
					    </h2>
					    <p>The East Baton Rouge Parish School System, in partnership with our community, educates all students to their maximum potential in a caring, rigorous and safe environment.</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="darkshade">
						<h2 class="no-background">
							<span>OUR VISION</span>					
					    </h2>
					    <p>All East Baton Rouge Parish School System Students will graduate with the knowledge, skills and values necessary to become active and successful members of a dynamic learning community. </p>
						</div>
					</div>
					<div class="col-md-12">
						<div class="darkshade">
						<h2 class="no-background">
							<span>OUR EXPECTATIONS</span>					
					    </h2>
					    <p>Will be clearly and constantly communicated to students, parents and other family members alike.  Through these means, students will be motivated to become high achievers. The EBRPSS personnel will always welcome parental and community involvement.  Through its professional and caring example, EBRPSS will earn parental respect and continued support. In turn, the East Baton Rouge Parish (EBR) community will treasure the school system and will provide their full support to strengthen high quality teaching and learning in a safe and attractive environment.</p>
						</div>
					</div>					
			</div>
		</section>
	
		<section class="did-you-konw-section blue_bg" style="background-color: #002e59;">
			<div class="inner-container">
				<div class="row">
					<div class="col-md-8">
						<label>Our Personnel</label>
						<p>
							To help meet the need of educating our citizens, there are approximately 6,252 full-time employees with more than 3,509 teachers. Of those teachers, 53% hold advanced degrees. More than 98.5% of the EBRPSS teachers are either state certified or highly qualified by Federal No Child Left Behind definition. Contact Human Resources at 922-5648.
						</p>
						<a href="#">JOIN OUR TEAM</a>
					</div>
					<div class="col-md-4">
						<div class="image-thum-block">
							<img src="<?php echo $path; ?>/img/ne1.jpg" alt="" class="primary-image">
							<img src="<?php echo $path; ?>/img/ne2.jpg" alt="" class="secondary-image">
						</div>
					</div>
				</div>
			</div>
		</section>

     	<section class="inner-container wall-of-fame">
			 <div class="row">
		         	<div class="col-12 text-center">
		         		<label>Community Support</label>
		         		<h2>Notable Alumni</h2>
		         		<p>See some of the graduates from our East Baton Rouge Parish schools who have gone on to do great things. </p>
		         	</div>		         		
		        </div>
				<div class="d-none d-lg-block">
			        <div class="row justify-content-center m_t30">				
						<div class="col-md-2 text-center">
							<div class="famediv">
								<img src="<?php echo $path; ?>/img/board-member.png">
								<h3>MARK BELLUE</h3>
								<span>General Counsel</span>
								<p>2008</p>
							</div>	
						</div>	
						<div class="col-md-2 text-center">
							<div class="famediv">
								<img src="<?php echo $path; ?>/img/board-member.png">
								<h3>MARK BELLUE</h3>
								<span>General Counsel</span>
								<p>2008</p>
							</div>	
						</div>	
						<div class="col-md-2 text-center">
							<div class="famediv">
								<img src="<?php echo $path; ?>/img/board-member.png">
								<h3>MARK BELLUE</h3>
								<span>General Counsel</span>
								<p>2008</p>
							</div>	
						</div>	
						<div class="col-md-2 text-center">
							<div class="famediv">
								<img src="<?php echo $path; ?>/img/board-member.png">
								<h3>MARK BELLUE</h3>
								<span>General Counsel</span>
								<p>2008</p>
							</div>	
						</div>	
						<div class="col-md-2 text-center">
							<div class="famediv">
								<img src="<?php echo $path; ?>/img/board-member.png">
								<h3>MARK BELLUE</h3>
								<span>General Counsel</span>
								<p>2008</p>
							</div>	
						</div>	
						<div class="col-md-2 text-center">
							<div class="famediv">
								<img src="<?php echo $path; ?>/img/board-member.png">
								<h3>MARK BELLUE</h3>
								<span>General Counsel</span>
								<p>2008</p>
							</div>	
						</div>	
						<div class="col-md-2 text-center">
							<div class="famediv">
								<img src="<?php echo $path; ?>/img/board-member.png">
								<h3>MARK BELLUE</h3>
								<span>General Counsel</span>
								<p>2008</p>
							</div>	
						</div>	
						<div class="col-md-2 text-center">
							<div class="famediv">
								<img src="<?php echo $path; ?>/img/board-member.png">
								<h3>MARK BELLUE</h3>
								<span>General Counsel</span>
								<p>2008</p>
							</div>	
						</div>	
						<div class="col-md-2 text-center">
							<div class="famediv">
								<img src="<?php echo $path; ?>/img/board-member.png">
								<h3>MARK BELLUE</h3>
								<span>General Counsel</span>
								<p>2008</p>
							</div>	
						</div>	
						<div class="col-md-2 text-center">
							<div class="famediv">
								<img src="<?php echo $path; ?>/img/board-member.png">
								<h3>MARK BELLUE</h3>
								<span>General Counsel</span>
								<p>2008</p>
							</div>	
						</div>	
						<div class="col-md-2 text-center">
							<div class="famediv">
								<img src="<?php echo $path; ?>/img/board-member.png">
								<h3>MARK BELLUE</h3>
								<span>General Counsel</span>
								<p>2008</p>
							</div>	
						</div>	
						<div class="col-md-2 text-center">
							<div class="famediv">
								<img src="<?php echo $path; ?>/img/board-member.png">
								<h3>MARK BELLUE</h3>
								<span>General Counsel</span>
								<p>2008</p>
							</div>	
						</div>	
					</div>
				</div>
				<div class="d-block d-lg-none">
					<div class="slider slider-nav">
					    <div>
					    	<div class="row">
						    	<div class="col-md-6 text-center">
									<div class="famediv">
										<img src="<?php echo $path; ?>/img/board-member.png">
										<h3>MARK BELLUE</h3>
										<span>General Counsel</span>
										<p>2008</p>
									</div>	
								</div>	
								<div class="col-md-6 text-center">
									<div class="famediv">
										<img src="<?php echo $path; ?>/img/board-member.png">
										<h3>MARK BELLUE</h3>
										<span>General Counsel</span>
										<p>2008</p>
									</div>	
								</div>
						 </div>
					   </div>
					   <div>
					    	<div class="row">
						    	<div class="col-md-6 text-center">
									<div class="famediv">
										<img src="<?php echo $path; ?>/img/board-member.png">
										<h3>MARK BELLUE</h3>
										<span>General Counsel</span>
										<p>2008</p>
									</div>	
								</div>	
								<div class="col-md-6 text-center">
									<div class="famediv">
										<img src="<?php echo $path; ?>/img/board-member.png">
										<h3>MARK BELLUE</h3>
										<span>General Counsel</span>
										<p>2008</p>
									</div>	
								</div>
						 </div>
					   </div>
					   <div>
					    	<div class="row">
						    	<div class="col-md-6 text-center">
									<div class="famediv">
										<img src="<?php echo $path; ?>/img/board-member.png">
										<h3>MARK BELLUE</h3>
										<span>General Counsel</span>
										<p>2008</p>
									</div>	
								</div>	
								<div class="col-md-6 text-center">
									<div class="famediv">
										<img src="<?php echo $path; ?>/img/board-member.png">
										<h3>MARK BELLUE</h3>
										<span>General Counsel</span>
										<p>2008</p>
									</div>	
								</div>
						 </div>
					   </div>
					    
					  </div>
				</div>		
		</section>
		

		<section class="community" style="background-image: url(<?php echo $path; ?>/img/story-bg.jpg);">
			<div class="inner-container">
			 <div class="row">
		         	<div class="col-12 text-center">
		         		<label>Wall of fame</label>
		         		<hr>
		         		<h2>In 1880</h2>	
		         		<p>East Baton Rouge Parish School System was incorporated.</p>	  
		         		<hr>
		         		<h2>In 1998</h2>	
		         		<p>The voters of EBR Parish approved the five-year collection of a one-cent sales tax dedicated to improving public education. The tax generated nearly $300 million which the school system used to fund a facilities improvement plan including the construction of four new schools, classroom additions at another 22, and improvements and repairs to every school campus. The tax also funded the purchase and implementation of computer technology at every school, alternative learning spaces and discipline centers at the elementary, middle and high school levels for system-wide discipline programs, and pay supplements for teachers and all system employees.</p>	  
		         		<hr>
		         		<h2>In 2003</h2>	
		         		<p>The voters renewed the collection of the sales tax for another five years. With the nearly $300 million collected from the renewal, the district was able to build eight new schools, renovate four more and make repairs to an additional 40. Funding was maintained for the discipline programs and employee pay supplements.</p>	
		         		<hr> 
		         		<h2>In 2007</h2>	
		         		<p>The voters renewed two property millages for another 10 years. This will generate more than $26 million, which the district will use toward maintaining and improving salaries for all public school employees and operating the School System.</p>	
		         		<hr> 
		         		<h2>In 2008</h2>	
		         		<p>The voters of EBR parish once again approved the one-cent sales tax. However, the collection was extended from the original five-year collection period to ten-year. The passage of this renewal is expected to generate close to $488 million.</p>   
		         		<hr> 
		         		<a href="">SEE HOW YOU CAN HELP</a>
		         	</div>		         		
		        </div>
		    </div>    
		</section>

		<section class="selectionpart">
			<div class="title">learn more about our:</div>			
		<div class="bottom_menu">
			<ul>
				<li><a href="">Leadership</a></li>
				<li class="active"><a href="">Story</a></li>
				<li><a href="">board</a></li>
				<li><a href="">policies</a></li>
				<li><a href="">communication</a></li>
			</ul>
		</div>
		</section>

		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
