<?php
/**
 * Template Name: Common Pages
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>
<div class="wrapper" id="full-width-page-wrapper">
	<div class="container-fluid no-padding">
<?php if( have_rows('flexible_content_sections') ): 
		while ( have_rows('flexible_content_sections') ) : the_row(); ?>
		<?php if( get_row_layout() == 'board_meeting_dates' ):
		$button_text = get_sub_field('button_text'); 
		$button_url = get_sub_field('button_url');
		$meeting_title = get_sub_field('meeting_title');
		$meeting_sub_title = get_sub_field('meeting_sub_title');
			?>

				
			<section class="inner-container paragraphpart p_t_0">	
				<div class="row">
					<div class="col-md-12 text-center">
						<?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>
					</div>
				</div>
			</section>

			<section class="inner-container blue-section">
			<div class="blue-border-box" id="one">
				<?php if( !empty($meeting_title)):?><div class="ribbonpart"><label><?php echo $meeting_title; ?></label></div><?php endif;?>
				<?php if( !empty($meeting_sub_title)):?><p><?php echo $meeting_sub_title; ?></p><?php endif;?>
				 <div class="row justify-content-center">

				 	<?php if( have_rows('meeting_blocks') ): $i = 0;
					while ( have_rows('meeting_blocks') ) : the_row();
						$month_name = get_sub_field('month_name'); 
						$date = get_sub_field('date'); 
						$special_date = get_sub_field('special_date');

						?>
						<div class="col-md-4">
							<?php if( !empty($month_name)):?><label><?php echo $month_name; ?></label><?php endif;?>
							<?php if( !empty($date)):?><h6><?php echo $date; ?></h6><?php endif;?>
							<?php if( !empty($special_date)):?><h6><strong>Special Date:</strong> <?php echo $special_date; ?></h6><?php endif;?>
						</div>	
					<?php $i++;
					if($i == 3){?><hr class="d-none d-md-block"><?php $i = 0;} ?>
					<?php  endwhile; endif;?>

					
				</div>	
				
					
			</div>
		</section>

		<?php elseif( get_row_layout() == 'enrollment_steps' ): 
			$step_title = get_sub_field('step_title'); 
			$step_description = get_sub_field('step_description'); ?>

		<section class="inner-container accordianpart">
			<?php if( !empty($step_title)):?><h2 class="text-center"><?php echo $step_title; ?></h2><?php endif;?>
			<?php if( !empty($step_description)):?><p class="text-center"><?php echo $step_description; ?></p><?php endif;?>
		</section>

		<?php elseif( get_row_layout() == 'custom_blocks_without_text' ): ?>

			<section class="yellowpart">
			<div class="content-conainer">
				<div class="row justify-content-center display-flex">
					<?php if( have_rows('boxs') ): 
					while ( have_rows('boxs') ) : the_row();
					$box_title = get_sub_field('box_title'); 
					$button_text = get_sub_field('button_text'); 
					$button_url = get_sub_field('button_url'); ?>
					<div class="col-md-6">
						<div class="darkshade">
						<?php if( !empty($box_title)):?>
							<h2 class="no-background">
								<span><?php echo $box_title; ?></span>					
						    </h2>
					    <?php endif;?>
					    <?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>	
						</div>
					</div>
					<?php endwhile; endif;?>
			</div>
		</section>
		
		<?php elseif( get_row_layout() == 'member_section'):

			$section_title = get_sub_field('section_title'); 
			$section_sub_title = get_sub_field('section_sub_title'); ?>

		<section class="inner-container paragraphpart">	
			<div class="row">
				<div class="col-md-12 text-center">
					<?php if( !empty($section_title)):?><label><?php echo $section_title; ?></label><?php endif;?>	<?php if( !empty($section_sub_title)):?><p class="subtitle"><?php echo $section_sub_title; ?></p><?php endif;?>	
				</div>	
			</div>		
		</section>

		<section class="inner-container">
			<div class="row justify-content-center">
				<?php if( have_rows('members') ):  $i = 1;
				while ( have_rows('members') ) : the_row();
				$profile_image = get_sub_field('profile_image'); 
				$name = get_sub_field('name'); 
				$designation = get_sub_field('designation'); 
				$district = get_sub_field('district');
				$email = get_sub_field('email');
				$button_text = get_sub_field('button_text');
				$button_url = get_sub_field('button_url');
				$pop_up_option = get_sub_field('pop_up_option');?>
				<div class="col-md-4 text-center">
					<div class="profilediv">
						<?php if( !empty($profile_image)):?><img src="<?php echo $profile_image['url']; ?>" alt="<?php echo $profile_image['alt']; ?>">
						<?php else:?>
						<img src="<?php echo $path."/img/nofound.png"; ?>"><?php endif;?>	
						<?php if( !empty($name)):?><div class="ribbonpart"><label><?php echo $name; ?></label></div><?php endif;?>
						<?php if( !empty($designation)):?><span><?php echo $designation; ?></span><?php endif;?>
						<?php if( !empty($district)):?><span><?php echo $district; ?></span><?php endif;?>
						<?php if( !empty($email)):?><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a><?php endif;?>
						<a href="#detail<?php echo $i;?>" class="btn fancybox"><?php echo $button_text; ?></a>
						<div id="detail<?php echo $i;?>" style="display:none">
							<?php if($pop_up_option == "Form") { 
								//echo do_shortcode("[contact-form-7 id='92' title='Meet them form']");
								echo do_shortcode( '[contact-form-7 id="92" title="Meet them form" destination-email="'.$email.'"]' );

							} else { ?>
							<?php $pop_up_description = get_sub_field('pop_up_description');?>	
							<div class="team-container">
							 <div class="row">
							 	<div class="col-md-4">
							 		<?php if( !empty($profile_image)):?><img src="<?php echo $profile_image['url']; ?>" alt="<?php echo $profile_image['alt']; ?>"><?php else:?>
									<img src="<?php echo $path."/img/nofound.png"; ?>"><?php endif;?>
							 	</div>
							 	<div class="col-md-8">	
							 	<?php if( !empty($name)):?><div class="ribbonpart"><label><?php echo $name; ?></label></div><?php endif;?>
								<?php if( !empty($designation)):?><span><?php echo $designation; ?></span><?php endif;?>
								<?php if( !empty($district)):?><span><?php echo $district; ?></span><?php endif;?>
								<?php if( !empty($email)):?><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a><?php endif;?>
							 	</div>	
							 	<div class="col-md-12">
							 		<?php if( !empty($pop_up_description)):?><span><?php echo $pop_up_description; ?></span><?php endif;?>
							 	</div>
							 </div>	
							</div>

							<?php }?>
						</div>
					</div>	
				</div>
				<?php $i++; endwhile; endif;?>
				
			</div>	
		</section>

		<?php elseif( get_row_layout() == 'job_openings'):?>
			<section class="inner-container join-our-team">	
					<div class="row">	
					    <div class="col-lg-8">
					    	<ul class="linkpart">
							<?php if( have_rows('jobs') ): 
								while ( have_rows('jobs') ) : the_row();
									$job_name = get_sub_field('job_name'); 
									$job_link = get_sub_field('job_link'); ?>
									<?php if( !empty($job_link)):?><li><a href="<?php echo $job_link; ?>" target="_blank"> <?php echo $job_name; ?><span class="link-arrow"></span></a></li><?php endif;?>
								<?php endwhile; endif;?>
							</ul>

					    <?php $video_title = get_sub_field('video_title');
					   		  $video_iframe = get_sub_field('video_iframe'); ?>
					    <section class="ribbontitle">
								<div class="row">
						         	<div class="col-12 text-center">
						         		<?php if( !empty($video_title)):?><label><?php echo $video_title; ?></label><?php endif;?>
						         	</div>		         		
						        </div>
						        <?php if( !empty($video_iframe)):?><?php echo $video_iframe; ?><?php endif;?>
						</section>		
					    </div>	
					    <div class="col-lg-4 sidebar-blue-section">
							<div class="blue-border-box one">
								 <?php $sidbar_main_title = get_sub_field('sidbar_main_title');
					   		  $sidebar_title = get_sub_field('sidebar_title'); 
					   		  $sidebar_sub_title = get_sub_field('sidebar_sub_title'); ?>
								<?php if( !empty($sidbar_main_title)):?><div class="ribbonpart"><label><?php echo $sidbar_main_title; ?></label></div><?php endif;?>
								<?php if( !empty($sidebar_title)):?><h3><?php echo $sidebar_title; ?></h3><?php endif;?>
								<?php if( !empty($sidebar_sub_title)):?><h2><?php echo $sidebar_sub_title; ?></h2><?php endif;?>

								<div id="microsoftcounter">
									<?php if( have_rows('do_you_know_reapeter') ): $i=1;
									while ( have_rows('do_you_know_reapeter') ) : the_row();
									$icon = get_sub_field('icon'); 
									$number = get_sub_field('number');
									$description = get_sub_field('description'); ?>
										<?php if( !empty($sidebar_sub_title)):?>
											<div class="iconpart">
												<img src="<?php echo $icon['url']; ?>">
											</div>	
										<?php endif;?>
										<div class="counterpart">
								        	<?php if( !empty($number)):?><div class="count" data-count="<?php echo $number; ?>">0</div><?php endif;?><div class="extention"><?php if($i == 2) {echo "nd"; } else {echo "+";}?></div>
								       <?php if( !empty($description)):?> <label><?php echo $description; ?></label><?php endif;?>
								        </div>

								    <?php $i++; endwhile; endif;?>
								</div>	
							</div>
					    </div>	
					</div>    
		</section>

		<?php elseif( get_row_layout() == 'employee_content_ with_sidebar'):
			$content_title = get_sub_field('content_title');
			$content_are = get_sub_field('content_are');
			$table_title = get_sub_field('table_title');?>

		<section class="inner-container communicationpart">	
			<div class="row">	
			    <div class="col-lg-8">
			    	<?php if( !empty($content_title)):?><h2><?php echo $content_title; ?></h2><?php endif;?>
			    	<?php if( !empty($content_are)):?><?php echo $content_are; ?><?php endif;?>

			    	<div class="yellowborderbox one">
			    		<div class="titlepart">
			    			<?php if( !empty($table_title)):?><h2><?php echo $table_title; ?></h2><?php endif;?>
			    		</div>	
			    		<div class="tabItem">	
			    			<?php if( have_rows('table_repeater') ): 
							while ( have_rows('table_repeater') ) : the_row();
							$title = get_sub_field('title'); 
							$link = get_sub_field('link');
							$description = get_sub_field('description'); 
							$button_text = get_sub_field('button_text');
							$button_url = get_sub_field('button_url');?>
				    			<div class="row">
					    			<div class="col-md-9">
					    				<?php if( !empty($title)):?><a href="<?php echo $link; ?>"><?php echo $title; ?></a><?php endif;?>
										<?php if( !empty($description)):?><p class="shortdetails"><?php echo $description; ?></p><?php endif;?>
									</div>
									<div class="col-md-3 text-center">	
										<?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>
									</div>
								</div>
								<hr>
							<?php endwhile; endif;?>
						</div>	
			    	</div>	
			    </div>

			    <div class="col-lg-4 sidebar-blue-section">
			    	<?php $sidbar_main_title = get_sub_field('sidbar_main_title');
					$show_you_may_also_interested = get_sub_field('show_you_may_also_interested');?>
					<div class="blue-border-box one">
						<?php if( !empty($sidbar_main_title)):?><div class="ribbonpart"><label><?php echo $sidbar_main_title; ?></label></div><?php endif;?>
						
						<?php if( have_rows('link_reapeter') ): 
							while ( have_rows('link_reapeter') ) : the_row();
							$heading = get_sub_field('heading');?>
							
							<?php if( !empty($heading)):?><label> <?php echo $heading; ?> </label><?php endif;?>
							<ul>
								<?php if( have_rows('links') ): 
									while ( have_rows('links') ) : the_row();
									$link_text = get_sub_field('link_text');
									$link_url = get_sub_field('link_url');?>
										<?php if( !empty($link_url)):?><li><a class="f_links" href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a></li><?php endif;?>
								<?php  endwhile; endif;?>
							</ul>	
							<hr>
						<?php  endwhile; endif;?>

						<?php if($show_you_may_also_interested == "Yes"){?>
							
							<?php $interested_in_title = get_field('interested_in_title', 'option');?>
							<?php if( !empty($interested_in_title)):?><label> <?php echo $interested_in_title; ?> </label><?php endif;?>
							<ul>
								<?php if( have_rows('interested_in_links', 'option') ):
									while ( have_rows('interested_in_links', 'option') ) : the_row();
									$link_text = get_sub_field('link_text', 'option');
									$link_url = get_sub_field('link_url', 'option');?>
								<?php if( !empty($link_url)):?><li><a class="f_links" href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a></li><?php endif;?>
								<?php endwhile; endif;?>
							</ul>
						<?php } ?>
					</div>
			    </div>	
			</div>    
		</section>

		<?php elseif( get_row_layout() == 'enrollment_get_prepared'):
			$button_text = get_sub_field('button_text');
			$button_url = get_sub_field('button_url');?>
			<section class="bluecontainer enroll no-bg">		
				<div class="row">
					<div class="col-md-12 text-center">						
						<div class="processBox" id="microsoftcounter">
							
						<?php if( have_rows('prepared_repeater') ): 
							while ( have_rows('prepared_repeater') ) : the_row();
							$description = get_sub_field('description');
							$title = get_sub_field('title');?>
							<div class="processBoxCont">
								<div class="imgBoxMain">
								   <?php if( !empty($title)):?>
							        <div class="counterpart">
							        	<label><?php echo $title; ?></label>
							        	<div class="clerfix"></div>
							        </div>
							      </p><?php endif;?>
							      <?php if( !empty($description)):?> <p class="sortdiscription"><?php echo $description; ?></p><?php endif;?>

								</div>
							</div>
							<?php endwhile; endif;?>
						</div>	
						<div class="clerfix"></div>
						<?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>
					</div>	
				</div>		
		</section>

		<?php elseif( get_row_layout() == 'enrollment_process'):
			$main_title = get_sub_field('main_title');
			$sub_title = get_sub_field('sub_title');
			$description = get_sub_field('description');?>

		<section class="bluecontainer enroll">	
			<div class="inner-container">	
				<div class="row">
					<div class="col-md-12 text-center">						
						
						<?php if( !empty($main_title)):?><label> <?php echo $main_title; ?> </label><?php endif;?>
						<?php if( !empty($sub_title)):?><h3> <?php echo $sub_title; ?> </h3><?php endif;?>
						<?php if( !empty($description)):?><p> <?php echo $description; ?> </p><?php endif;?>
						<div class="processBox" id="microsoftcounter">
							<?php if( have_rows('process_repeater') ): 
							while ( have_rows('process_repeater') ) : the_row();
							$icon = get_sub_field('icon');
							$title = get_sub_field('title');?>

							<div class="processBoxCont">
								<div class="imgBoxMain">
									<div class="imgBox">
							 	      <?php if( !empty($main_title)):?><img src="<?php echo $icon['url']; ?>"><?php endif;?>
							        </div> 
							        <div class="counterpart">
							        	<?php if( !empty($title)):?><label><?php echo $title; ?></label><?php endif;?>
							        	
							        <div class="clerfix"></div>
							        </div>
							       							       
								</div>
							</div>
							<?php endwhile; endif;?>
							
						
						</div>	
						<div class="clerfix"></div>
					</div>	
				</div>		
			</div>		
		</section>

		<?php elseif( get_row_layout() == 'enrollment_content_with_sidebar'):
			$content_title = get_sub_field('content_title');
			$content_are = get_sub_field('content_are');
			$button_url = get_sub_field('button_url');
			$button_text = get_sub_field('button_text');
			?>

		<section class="inner-container communicationpart">	
			<div class="row">	
			    <div class="col-lg-8">
			    	<?php if( !empty($content_title)):?><h2><?php echo $content_title; ?></h2><?php endif;?>
			    	<?php if( !empty($content_are)):?><?php echo $content_are; ?><?php endif;?>	
			    	<?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn" target="_blank"><?php echo $button_text; ?></a><?php endif;?>
			    </div>

			    <div class="col-lg-4 sidebar-blue-section">
			    	<?php $sidbar_main_title = get_sub_field('sidbar_main_title');
					$show_you_may_also_interested = get_sub_field('show_you_may_also_interested');?>
					<div class="blue-border-box one">
						<?php if( !empty($sidbar_main_title)):?><div class="ribbonpart"><label><?php echo $sidbar_main_title; ?></label></div><?php endif;?>
						
						<?php if( have_rows('link_reapeter') ): 
							while ( have_rows('link_reapeter') ) : the_row();
							$heading = get_sub_field('heading');?>
							
							<?php if( !empty($heading)):?><label> <?php echo $heading; ?> </label><?php endif;?>
							<ul>
								<?php if( have_rows('links') ): 
									while ( have_rows('links') ) : the_row();
									$link_text = get_sub_field('link_text');
									$link_url = get_sub_field('link_url');?>
										<?php if( !empty($link_url)):?><li><a class="f_links" href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a></li><?php endif;?>
								<?php  endwhile; endif;?>
							</ul>	
							<hr>
						<?php  endwhile; endif;?>

						<?php if($show_you_may_also_interested == "Yes"){?>
							
							<?php $interested_in_title = get_field('interested_in_title', 'option');?>
							<?php if( !empty($interested_in_title)):?><label> <?php echo $interested_in_title; ?> </label><?php endif;?>
							<ul>
								<?php if( have_rows('interested_in_links', 'option') ):
									while ( have_rows('interested_in_links', 'option') ) : the_row();
									$link_text = get_sub_field('link_text', 'option');
									$link_url = get_sub_field('link_url', 'option');?>
								<?php if( !empty($link_url)):?><li><a class="f_links" href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a></li><?php endif;?>
								<?php endwhile; endif;?>
							</ul>
						<?php } ?>
					</div>
			    </div>	
			</div>    
		</section>

		<?php elseif( get_row_layout() == 'contact_directory'):
			$contact_directory_title = get_sub_field('contact_directory_title');
			$map_title = get_sub_field('map_title');
			$map = get_sub_field('map');?>		

			<section class="paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						
						<?php if ( get_theme_mod( 'phone_label' ) ):?>
										<strong><a href="tel:<?php echo get_theme_mod( 'phone_label' );?>"> <?php echo get_theme_mod( 'phone_label' );?></a></strong>
									<?php endif; ?>
						<div class="text-center">
							<ul class="social">
							    	<?php if ( get_theme_mod( 'facebook_url' ) ):?>
			    		<li>
			    			<a href="<?php echo get_theme_mod( 'facebook_url' );?>" target="_blank">
			    				<i class="fa <?php echo get_theme_mod( 'facebook_icon_class' );?>" aria-hidden="true"></i>
			    			</a>
			    		</li>
			    	<?php endif; ?>
			    	<?php if ( get_theme_mod( 'twitter_url' ) ):?>
			    		<li>
			    			<a href="<?php echo get_theme_mod( 'twitter_url' );?>" target="_blank">
			    				<i class="fa <?php echo get_theme_mod( 'twitter_icon_class' );?>" aria-hidden="true"></i>
			    			</a>
			    		</li>
			    	<?php endif; ?>
			    	<?php if ( get_theme_mod( 'youtube_url' ) ):?>
			    		<li>
			    			<a href="<?php echo get_theme_mod( 'youtube_url' );?>" target="_blank">
			    				<i class="fa <?php echo get_theme_mod( 'youtube_icon_class' );?>" aria-hidden="true"></i>
			    			</a>
			    		</li>
			    	<?php endif; ?>
			    	<?php if ( get_theme_mod( 'instagram_url' ) ):?>
			    		<li>
			    			<a href="<?php echo get_theme_mod( 'instagram_url' );?>" target="_blank">
			    				<i class="fa <?php echo get_theme_mod( 'instagram_icon_class' );?>" aria-hidden="true"></i>
			    			</a>
			    		</li>
			    	<?php endif; ?>
							    </ul>
						</div>
					</div>	
				</div>		
			</section>

			<section class="inner-container communicationpart">	
			<div class="row">	
			    <div class="col-md-12">
			    	<div class="yellowborderbox one">
			    		<div class="titlepart">
			    			<?php if( !empty($contact_directory_title)):?><h2><?php echo $contact_directory_title; ?></h2><?php endif; ?>
			    		</div>	
			    		<div class="directorypart">
			    			
			    			
			    					<?php $args = array( 'post_type' => 'department-contacts', 'posts_per_page' => -1, 'orderby' => 'title', 'order'   => 'ASC',);


										$query = new WP_Query( $args );
										$post_count = $query->post_count;
										$posts_per_column = ceil($post_count / 3);      

										$rows = array();                                            
										$count = 0;
										while ($query->have_posts()){ $query->the_post();
										  
						       			  $fax = get_field('fax');
						       			  $address = get_field('address');
						       			  $address2 = get_field('address_2');?>
						       			
									   <?php if($rows[$count] == ""){ $rows[$count] = '<div class="row">'; }

									    $rows[$count] = $rows[$count] . '<div class="col-md-4">'.
									    
									            '<label>'.get_the_title().'</label>' .
									            '<p>'.$address.' ' .$address2.'</p><p>';?>

									          <?php if( have_rows('phone_number') ): $i=1;
												while ( have_rows('phone_number') ) : the_row();
												 $phone = get_sub_field('phone'); ?>
												<?php if( !empty($phone)):
													
													     
					 
									 $rows[$count] = $rows[$count] . ''.(($i == 1) ? '<strong>Ph:</strong>' : ' | ').'<a href="tel:'.$phone.'">'. $phone.'</a>';  
				
									endif;?>
									<?php $i++; 

								endwhile; endif;?>

									           <?php 
									           $rows[$count] = $rows[$count] .' <strong>Fax:</strong>'.$fax.'</p>'.
									            '</div>';
										    $count++;                           
										    if ($count == $posts_per_column ) { $count = 0; }  
										     
										}
 wp_reset_query();
										foreach ($rows as $row) { echo $row . '</div>'; }

					        		 /* $loop = new WP_Query( $args );
					        		  if ( $loop->have_posts() ) {
					       			  while ( $loop->have_posts() ) : $loop->the_post();
					       			  $name = get_field('name');
					       			  $designation = get_field('designation');
					       			  $fax = get_field('fax');
					       			  $address = get_field('address');
					       			  $map_link = get_field('map_link');
					       			  $dispatch_hours = get_field('dispatch_hours');
					       			  $num = $loop->post_count; 
					       			  $number1 = $num / 3; ?>
					       			  <div class="col-md-4">
							       		<label><?php the_title();?></label>
							       		<?php if(!empty($address)):?><p><?php echo $address;?></p><?php endif;?>
							       		<?php if( !empty($phone_number) || !empty($fax)):?>
							       		<p>
							       		<?php if( have_rows('phone_number') ): $i=1;
										while ( have_rows('phone_number') ) : the_row();
										 $phone = get_sub_field('phone'); ?>
										<?php if( !empty($phone)):?>
											
												<?php if($i == 1){?><strong>Ph:</strong><?php } ?>
												<?php if($i != 1){?>or<?php } ?>
												<a href="tel:<?php echo $phone;?>"><?php echo $phone;?></a>   
										<?php endif;?>
										<?php $i++; endwhile; endif;?> 

										<strong>Fax:</strong> <?php echo $fax;?>
										</p>
										<?php endif;?>
										</div>
										<?php  wp_reset_query(); endwhile; 
								   		} else {
											echo "No Contact Found";
										}*/?>	
			    				
			    			
			    		</div>
			    	</div>	
			    </div>			   
			</div>    
		</section>

		<section class="gmap text-center">
			<?php if( !empty($map_title)):?><div class="ribbonpart"><label><?php echo $map_title; ?></label></div><?php endif; ?>
			<?php if( !empty($map_title)):?> <?php echo $map;?><?php endif; ?>
		</section>	

		<?php elseif( get_row_layout() == 'family_content_ with_sidebar'):
			$content_title = get_sub_field('content_title');
			$content_are = get_sub_field('content_are');
			$table_title = get_sub_field('table_title');?>

		<section class="inner-container communicationpart">	
			<div class="row">	
			    <div class="col-lg-8">
			    	<?php if( !empty($content_title)):?><h2><?php echo $content_title; ?></h2><?php endif;?>
			    	<?php if( !empty($content_are)):?><?php echo $content_are; ?><?php endif;?>

			    	<div class="yellowborderbox one">
			    		<div class="titlepart">
			    			<?php if( !empty($table_title)):?><h2><?php echo $table_title; ?></h2><?php endif;?>
			    		</div>
			    		
			    		<div class="tab tab2">
								<div class="tabHeader">
									<ul>
										<?php if( have_rows('table_repeater') ): $i = 1;
										while ( have_rows('table_repeater') ) : the_row();
										$title = get_sub_field('title'); 
										?>
									      <?php if( !empty($title)):?><li <?php if($i == 1){?>class="active"<?php }?>><?php echo $title; ?></li><?php endif;?>
									     <?php $i++; endwhile; endif;?> 
								    </ul>
								</div>
								<div class="tabContent">
									<?php if( have_rows('table_repeater') ): $i = 1;
										while ( have_rows('table_repeater') ) : the_row();?>

										<div class="tabItem <?php if($i == 1){?>active<?php }?>">		
											<?php if( have_rows('tab_repeater') ): 
											while ( have_rows('tab_repeater') ) : the_row();
												$title = get_sub_field('title'); 
												$link = get_sub_field('link');
												$description = get_sub_field('description');
												?>				
												<a href="<?php echo $link; ?>">
									       		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
									       		<?php echo $title; ?></a>
									       		<p class="shortdetails"><?php echo $description; ?></p>
								       		<?php endwhile; endif;?>

									    </div>
								    <?php $i++; endwhile; endif;?>
								</div>
						</div>

			    			
			    	</div>	
			    </div>

			    <div class="col-lg-4 sidebar-blue-section">
			    	<?php $sidbar_main_title = get_sub_field('sidbar_main_title');
					$show_you_may_also_interested = get_sub_field('show_you_may_also_interested');?>
					<div class="blue-border-box one">
						<?php if( !empty($sidbar_main_title)):?><div class="ribbonpart"><label><?php echo $sidbar_main_title; ?></label></div><?php endif;?>
						
						<?php if( have_rows('link_reapeter') ): 
							while ( have_rows('link_reapeter') ) : the_row();
							$heading = get_sub_field('heading');?>
							
							<?php if( !empty($heading)):?><label> <?php echo $heading; ?> </label><?php endif;?>
							<ul>
								<?php if( have_rows('links') ): 
									while ( have_rows('links') ) : the_row();
									$link_text = get_sub_field('link_text');
									$link_url = get_sub_field('link_url');?>
										<?php if( !empty($link_url)):?><li><a class="f_links" href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a></li><?php endif;?>
								<?php  endwhile; endif;?>
							</ul>	
							<hr>
						<?php  endwhile; endif;?>

						<?php if($show_you_may_also_interested == "Yes"){?>
							
							<?php $interested_in_title = get_field('interested_in_title', 'option');?>
							<?php if( !empty($interested_in_title)):?><label> <?php echo $interested_in_title; ?> </label><?php endif;?>
							<ul>
								<?php if( have_rows('interested_in_links', 'option') ):
									while ( have_rows('interested_in_links', 'option') ) : the_row();
									$link_text = get_sub_field('link_text', 'option');
									$link_url = get_sub_field('link_url', 'option');?>
								<?php if( !empty($link_url)):?><li><a class="f_links" href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a></li><?php endif;?>
								<?php endwhile; endif;?>
							</ul>
						<?php } ?>
					</div>
			    </div>	
			</div>    
		</section>


		<?php elseif( get_row_layout() == 'district_master_calendar_section'):?>

			<?php $main_title = get_sub_field('main_title');
			$description = get_sub_field('description');
			$button_text = get_sub_field('button_text');
			$button_url = get_sub_field('button_url');?>
			<?php if( !empty($main_title) || !empty($description)  || !empty($button_url)):?>

				<section class="bluecontainer featuredbox-sec my-margin">			
				<div class="row">
					<div class="col-md-12 text-center">
						<?php if( !empty($main_title)):?><h2><?php echo $main_title; ?></h2><?php endif;?>
						<?php if( !empty($description)):?><p><?php echo $description; ?></p><?php endif;?>
						<?php if( !empty($button_url)):?><a href="<?php echo $button_url;?>" class="btn"><?php echo $button_text;?></a><?php endif;?>
					</div>
				</div>			 	
				</section>
			<?php endif;?>

			<?php elseif( get_row_layout() == 'calendar_with_sidebar'):
				$calendar_shortcode = get_sub_field('calendar_shortcode');
				$calendar_title = get_sub_field('calendar_title');
				$sidebar_title = get_sub_field('sidebar_title');?>

			<section class="inner-container news_content">	
				<div class="row">	
			   		<div class="col-lg-8">
			   			
			   			<?php if( !empty($calendar_title)):?>
			   				<div class="communicationpart">
			   				<div class="ribbonpart"><label><?php echo $calendar_title; ?></label></div>
			   				</div>
			   			<?php endif;?>
			   			<div class="calendar-box">
						<?php echo do_shortcode($calendar_shortcode); ?>
					</div>
			   		</div>
			   		<div class="col-lg-4 sidebar-blue-section">
			   			<div class="paragraphpart"> <h3><?php echo $sidebar_title; ?></h3></div>
			   			
			   			<div class="eventpart">
			         		
			         	<div class="row">
			         		<?php global $post;
			    	
			    	
					$get_posts = tribe_get_events(array('posts_per_page'=>'1', 'eventDisplay'=>'upcoming') );
						if($get_posts){
					foreach($get_posts as $post) { setup_postdata($post);
					        ?>
	        			
				        <div class="col-12 col-md-12 spacer">
					        <a href="<?php the_permalink(); ?>">
					        	<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <?php	if ( has_post_thumbnail() ) {  
				                        	the_post_thumbnail();  
				          				  }
				          				else {
				          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
				          				  }
			          				  ?> 
		         					<div class="ribbon">
										  <span class="ribbon2"><?php echo tribe_get_start_date( $post, false, 'M' );?> <br><strong><?php echo tribe_get_start_date( $post, false, 'j' );?> </strong></span>
										</div>
		         				    </div>
		         				</div>
			         		</a> 	
						</div>
						<div class="col-12 col-md-12 spacer">
							<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
							<!-- <p><span>Events</span>  <?php //echo tribe_get_start_date( $post, false, 'M, d Y');?></p> -->
							<p class="sortcontent">
								<?php $content = get_the_content();
					                  $content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,270); ?>...
							</p>	
							<a href="<?php the_permalink(); ?>" class="enlink">See Details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				                           
				        </div>
				      
				     

					<?php } }else {
						echo "No Upcomin Events";
					}?>
					<?php wp_reset_query(); ?>
		         			

		         		 <div class="col-12 col-md-12"><a href="<?php echo esc_url( home_url( '/' ) ); ?>events" class="btn">see all events</a>  </div>     	
				    
				</div>	
</div>
			   		</div>
			   	</div>
			</section>

		<?php elseif( get_row_layout() == 'links_departments_programs'):?>
			
			<?php $section_heading_icon = get_sub_field('section_heading_icon');
			$section_heading = get_sub_field('section_heading');
			$section_description = get_sub_field('section_description');?>
			<?php if( !empty($section_heading_icon) || !empty($section_heading)  || !empty($section_description)):?>
				<section class="bluecontainer featuredbox-sec">			
				<div class="row">
					<div class="col-md-12 text-center">
						<?php if( !empty($section_heading_icon)):?><img src="<?php echo $section_heading_icon['url'];?>"><?php endif;?> 
						<?php if( !empty($section_heading)):?><h2><?php echo $section_heading; ?></h2><?php endif;?>
						<?php if( !empty($section_description)):?><p><?php echo $section_description; ?></p><?php endif;?>
					</div>
				</div>			 	
				</section>
			<?php endif;?>

			<section class="inner-container visitlink-sec">
				<div class="row justify-content-center display-flex">
					<?php if( have_rows('link_repeater') ): 
					while ( have_rows('link_repeater') ) : the_row();
					$icon = get_sub_field('icon');
					$title = get_sub_field('title'); 
					$description = get_sub_field('description');
					$button_text = get_sub_field('button_text'); 
					$button_url = get_sub_field('button_url');?>
						<div class="col-md-3 text-center">
							<div class="visitlinkdiv">
								<?php if( !empty($icon)):?><img src="<?php echo $icon['url']; ?>" class="svg" id="logo"><?php endif;?>
								<?php if( !empty($title)):?><h3><?php echo $title;?></h3><?php endif;?>
								<?php if( !empty($description)):?><p><?php echo $description;?></p>	<?php endif;?>					
								<?php if( !empty($button_url)):?><a href="<?php echo $button_url;?>" class="btn"><?php echo $button_text;?></a><?php endif;?>
							</div>	
						</div>
					 <?php endwhile; endif;?>
				</div>	
			</section>
		
		<?php elseif( get_row_layout() == 'give_back_volunteer_content'):?>
			<section class="inner-container join-our-team communicationpart">
					<div class="row">	
					    <div class="col-lg-8">
						<?php $content_title = get_sub_field('content_title');
					   		  $content_are = get_sub_field('content_are');
					   		  $button_text = get_sub_field('button_text');
					   		  $button_url = get_sub_field('button_url');
					   		  $content_sub_title = get_sub_field('content_sub_title');
					   		  $description = get_sub_field('description'); 
					   		  ?>
					    	<?php if( !empty($content_title)):?><h2><?php echo $content_title; ?></h2><?php endif;?>
					    	<?php if( !empty($content_are)):?><?php echo $content_are; ?><?php endif;?>
					    	  
					    	<?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>

							<div class="short-contentpart">
								<?php if( !empty($content_sub_title)):?><h2><?php echo $content_sub_title; ?></h2><?php endif;?>
								<?php if( !empty($description)):?><?php echo $description; ?><?php endif;?>
							</div>	
					    	<ul class="linkpart">
							<?php if( have_rows('link_repeater') ): 
								while ( have_rows('link_repeater') ) : the_row();
									$text = get_sub_field('text'); 
									$link = get_sub_field('link'); ?>
									<?php if( !empty($link)):?><li><a href="<?php echo $link; ?>" target="_blank"> <?php echo $text; ?><span class="link-arrow"></span></a></li><?php endif;?>
								<?php endwhile; endif;?>
							</ul>

					    		
					    </div>	
					    <div class="col-lg-4 sidebar-blue-section">
							<div class="blue-border-box one">
								 <?php $sidbar_main_title = get_sub_field('sidbar_main_title');
					   		  $sidebar_title = get_sub_field('sidebar_title'); 
					   		  $sidebar_sub_title = get_sub_field('sidebar_sub_title'); ?>
								<?php if( !empty($sidbar_main_title)):?><div class="ribbonpart"><label><?php echo $sidbar_main_title; ?></label></div><?php endif;?>
								<?php if( !empty($sidebar_title)):?><h3><?php echo $sidebar_title; ?></h3><?php endif;?>
								<?php if( !empty($sidebar_sub_title)):?><h2><?php echo $sidebar_sub_title; ?></h2><?php endif;?>

								<div id="microsoftcounter">
									<?php if( have_rows('do_you_know_reapeter') ): $i=1;
									while ( have_rows('do_you_know_reapeter') ) : the_row();
									$icon = get_sub_field('icon'); 
									$number = get_sub_field('number');
									$description = get_sub_field('description'); ?>
										<?php if( !empty($sidebar_sub_title)):?>
											<div class="iconpart">
												<img src="<?php echo $icon['url']; ?>">
											</div>	
										<?php endif;?>
										<div class="counterpart">
								        	<?php if( !empty($number)):?><div class="count" data-count="<?php echo $number; ?>">0</div><?php endif;?><div class="extention"><?php if($i == 2) {echo "nd"; } else {echo "+";}?></div>
								       <?php if( !empty($description)):?> <label><?php echo $description; ?></label><?php endif;?>
								        </div>

								    <?php $i++; endwhile; endif;?>
								</div>	
							</div>
					    </div>	
					</div>    
		</section>

		<?php elseif( get_row_layout() == 'general_news_events'):?>

		<section class="inner-container newspart">	
		<?php $news_event_section_title = get_sub_field('news_&_events_title');?>	
	         <?php if( !empty($news_event_section_title)):?>
		         <div class="ss-style-triangles news">		   
		         	<h2><?php echo $news_event_section_title; ?> </h2>	
		         </div>	 
		         
	         <?php endif;?>	
	        
	        <div class="newspart">
		        <div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Recent news</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link myborder"><a href="<?php echo esc_url( home_url( '/' ) ); ?>news">see all</a></div>
		         	</div>		
		        </div>

		         <div class="row">
         			<?php 			     	  
	       			  $args = array( 'post_type' => 'news', 'posts_per_page' => 4,  'orderby' => 'DESC');

	        		  $loop = new WP_Query( $args );
	        		  if ( $loop->have_posts() ) {
	       			  while ( $loop->have_posts() ) : $loop->the_post();?>
	       		
						<div class="col-12 col-md-12 col-lg-12 col-xl-6">
		         		<div class="row">
		         			<div class="col-12 col-md-8 spacer">
		         				<a href="<?php the_permalink();?>">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         						<?php	if ( has_post_thumbnail() ) {  
								                        	the_post_thumbnail();  
								          		}elseif( get_theme_mod( 'news_default_image' ) ){
								          						 
					          						echo '<img src="' .get_theme_mod( 'news_default_image' ).'" />';
													} else {

					          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
								          					
								          			}
								          ?>
			         				    </div>
		         					</div>	
		         			 	</a>
		         			</div>	
		         			<div class="col-12 col-md-4 spacer">
		         				<a href="<?php the_permalink();?>"><h2><?php the_title();?></h2></a>
		         				<?php $tags =  get_the_tags();?>
		         				<p><span style="color: <?php the_field('tag_color',  'term_'.$tags[0]->term_id); ?>;"><?php  echo $tags[0]->name; ?></span>  <?php echo get_the_date('M. j Y');?></p>
		         				<p class="sortcontent">
		         					<?php $content = get_the_content();
            						$content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,116); ?>...
		         				</p>	
		         				<a href="<?php the_permalink();?>" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         		</div>	

			   		<?php  wp_reset_query(); endwhile; 
			   		} else {
						echo "No News Found";
					}?>

		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link"><a href="<?php echo esc_url( home_url( '/' ) ); ?>news">see all</a></div>
		         	</div>
		         </div>

	     	</div>
	    	
	    	

	     	<div class="eventpart myborder">
	     		<div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Events</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link"><a href="<?php echo esc_url( home_url( '/' ) ); ?>events">see all</a></div>
		         	</div>	
		        </div>

		        <div class="row">
		        	<?php 			     	  
	       			  $args = array( 'post_type' => 'tribe_events', 'posts_per_page' => 2, 'orderby' => 'DESC');

	        		  $loop = new WP_Query( $args );
	        		  if ( $loop->have_posts() ) {
	       			  while ( $loop->have_posts() ) : $loop->the_post();?>
			         	<div class="col-12 col-md-12 col-lg-12 col-xl-6">
			         		<div class="row">
			         			<div class="col-12 col-md-8 spacer">
			         				<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <?php	if ( has_post_thumbnail() ) {  
								                        	the_post_thumbnail();  
								          		}elseif( get_theme_mod( 'event_default_image' ) ){
								          						 
					          						echo '<img src="' .get_theme_mod( 'event_default_image' ).'" />';
													} else {

					          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
								          					
								          			}
								          ?>  
			         					  <div class="ribbon">
											 <span class="ribbon2">
											  	<?php echo tribe_get_start_date( $post, false, 'M' );?> <br><strong><?php echo tribe_get_start_date( $event, false, 'j' );?> </strong></span>
											</div>
			         				    </div>
			         				</div>	
			         			</div>	
			         			<div class="col-12 col-md-4 spacer">
			         				<a href="<?php the_permalink();?>"><h2><?php the_title();?></h2></a>		        				
			         				<p class="sortcontent">
			         					<?php $content = get_the_content();
	                    						$content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,130); ?>...
			         				</p>	
			         				<a href="<?php the_permalink();?>" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
			         			</div>	
			         		</div>
			         	</div>

			   		<?php  wp_reset_query(); endwhile; 
			   		} else {
						echo "No Events Found";
					}?>
		         			
		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link myborder"><a href="<?php echo esc_url( home_url( '/' ) ); ?>events">see all</a></div>
		         	</div>
		         </div>
	     	</div>
	     	
		</section>


		<?php elseif( get_row_layout() == 'slider'):?>

		<?php $li = get_sub_field('slider_shortcode'); ?>
		<?php echo do_shortcode($li); ?>

		<?php elseif( get_row_layout() == 'custom_blocks_with_text'):?>

		<section class="yellowadvance">
			<div class="content-conainer">
				<div class="row justify-content-center display-flex">
					<?php if( have_rows('boxs') ): 
					while ( have_rows('boxs') ) : the_row();
					$box_title = get_sub_field('box_title'); 
					$box_description = get_sub_field('box_description'); 
					$button_text = get_sub_field('button_text'); 
					$button_url = get_sub_field('button_url'); ?>
						<div class="col-md-4">
							<div class="darkshade">
							<?php if( !empty($box_title)):?>
								<h2 class="no-background">
									<span><?php echo $box_title; ?></span>					
							    </h2>
							<?php endif;?>
						    <?php if( !empty($box_description)):?><p><?php echo $box_description; ?></p><?php endif;?>
						   
						     <?php if( !empty($button_url)):?><div class="text-right"><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a></div><?php endif;?>	
							</div>
						</div>
					<?php endwhile; endif;?>					
			</div>
		</section>

		<?php elseif( get_row_layout() == 'school_map_section'):
			$map_title = get_sub_field('map_title'); 
			$map_image = get_sub_field('map_image'); 
			$map_url = get_sub_field('map_url'); ?>

		<section class="inner-container blue-section school-map">
			<?php if( !empty($map_title)):?><div class="ribbonpart"><label><?php echo $map_title;?></label></div><?php endif;?>
			<div class="blue-border-box" id="one">
				
				<a href="<?php echo $map_url;?>" target="_blank"><img src="<?php echo $map_image['url'];?>" alt="" /></a>
				 
			</div>
		</section>

		<?php elseif( get_row_layout() == 'search_school_section'):
			$main_title = get_sub_field('main_title'); 
			$sub_title = get_sub_field('sub_title'); 
			$road_image = get_sub_field('road_image'); 
			$left_title = get_sub_field('left_title');
			$left_button_text = get_sub_field('left_button_text');
			$left_button_url = get_sub_field('left_button_url');
			$right_title = get_sub_field('right_title');
			$right_button_text = get_sub_field('right_button_text');
			$right_button_url = get_sub_field('right_button_url'); ?>

		<section class="bluecontainer map-section">
			<div class="inner-container">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php if( !empty($main_title)):?><h2><?php echo $main_title;?></h2><?php endif;?>
						<?php if( !empty($sub_title)):?><p><?php echo $sub_title;?></p><?php endif;?>
					</div>
				</div>
				<div class="row align-items-center justify-content-center">
					<div class="col-md-3 text-center no-padding">

						<?php if( !empty($left_title)):?><h4><?php echo $left_title;?></h4><?php endif;?>
						<?php if( !empty($left_button_url)):?><a href="<?php echo $left_button_url;?>" class="btn"><?php echo $left_button_text;?></a><?php endif;?>

					</div>
					<div class="col-md-6 text-center no-padding">
						<?php if( !empty($left_title)):?><img src="<?php echo $road_image['url']; ?>" alt="" /><?php endif;?>
					</div>
					<div class="col-md-3 text-center">
						<?php if( !empty($right_title)):?><h4><?php echo $right_title;?></h4><?php endif;?>
						<?php if( !empty($right_button_url)):?><a href="<?php echo $right_button_url;?>" class="btn" target="_blank"><?php echo $right_button_text;?></a><?php endif;?>
					</div>
				</div>
			</div>
		</section>


		<?php elseif( get_row_layout() == 'policies_forms_school_accordion'):
			$custom_post_type = get_sub_field('custom_post_type'); 

 			if($custom_post_type == "Policies"){
 				$post_type =  "ebrpolicies";
 			} elseif($custom_post_type == "Forms"){
 				$post_type =  "ebrforms";
 			} elseif($custom_post_type == "Schools"){
 				$post_type =  "ebrschools";
 			}
			?>
		<?php if($custom_post_type == "Schools"){?>

		<section class="inner-container accordianpart">		
		    <div id="accordion" class="accordion schools-accordion">
		        <div class="card mb-0">
		        	<?php if( have_rows('accordion') ): $i = 1;
					while ( have_rows('accordion') ) : the_row();
					$accordion_icon = get_sub_field('accordion_icon'); 
					$accordion_title = get_sub_field('accordion_title'); 
					$category = get_sub_field('category'); 
	    			  $terms = get_term($category); 
			     	  $category_name = $terms->name;
			     	  ?>

			            <div class="card-header collapsed" data-toggle="collapse" href="#collapse<?php echo $i;?>">
			                <div class="svgsection">
			                	<?php if( !empty($accordion_icon)):?><img src="<?php echo $accordion_icon['url']; ?>" class="svg" id="logo" /><?php endif;?>
			                </div>
			                <?php if( !empty($accordion_title)):?>
			                	<a class="card-title"><?php echo $accordion_title; ?></a>
			                <?php endif;?>
			            </div>
			            <div id="collapse<?php echo $i;?>" class="card-body collapse" data-parent="#accordion" >	<div class="card-body-panel">
			            	<div class="row justify-content-center">
									<div class="col-md-12">
										<h4><?php echo $category_name;?></h4>
									</div>
								</div>
								<div class="row justify-content-center display-flex">
			            	<?php 			     	  
			       			  $args = array( 'post_type' => $post_type, 'posts_per_page' => -1, 'category_name' => $category_name, 'orderby' => 'DESC');

			        		  $loop = new WP_Query( $args );
			        		  if ( $loop->have_posts() ) {
			       			  while ( $loop->have_posts() ) : $loop->the_post(); ?>
			       				
								<div class="col-md-6">
									<div class="school-info-block">
										<h5 class="school-name"><?php echo get_the_title();?></h5>
										<div class="row align-items-end">
											<div class="col-xl-9">
											<?php if( have_rows('flexible_content_sections') ): 
											while ( have_rows('flexible_content_sections') ) : the_row(); ?>

												<?php if( get_row_layout() == 'contact_details' ):?>
												<?php $address = get_sub_field('address'); 
												$fax = get_sub_field('fax');

												?>
												<?php if( !empty($address)):?><p class="address">
													<?php echo $address; ?>
												</p>
											<?php endif;?>

												<p>
													<?php if( have_rows('phone_number') ): $j=1;
													while ( have_rows('phone_number') ) : the_row();
													 $phone = get_sub_field('phone'); ?>

													<?php if( !empty($phone)):?>
														
															<?php if($j == 1){?><strong>PH:</strong><?php } ?>
															<?php if($j != 1){?> | <?php } ?>
															<a href="tel:<?php echo $phone;?>"><?php echo $phone;?></a>   
													<?php endif;?>
													<?php $j++; endwhile; endif;?>

													<?php if( !empty($fax)):?><strong>FAX:</strong> <?php echo $fax; ?><?php endif;?>
												</p>

											<?php endif; endwhile; endif;?>

											</div>
											<div class="col-xl-3">
												<a href="<?php echo get_the_permalink(); ?>" class="btn">LEARN MORE</a>
											</div>
										</div>
									</div>
								</div>

					   		<?php  wp_reset_query(); endwhile; 
					   		} else {
								echo "Not Found";
							}?>
			               </div>
			            </div>
			            </div>
		            <?php $i++; endwhile; endif;?>	
		        </div>

		    </div>		
		</section>
 				
 		<?php } else {	?>

		<section class="inner-container accordianpart">		
		    <div id="accordion" class="accordion">
		        <div class="card mb-0">
		        	<?php if( have_rows('accordion') ): $i = 1;
					while ( have_rows('accordion') ) : the_row();
					$accordion_icon = get_sub_field('accordion_icon'); 
					$accordion_title = get_sub_field('accordion_title'); 
					$category = get_sub_field('category'); 
	    			  $terms = get_term($category); 
			     	  $category_name = $terms->name;
			     	  ?>

			            <div class="card-header collapsed" data-toggle="collapse" href="#collapse<?php echo $i;?>">
			                <div class="svgsection">
			                	<?php if( !empty($accordion_icon)):?><img src="<?php echo $accordion_icon['url']; ?>" class="svg" id="logo" /><?php endif;?>
			                </div>
			                <?php if( !empty($accordion_title)):?>
			                	<a class="card-title"><?php echo $accordion_title; ?></a>
			                <?php endif;?>
			            </div>
			            <div id="collapse<?php echo $i;?>" class="card-body collapse" data-parent="#accordion" >
			            	<?php 			     	  
			       			  $args = array( 'post_type' => $post_type, 'posts_per_page' => -1, 'category_name' => $category_name, 'orderby' => 'DESC');

			        		  $loop = new WP_Query( $args );
			        		  if ( $loop->have_posts() ) {
			       			  while ( $loop->have_posts() ) : $loop->the_post();
			       			  		$attachment_title = get_field('attachment_title');
			       			  		$attachment_url = get_field('attachment_url');
			       			  	?>
			       			
								<?php if( !empty($attachment_url)):?><p class="smalldetail"><a href="<?php echo $attachment_url;?>" target="_blank"><i class="fa fa-file-pdf-o " aria-hidden="true"></i> <?php echo $attachment_title; ?></a></p>
								 <hr/><?php endif;?>

					   		<?php  wp_reset_query(); endwhile; 
					   		} else {
								echo "Not Found";
							}?>
			               
			            </div>

		            <?php $i++; endwhile; endif;?>	
		        </div>
		    </div>		
		</section>

		<?php } ?>

		<?php elseif( get_row_layout() == 'parallax_green_box'):
			$background_image = get_sub_field('background_image');
			$alignment = get_sub_field('alignment');?>

			<section class="parallaxpart <?php if($alignment == '3 Column'){ echo 'threebox';}?>" style="background-image: url(<?php echo $background_image['url']; ?>">
			<div class="inner-container">
			<div class="row justify-content-center display-flex">
				<?php if( have_rows('boxs') ): 
					while ( have_rows('boxs') ) : the_row();
					$box_title = get_sub_field('box_title'); 
					$box_description = get_sub_field('box_description'); 
					$button_text = get_sub_field('button_text'); 
					$button_url = get_sub_field('button_url'); ?>
					<?php if($alignment == "2 Column"){?>
					<div class="col-md-6">
						<div class="greenbox">
							    <?php if( !empty($box_title)):?>
									<h2 class="no-background">
										<span><?php echo $box_title; ?></span>					
								    </h2>
								<?php endif;?>
						    <div class="row">
							    <div class="col-lg-8">
							    	<?php if( !empty($box_description)):?><p><?php echo $box_description; ?></p><?php endif;?>
							    </div>
							    <div class="col-lg-4">
							    	 <?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>
							    </div>	
							</div>    
						</div>	
					</div>	

					<?php } elseif($alignment == "3 Column"){?>

						<div class="col-md-6 col-lg-4">
							<div class="greenbox">
								<?php if( !empty($box_title)):?><h2><?php echo $box_title; ?></h2><?php endif;?>
								<hr>	
								<?php if( !empty($box_description)):?><p class="text-center"><?php echo $box_description; ?></p><?php endif;?>			    									
								<?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>					   
							</div>    
						</div>

					<?php }?>
 
				<?php endwhile; endif;?>
			</div>
			</div>
		</section>

		<?php elseif( get_row_layout() == 'custom_blocks_with_text_title'):
			$background_image = get_sub_field('background_image');
			$title = get_sub_field('title');  ?>

			<section class="parallaxpart " style="background-image: url(<?php echo $background_image['url']; ?>">
			<?php if( !empty($title)):?><div class="ribbonpart"><label><?php echo $title; ?></label></div><?php endif;?>
			<div class="inner-container <?php if(is_page('92')){?>my-margi<?php }?>">
			<div class="row justify-content-center display-flex">
				<?php if( have_rows('boxs') ): 
					while ( have_rows('boxs') ) : the_row();
					$box_title = get_sub_field('box_title'); 
					$box_description = get_sub_field('box_description'); 
					$button_text = get_sub_field('button_text'); 
					$button_url = get_sub_field('button_url'); ?>
						<div class="col-md-6 <?php if(is_page('92')){?>col-lg-6<?php } else {?>col-lg-4<?php }?>">
							<div class="greenbox">
							<?php if( !empty($box_title)):?>
								<h2 class="no-background">
									<span><?php echo $box_title; ?></span>					
							    </h2>
							<?php endif;?>
						    <?php if( !empty($box_description)):?><p><?php echo $box_description; ?></p><?php endif;?>
						   
						     <?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn rightside"><?php echo $button_text; ?></a><?php endif;?>	
							</div>
						</div>
					<?php endwhile; endif;?>					

				</div>	
			</div>	
			</div>
		</section>

		<?php elseif( get_row_layout() == 'gallery'):
			$gallery_title = get_sub_field('gallery_title');
			$gallery_description = get_sub_field('gallery_description');
			$images = get_sub_field('images'); ?>
			
			<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<?php if( !empty($gallery_title)):?><label><?php echo $gallery_title; ?></label><?php endif;?>	
						<?php if( !empty($gallery_description)):?><p><?php echo $gallery_description; ?></p><?php endif;?>	
					</div>	
				</div>		
			</section>
			<div class="inner-container fancy-gallery-part">	
			<div class="container-fluid">		
				<div class="row justify-content-center">

					<?php $i = 1; foreach ($images as $image) {?>
				        <div class="col-md-3 text-center <?php if($i > 8){ echo "d-none";}?>">	
				            <div class="gallery">	            	
				                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $image['url']; ?>">
				                    <img class="img-responsive" alt="" src="<?php echo $image['url']; ?>" />	
				                </a>
				            </div> 	           
				    	</div> 
			    	<?php $i++;} ?>	
					</div> 
				</div>
			</div>	
			

		
		<?php elseif( get_row_layout() == 'custom_blocks_with_full_width'):?>

		<section class="yellowadvance yfixed">
			<div class="content-conainer">
				<div class="row justify-content-center display-flex">
					<?php if( have_rows('boxs') ): $i =1;
					while ( have_rows('boxs') ) : the_row();
					$box_title = get_sub_field('box_title'); 
					$box_description = get_sub_field('box_description'); 
					 ?>
						<div class="<?php if($i == 3){echo "col-md-12"; } else {echo "col-md-6";}?>">
							<div class="darkshade">
							<?php if( !empty($box_title)):?>
								<h2 class="no-background">
									<span><?php echo $box_title; ?></span>					
							    </h2>
							<?php endif;?>
						    <?php if( !empty($box_description)):?><p><?php echo $box_description; ?></p><?php endif;?>
							</div>
						</div>
					<?php $i++; endwhile; endif;?>					
			</div>
		</section>

		<?php elseif( get_row_layout() == 'content_area_with_1_image'):
				$image_alignment = get_sub_field('image_alignment'); 
				$image = get_sub_field('image'); 
				$main_title = get_sub_field('main_title'); 
				$content = get_sub_field('content'); 
				$button_text = get_sub_field('button_text'); 
				$button_url = get_sub_field('button_url'); 
				$name = get_sub_field('name'); 
				$sub_title = get_sub_field('sub_title'); ?>

				<?php if($image_alignment == "Top"){?>

					<?php if( !empty($image)):?><div class="overlapcontent" style="background-image: url(<?php echo $image['url']; ?>);"><?php endif;?>	
					</div>	
					<div class="overlaptextarea">
						<div class="overalappart inner-container">		
								<div class="row paragraphpart">
									<div class="col-md-12 text-center">
										<?php if( !empty($main_title)):?><h1><?php echo $main_title; ?></h1><?php endif;?>	
										<?php if( !empty($name)):?><p><label><?php echo $name; ?></label></p>
										<hr><?php endif;?>
										<?php if( !empty($sub_title)):?><h4><?php echo $sub_title; ?></h4><?php endif;?>
									</div>
								</div>
								<?php if( !empty($content)):?><?php echo $content; ?><?php endif;?>
								<?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>	
						</div>
					</div>
				<?php } else {?>

					<section class="areapart-img">	
						<div class="row">
							<div class="col-lg-6 leftimg <?php if($image_alignment == 'Right'){ echo 'order-2'; }?>">
								<img src="<?php echo $image['url']; ?>">
							</div>	
							<div class="col-lg-6">
								<div class="slide-description">
									<?php if( !empty($main_title)):?><h1><?php echo $main_title; ?></h1><?php endif;?>
				       				<?php if( !empty($name)):?><p><label><?php echo $name; ?></label></p>
									<?php endif;?>
				       				<hr>
									<?php if( !empty($sub_title)):?><h4><?php echo $sub_title; ?></h4><?php endif;?>
									<?php if( !empty($content)):?><p><?php echo $content; ?></p><?php endif;?>
									<?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>
								</div>
							</div>	
						</div>	
					</section>

				<?php }?>



		<?php elseif( get_row_layout() == 'content_area_with_2_image'):
				$with_background = get_sub_field('with_background'); 
				$main_title = get_sub_field('main_title'); 
				$sub_title = get_sub_field('sub_title'); 
				$content = get_sub_field('content'); 
				$button_text = get_sub_field('button_text'); 
				$button_url = get_sub_field('button_url'); 
				$first_image = get_sub_field('first_image'); 
				$second_image = get_sub_field('second_image'); 
			?>

		<section class="did-you-konw-section <?php if($with_background == "Yes"){ echo 'blue_bg'; }?>" style="<?php if($with_background == "Yes"){ echo 'background-color: #002e59'; }?>">
			<div class="inner-container">		
				<div class="row">
					<div class="col-md-8">
						<?php if( !empty($main_title)):?><h2 class="no-background"><span><?php echo $main_title; ?></span></h2><?php endif;?>	
						<?php if( !empty($sub_title)):?><h3><?php echo $sub_title; ?></h3><?php endif;?>
						<?php if( !empty($content)):?><p><?php echo $content; ?></p><?php endif;?>
						<?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>
					</div>
					<div class="col-md-4">
						<div class="image-thum-block">
							<?php if( !empty($first_image)):?><img src="<?php echo $first_image['url']; ?>" alt="" class="primary-image"><?php else:?>
									<img src="<?php echo $path."/img/nofound.png"; ?>" class="primary-image"><?php endif;?>
							<?php if( !empty($second_image)):?><img src="<?php echo $second_image['url']; ?>" alt="" class="secondary-image"><?php else:?>
									<img src="<?php echo $path."/img/nofound.png"; ?>" class="secondary-image"><?php endif;?>
						</div>
					</div>
				</div>	
			</div>		
		</section>

		<?php elseif( get_row_layout() == 'wall_of_fame'):
				$wall_title = get_sub_field('wall_title'); 
				$wall_sub_title = get_sub_field('wall_sub_title'); 
				$wall_descriptiom = get_sub_field('wall_descriptiom');?>

			<section class="inner-container wall-of-fame">
			 <div class="row">
		         	<div class="col-12 text-center">
		         		<?php if( !empty($wall_title)):?><label><?php echo $wall_title; ?></label><?php endif;?>
		         		<?php if( !empty($wall_sub_title)):?><h2><?php echo $wall_sub_title; ?></h2><?php endif;?>
		         		<?php if( !empty($wall_descriptiom)):?><p><?php echo $wall_descriptiom; ?></p><?php endif;?>
		         	</div>		         		
		        </div>
				<div class="d-none d-lg-block">
			        <div class="row justify-content-center m_t30">
			        	<?php if( have_rows('wall_members') ): $i =1;
						while ( have_rows('wall_members') ) : the_row();
						$profile_image = get_sub_field('profile_image'); 
						$name = get_sub_field('name');
						$designation = get_sub_field('designation');
						$year = get_sub_field('year'); ?>	
						<div class="col-md-2 text-center <?php if($i > 12){ echo "d-lg-none";}?>">
							<div class="famediv">
								<?php if( !empty($profile_image)):?><img src="<?php echo $profile_image['url']; ?>"><?php else:?>
									<img src="<?php echo $path."/img/nofound.png"; ?>">
								<?php endif;?>
								<?php if( !empty($name)):?><h3><?php echo $name; ?></h3><?php endif;?>
								<?php if( !empty($designation)):?><span><?php echo $designation; ?></span><?php endif;?>
								<?php if( !empty($year)):?><p><?php echo $year; ?></p><?php endif;?>
							</div>	
						</div>	
						<?php $i++; endwhile; endif;?>	
					</div>
				</div>
				<div class="d-block d-lg-none">
					<div class="slider slider-nav">
						<?php if( have_rows('wall_members') ):
						while ( have_rows('wall_members') ) : the_row();
						$profile_image = get_sub_field('profile_image'); 
						$name = get_sub_field('name');
						$designation = get_sub_field('designation');
						$year = get_sub_field('year'); ?>
					    <div>
					    	<div class="row">
						    	<div class="col-md-12 text-center">
									<div class="famediv">
									<?php if( !empty($profile_image)):?><img src="<?php echo $profile_image['url']; ?>"><?php else:?>
									<img src="<?php echo $path."/img/nofound.png"; ?>"><?php endif;?>
								<?php if( !empty($name)):?><h3><?php echo $name; ?></h3><?php endif;?>
								<?php if( !empty($designation)):?><span><?php echo $designation; ?></span><?php endif;?>
								<?php if( !empty($year)):?><p><?php echo $year; ?></p><?php endif;?>
									</div>	
								</div>	
						 </div>
					   </div>
					   <?php endwhile; endif;?>
				</div>		
		</section>

		<?php elseif( get_row_layout() == 'timeline'):
			$timeline_background = get_sub_field('timeline_background'); 
				$timeline_title = get_sub_field('timeline_title');
				$button_text = get_sub_field('button_text');
				$button_url = get_sub_field('button_url'); 
				 ?>

			<section class="community" style="background-image: url(<?php echo $timeline_background['url']; ?>);">
			<div class="inner-container">
			 <div class="row">
		         	<div class="col-12 text-center">
		         		<?php if( !empty($timeline_title)):?><label><?php echo $timeline_title; ?></label><?php endif;?>
		         		<?php if( have_rows('timeline_blocks') ):
						while ( have_rows('timeline_blocks') ) : the_row();
							$in_year = get_sub_field('in_year'); 
							$Year_description = get_sub_field('Year_description'); 
							?>
		         		<hr>
		         		<?php if( !empty($in_year)):?><h2><?php echo $in_year; ?></h2><?php endif;?>
		         		<?php if( !empty($Year_description)):?><p><?php echo $Year_description; ?></p><?php endif;?>
		         		<?php endwhile; endif;?>
		         		<hr> 
		         		<?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>
		         	</div>		         		
		        </div>
		    </div>    
		</section>

		<?php elseif( get_row_layout() == 'bottom_about_our_menu'):
			$section_title = get_sub_field('section_title');?>
		<section class="selectionpart">
				<?php if( !empty($section_title)):?><div class="title"><?php echo $section_title; ?></div><?php endif;?>
					<?php $section_title = get_sub_field('section_title'); ?>
				<div class="bottom_menu"><ul>
				<?php if( have_rows('links') ): 
				while ( have_rows('links') ) : the_row();
					$link_text = get_sub_field('link_text');
					$link_url = get_sub_field('link_url');
					$current_page = get_sub_field('current_page');
					?>
					<?php if( !empty($link_url)):?><li class="<?php if($current_page[0] == "Yes"){echo "active"; }?>"><a href="<?php echo $link_url; ?>" ><?php echo $link_text; ?></a></li><?php endif;?>
					
				<?php endwhile; endif;?>
				</ul>
				</div>
		</section>	
		
		<?php endif; ?>
<?php 
    endwhile;
else :
   echo 'No Row Found';
endif;
?>
		<div class="clerfix"></div>
		</div>
	</div><!-- #content -->
</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
