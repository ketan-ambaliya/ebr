<?php
/**
 * Template Name: Enroll Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		<!-- <section class="overlay breadcrumbs " style="background-image: url(<?php echo $path; ?>/img/breadcrumb.jpg);">
			<div class="container-fluid">
				<div class="row">					
					<div class="col-md-12">
					 <ul>
					 	<li><a href="">Enroll Now</a></li>
					 </ul>					
					</div>
				</div>
			</div>
		</section> -->

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<img src="<?php echo $path; ?>/img/sducation-cap-gren.png" />
						<label>Enrollment</label>
					</div>	
				</div>		
		</section>

		<section class="bluecontainer enroll">	
			<div class="inner-container">	
				<div class="row">
					<div class="col-md-12 text-center">						
						<label>ABC...</label>
						<h3>Easy as : </h3>
						<p>Enrolling a studens is a multi-steps process. See some of the things you can to do get started!</p>
						<div class="processBox" id="microsoftcounter">
							<div class="processBoxCont">
								<div class="imgBoxMain">
									<div class="imgBox">
							 	      <img src="<?php echo $path; ?>/img/one.png">
							        </div> 
							        <div class="counterpart">
							        	<label>Identification</label>
							        	<!-- <div class="count" data-count="41000">0</div><div class="extention">+</div> -->
							        <div class="clerfix"></div>
							        </div>
							       <!--  <label>Students</label> -->							       
								</div>
							</div>
							<div class="processBoxCont">
								<div class="imgBoxMain">
									<div class="imgBox">
							 	      <img src="<?php echo $path; ?>/img/2.png">
							        </div> 
							        <div class="counterpart">
							        	<label>Proof of residency</label>
							        	<!-- <div class="count" data-count="41000">0</div><div class="extention">+</div> -->
							        <div class="clerfix"></div>
							        </div>
							       <!--  <label>Students</label> -->							      
								</div>
							</div>
							<div class="processBoxCont">
								<div class="imgBoxMain">
									<div class="imgBox">
							 	      <img src="<?php echo $path; ?>/img/3.png">
							        </div>
							        <div class="counterpart">
							        	<label>School History</label>
							        	<!-- <div class="count" data-count="41000">0</div><div class="extention">+</div> -->
							        <div class="clerfix"></div>
							        </div>
							       <!--  <label>Students</label> -->
							       
								</div>
							</div>
						
						</div>	
						<div class="clerfix"></div>
					</div>	
				</div>		
			</div>		
		</section>

		<section class="inner-container accordianpart">
			<h2 class="text-center">Step 1: Determine your school</h2>	
			<p class="text-center">To begin the enrollment process, view our list of schools below.
			    	</p>		
		    <div id="accordion" class="accordion">
		        <div class="card mb-0">
		        	<div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
		            	<div class="svgsection">
		                	<img src="<?php echo $path; ?>/img/abc-block.svg" class="svg" id="logo" />
		                </div>
		                <a class="card-title">EARLY CHILDHOOD PROGRAM FORMS</a>
		            </div>
		            <div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
		                <p class="smalldetail">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita sed repellat eligendi totam! Quam praesentium quibusdam, veniam porro ipsa possimus delectus quia asperiores inventore vitae alias sint quas nobis, consequatur.
		                </p>
		            </div>
		            
		            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
						<div class="svgsection">
							<img src="<?php echo $path; ?>/img/board-1.svg" class="svg" id="logo" />
						</div>
						<a class="card-title">elementary schools</a>
					</div>
					<div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
						<div class="card-body-panel">
							<p class="smalldetail">
								<div class="row justify-content-center">								
									<div class="col-md-12">
										<h2>SIXTH - EIGHTH Grade</h2>
									</div>
								</div>
								<div class="row justify-content-center">
									<div class="col-md-6">
										<div class="school-info-block">
											<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
											<div class="row align-items-end">
												<div class="col-md-9">
													<p class="address">
														1225 Sharp Road. Baton Rouge, LA 70815
													</p>

													<p>
														<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
													</p>
												</div>
												<div class="col-md-3">
													<a href="#" class="btn">LEARN MORE</a>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="school-info-block">
											<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
											<div class="row align-items-end">
												<div class="col-md-9">
													<p class="address">
														1225 Sharp Road. Baton Rouge, LA 70815
													</p>

													<p>
														<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
													</p>
												</div>
												<div class="col-md-3">
													<a href="#" class="btn">LEARN MORE</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="school-info-block">
											<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
											<div class="row align-items-end">
												<div class="col-md-9">
													<p class="address">
														1225 Sharp Road. Baton Rouge, LA 70815
													</p>

													<p>
														<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
													</p>
												</div>
												<div class="col-md-3">
													<a href="#" class="btn">LEARN MORE</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="school-info-block">
											<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
											<div class="row align-items-end">
												<div class="col-md-9">
													<p class="address">
														1225 Sharp Road. Baton Rouge, LA 70815
													</p>

													<p>
														<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
													</p>
												</div>
												<div class="col-md-3">
													<a href="#" class="btn">LEARN MORE</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</p>
						</div>
					</div>

		         <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
						<div class="svgsection">
							<img src="<?php echo $path; ?>/img/book.svg" class="svg" id="logo" />
						</div>
						<a class="card-title">middle schools</a>
					</div>
					<div id="collapseThree" class="card-body collapse" data-parent="#accordion" >
						<div class="card-body-panel">
							<p class="smalldetail">
								<div class="row justify-content-center">
									<div class="col-md-12">
										<h2>SIXTH - EIGHTH Grade</h2>
									</div>
								</div>
								<div class="row justify-content-center">
									<div class="col-md-6">
										<div class="school-info-block">
											<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
											<div class="row align-items-end">
												<div class="col-md-9">
													<p class="address">
														1225 Sharp Road. Baton Rouge, LA 70815
													</p>

													<p>
														<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
													</p>
												</div>
												<div class="col-md-3">
													<a href="#" class="btn">LEARN MORE</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="school-info-block">
											<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
											<div class="row align-items-end">
												<div class="col-md-9">
													<p class="address">
														1225 Sharp Road. Baton Rouge, LA 70815
													</p>

													<p>
														<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
													</p>
												</div>
												<div class="col-md-3">
													<a href="#" class="btn">LEARN MORE</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="school-info-block">
											<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
											<div class="row align-items-end">
												<div class="col-md-9">
													<p class="address">
														1225 Sharp Road. Baton Rouge, LA 70815
													</p>

													<p>
														<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
													</p>
												</div>
												<div class="col-md-3">
													<a href="#" class="btn">LEARN MORE</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="school-info-block">
											<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
											<div class="row align-items-end">
												<div class="col-md-9">
													<p class="address">
														1225 Sharp Road. Baton Rouge, LA 70815
													</p>

													<p>
														<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
													</p>
												</div>
												<div class="col-md-3">
													<a href="#" class="btn">LEARN MORE</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</p>
						</div>
					</div>
					<div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
						<div class="svgsection">
							<img src="<?php echo $path; ?>/img/education-cap.svg" class="svg" id="logo" />
						</div>
						<a class="card-title">HIGH SCHOOLS</a>
					</div>
					<div id="collapseFour" class="card-body collapse" data-parent="#accordion" >
						<div class="card-body-panel">
							<p class="smalldetail">
								<div class="row justify-content-center">
									<div class="col-md-12">
										<h2>SIXTH - EIGHTH Grade</h2>
									</div>
								</div>
								<div class="row justify-content-center">
									<div class="col-md-6">
										<div class="school-info-block">
											<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
											<div class="row align-items-end">
												<div class="col-md-9">
													<p class="address">
														1225 Sharp Road. Baton Rouge, LA 70815
													</p>

													<p>
														<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
													</p>
												</div>
												<div class="col-md-3">
													<a href="#" class="btn">LEARN MORE</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="school-info-block">
											<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
											<div class="row align-items-end">
												<div class="col-md-9">
													<p class="address">
														1225 Sharp Road. Baton Rouge, LA 70815
													</p>

													<p>
														<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
													</p>
												</div>
												<div class="col-md-3">
													<a href="#" class="btn">LEARN MORE</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="school-info-block">
											<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
											<div class="row align-items-end">
												<div class="col-md-9">
													<p class="address">
														1225 Sharp Road. Baton Rouge, LA 70815
													</p>

													<p>
														<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
													</p>
												</div>
												<div class="col-md-3">
													<a href="#" class="btn">LEARN MORE</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="school-info-block">
											<h5 class="school-name">MCKINLEY MIDDLE MAGNET SCHOOL</h5>
											<div class="row align-items-end">
												<div class="col-md-9">
													<p class="address">
														1225 Sharp Road. Baton Rouge, LA 70815
													</p>

													<p>
														<strong>PH:</strong> 225.272.0540  <strong>FAX:</strong> 225.272.0195
													</p>
												</div>
												<div class="col-md-3">
													<a href="#" class="btn">LEARN MORE</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</p>	
						</div>
					</div>
					<div class="card-header collapsed" data-toggle="collapse" href="#collapseFive">
		                <div class="svgsection">
		                	<img src="<?php echo $path; ?>/img/icon1.svg" class="svg" id="logo" />
		                </div>
		                <a class="card-title">
		                    View all schools in EBRprss
		                </a>
		            </div>
		            <div id="collapseFive" class="card-body collapse" data-parent="#accordion" >
		                <p class="smalldetail">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis facilis dolor quidem. Tempora nesciunt, quod impedit. Ipsam, voluptatem. Consequuntur eligendi quisquam, doloribus dolor aliquam magni quidem sint quas officiis quasi.
		                </p>
		            </div>
		        </div>
		    </div>		
		</section>
		

		<section class="bluecontainer enroll no-bg">		
				<div class="row">
					<div class="col-md-12 text-center">						
						<h2>Step 2: Get Prepared</h2>
						<p>Every school district requires certain documentation to enroll a student. Below are the list of some of required documents that you<br> will need in order to register a student:</p>
						<div class="processBox" id="microsoftcounter">
							<div class="processBoxCont">
								<div class="imgBoxMain">
									<!-- <div class="imgBox">
							 	      <img src="<?php echo $path; ?>/img/board.png">
							        </div> -->
							        <div class="counterpart">
							        	<label>Identification</label>
							        	<!-- <div class="count" data-count="41000">0</div><div class="extention">+</div> -->
							        <div class="clerfix"></div>
							        </div>
							       <!--  <label>Students</label> -->
							       <p class="sortdiscription">
							       	every applicant will need an official photo I.D
							       </p>
								</div>
							</div>
							<div class="processBoxCont">
								<div class="imgBoxMain">
									<!-- <div class="imgBox">
							 	      <img src="<?php echo $path; ?>/img/board.png">
							        </div> -->
							        <div class="counterpart">
							        	<label>Proof of residency</label>
							        	<!-- <div class="count" data-count="41000">0</div><div class="extention">+</div> -->
							        <div class="clerfix"></div>
							        </div>
							       <!--  <label>Students</label> -->
							       <p class="sortdiscription">
							       	A letter stating the apllicant's official residence and length of time lived there.
							       </p>
								</div>
							</div>
							<div class="processBoxCont">
								<div class="imgBoxMain">
									<!-- <div class="imgBox">
							 	      <img src="<?php echo $path; ?>/img/board.png">
							        </div> -->
							        <div class="counterpart">
							        	<label>School History</label>
							        	<!-- <div class="count" data-count="41000">0</div><div class="extention">+</div> -->
							        <div class="clerfix"></div>
							        </div>
							       <!--  <label>Students</label> -->
							       <p class="sortdiscription">
							       	A list of the applicant's previous school history.
							       </p>
								</div>
							</div>
							<div class="processBoxCont">
								<div class="imgBoxMain">
									<!-- <div class="imgBox">
							 	      <img src="<?php echo $path; ?>/img/board.png">
							        </div> -->
							        <div class="counterpart">
							        	<label>medical records</label>
							        	<!-- <div class="count" data-count="41000">0</div><div class="extention">+</div> -->
							        <div class="clerfix"></div>
							        </div>
							       <!--  <label>Students</label> -->
							       <p class="sortdiscription">
							       	The Applicant's Current Immunization Records.
							       </p>
								</div>
							</div>
						</div>	
						<div class="clerfix"></div>
						<a href="#" class="btn">full details enrollment form</a>
					</div>	
				</div>		
		</section>

		<section class="inner-container communicationpart">	
			<div class="row">	
			    <div class="col-lg-8">
			    	<h2>Step 3: Register</h2>	
			    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus cum esse, possimus dolorum neque perferendis cupiditate, nemo cumque, reiciendis animi tempore eaque qui deleniti enim quos unde autem eius veritatis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores deleniti quasi ex nam distinctio autem, accusantium illum consectetur. Accusantium, facere? Ipsum praesentium, ad neque earum in explicabo aliquam! Dolor, voluptates?<br> <br>
			    	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores deleniti quasi ex nam distinctio autem, accusantium illum consectetur. Accusantium, facere? Ipsum praesentium, ad neque earum in explicabo aliquam! Dolor, voluptates?<br> <br>
			    	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores deleniti quasi ex nam distinctio autem, accusantium illum consectetur. Accusantium, facere? Ipsum praesentium, ad neque earum in explicabo aliquam! Dolor, voluptates?<br> <br>
			    	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores deleniti quasi ex nam distinctio autem, accusantium illum consectetur. Accusantium, facere? Ipsum praesentium, ad neque earum in explicabo aliquam! Dolor, voluptates?
			    	</p>
			    	<a href="" class="btn">Download our pre-registration checklist</a>
			    </div>			  

				<div class="col-lg-4 sidebar-blue-section">
						<div class="blue-border-box one">
							<div class="ribbonpart"><label>Other important info</label></div>
							
							<label>Employee Resources:</label>
							<ul>
								<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> District Master Calendar</a></li>
								<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> TeachBR</a></li>
								<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Job Opportunities</a></li>
								<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Payroll login</a></li>
								<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> PASS</a></li>
							</ul>	
							<hr>
							<label>Relevant Departments:</label>
							<ul>
								<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Adult Education</a></li>
								<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Human Resources</a></li>
								<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Information Technology</a></li>
								<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Instructional Technology</a></li>
							</ul>
							<hr>
							<label>You may also interested in:</label>
							<ul>
								<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> All form</a></li>
								<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> all policies</a></li>
								<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Contact direstory</a></li>
								<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> All departments</a></li>
								<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> all programs</a></li>
							</ul>
						</div>
				</div>	
			</section>		
		</section>	

		<section class="parallaxpart" style="background-image: url(<?php echo $path; ?>/img/banner2.jpg);">
			<div class="inner-container">
			<div class="row justify-content-center">
				<div class="col-md-6">
					<div class="greenbox">
						<h2 class="no-background">
							<span>Departments</span>					
					    </h2>
					    <div class="row">
						    <div class="col-lg-8">
						    	<p>View a full list of our Departments.</p>
						    </div>
						    <div class="col-lg-4">
						    	<a href="" class="btn">Read more</a>
						    </div>	
						</div>    
					</div>	
				</div>	
				<div class="col-md-6">
					<div class="greenbox">
						<h2 class="no-background">
							<span>PROGRAMS</span>					
					    </h2>
					    <div class="row">
						    <div class="col-lg-8">
						    	<p>View the Programs EBR has to offer.</p>
						    </div>
						    <div class="col-lg-4">
						    	<a href="" class="btn">Read more</a>
						    </div>	
						</div>    
					</div>	
				</div>	
			</div>
		</div>
		</section>

		<section class="selectionpart">
			<div class="title">Other popular pages</div>			
		<div class="bottom_menu">
			<ul>
				<li><a href=""><i class="fa fa-book" aria-hidden="true"></i> Join our team</a></li>
				<li><a href=""><i class="fa fa-users" aria-hidden="true"></i> Get involved</a></li>
				<li class="active"><a href=""><i class="fa fa-graduation-cap" aria-hidden="true"></i> Enroll now</a></li>
				<li><a href=""><i class="fa fa-bus" aria-hidden="true"></i> Transportation</a></li>
			</ul>
		</div>
		</section>

		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
