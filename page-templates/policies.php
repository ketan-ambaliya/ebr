<?php
/**
 * Template Name: policies page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<label>Policies</label>
						<h3>For the East Baton Rouge Parish School System</h3>
						<p>Click to expand to view the forms for the district and each department.</p>
					</div>	
				</div>		
		</section>
		
		<section class="inner-container accordianpart">		
		    <div id="accordion" class="accordion">
		        <div class="card mb-0">
		            <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne">
		                <div class="svgsection">
		                	<img src="<?php echo $path; ?>/img/icon1.svg" class="svg" id="logo" />
		                </div>
		                <a class="card-title">
		                    DISTRICT-WIDE FORMS
		                </a>
		            </div>
		            <div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
		                <p class="smalldetail">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis facilis dolor quidem. Tempora nesciunt, quod impedit. Ipsam, voluptatem. Consequuntur eligendi quisquam, doloribus dolor aliquam magni quidem sint quas officiis quasi.
		                </p>
		            </div>
		            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
		            	<div class="svgsection">
		                	<img src="<?php echo $path; ?>/img/icon2.svg" class="svg" id="logo" />
		                </div>
		                <a class="card-title">ACCOUNTABILITY FORMS</a>
		            </div>
		            <div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
		                <p class="smalldetail">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita sed repellat eligendi totam! Quam praesentium quibusdam, veniam porro ipsa possimus delectus quia asperiores inventore vitae alias sint quas nobis, consequatur.
		                </p>
		            </div>
		         <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
		            	<div class="svgsection">
		                	<img src="<?php echo $path; ?>/img/briefcase.svg" class="svg" id="logo" />
		                </div>
		                <a class="card-title">BUSINESS OPERATIONS FORMS</a>
		            </div>
		            <div id="collapseThree" class="card-body collapse" data-parent="#accordion" >
		                <p class="smalldetail">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita sed repellat eligendi totam! Quam praesentium quibusdam, veniam porro ipsa possimus delectus quia asperiores inventore vitae alias sint quas nobis, consequatur.
		                </p>
		            </div>

		            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
		            	<div class="svgsection">
		                	<img src="<?php echo $path; ?>/img/icon3.svg" class="svg" id="logo" />
		                </div>
		                <a class="card-title">CHILD NUTRITION FORMS</a>
		            </div>
		            <div id="collapseFour" class="card-body collapse" data-parent="#accordion" >
		                <p class="smalldetail">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita sed repellat eligendi totam! Quam praesentium quibusdam, veniam porro ipsa possimus delectus quia asperiores inventore vitae alias sint quas nobis, consequatur.
		                </p>
		            </div>
		            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
		            	<div class="svgsection">
		                	<img src="<?php echo $path; ?>/img/abc-block.svg" class="svg" id="logo" />
		                </div>
		                <a class="card-title">EARLY CHILDHOOD PROGRAM FORMS</a>
		            </div>
		            <div id="collapseFive" class="card-body collapse" data-parent="#accordion" >
		                <p class="smalldetail">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita sed repellat eligendi totam! Quam praesentium quibusdam, veniam porro ipsa possimus delectus quia asperiores inventore vitae alias sint quas nobis, consequatur.
		                </p>
		            </div>
		            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
		            	<div class="svgsection">
		                	<img src="<?php echo $path; ?>/img/parents.svg" class="svg" id="logo" />
		                </div>
		                <a class="card-title">HUMAN RESOURCES FORMS</a>
		            </div>
		            <div id="collapseSix" class="card-body collapse" data-parent="#accordion" >
		                <p class="smalldetail">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita sed repellat eligendi totam! Quam praesentium quibusdam, veniam porro ipsa possimus delectus quia asperiores inventore vitae alias sint quas nobis, consequatur.
		                </p>
		            </div>
		            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
		            	<div class="svgsection">
		                	<img src="<?php echo $path; ?>/img/website.svg" class="svg" id="logo" />
		                </div>
		                <a class="card-title">INFORMATION TECHNOLOGY FORMS</a>
		            </div>
		            <div id="collapseSeven" class="card-body collapse" data-parent="#accordion" >
		                <p class="smalldetail">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita sed repellat eligendi totam! Quam praesentium quibusdam, veniam porro ipsa possimus delectus quia asperiores inventore vitae alias sint quas nobis, consequatur.
		                </p>
		            </div>
		            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
		            	<div class="svgsection">
		                	<img src="<?php echo $path; ?>/img/worldwide.svg" class="svg" id="logo" />
		                </div>
		                <a class="card-title">VIEW ALL FORMS</a>
		            </div>
		            <div id="collapseEight" class="card-body collapse" data-parent="#accordion" >
		                <p class="smalldetail">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita sed repellat eligendi totam! Quam praesentium quibusdam, veniam porro ipsa possimus delectus quia asperiores inventore vitae alias sint quas nobis, consequatur.
		                </p>
		            </div>
		            
		        </div>
		    </div>		
		</section>
				
		<section class="parallaxpart" style="background-image: url(<?php echo $path; ?>/img/banner2.jpg);">
			<div class="row justify-content-center">
				<div class="col-md-6">
					<div class="greenbox">
						<h2 class="no-background">
							<span>Departments</span>					
					    </h2>
					    <div class="row">
						    <div class="col-lg-8">
						    	<p>View a full list of our Departments.</p>
						    </div>
						    <div class="col-lg-4">
						    	<a href="" class="btn">Read more</a>
						    </div>	
						</div>    
					</div>	
				</div>	
				<div class="col-md-6">
					<div class="greenbox">
						<h2 class="no-background">
							<span>PROGRAMS</span>					
					    </h2>
					    <div class="row">
						    <div class="col-lg-8">
						    	<p>View the Programs EBR has to offer.</p>
						    </div>
						    <div class="col-lg-4">
						    	<a href="" class="btn">Read more</a>
						    </div>	
						</div>    
					</div>	
				</div>	
			</div>
		</section>

		<section class="selectionpart">
			<div class="title">learn more about our:</div>			
		<div class="bottom_menu">
			<ul>
				<li class="active"><a href="">Leadership</a></li>
				<li><a href="">Story</a></li>
				<li><a href="">board</a></li>
				<li><a href="">policies</a></li>
				<li><a href="">communication</a></li>
			</ul>
		</div>
		</section>

		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
