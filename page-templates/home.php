<?php
/**
 * Template Name: home Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

		
		<div class="container-fluid no-padding">
		<section class="inner-container customcontainer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php $i_am_section_title = get_field('i_am_section_title');?>	
			         	 <?php if( !empty($i_am_section_title)):?>
				         		<h2><?php echo $i_am_section_title; ?></h2>
				         <?php endif;?>	
						<hr>
					</div>	
				</div>	
				<div class="row">
					<?php if( have_rows('i_am_box') ):  $i++;
							while ( have_rows('i_am_box') ) : the_row();
								$box_title = get_sub_field('box_title');
								$box_overlay_title = get_sub_field('box_overlay_title');
								$box_image = get_sub_field('box_image');
								$box_button_text = get_sub_field('box_button_text');
								$box_overlay_button_text = get_sub_field('box_overlay_button_text');
								$box_button_url = get_sub_field('box_button_url');
								$box_overlay_button_url = get_sub_field('box_overlay_button_url');
								if($i == 1){
									$extention = 'yellowbox';
								}elseif($i == 2){
									$extention = 'bluebox';
								}else{
									$extention = 'greenbox';
								}
								?>
					<div class="col-md-4 ">
						<div id="<?php echo $extention; ?>">
							<div class="contentpart">
								<div class="box1">
									<div class="row">
										<div class="col-5 col-md-5">
											<?php if( !empty($box_title)):?>
												<p><?php echo $box_title; ?></p>
											<?php endif;?>	
											<a class="btn explore-btn"><?php echo $box_button_text; ?></a>
										</div>	
										<div class="col-7 col-md-7 no-padding">
											<img src="<?php echo $box_image['url']; ?>" class="imgover" alt=""> 
										</div>	
									</div>									 
							   </div>
							   <div class="box2">
									<div class="row">
										<div class="col-md-12 text-left">
										<?php if( !empty($box_overlay_title)):?>
												<p><?php echo $box_overlay_title; ?></p>
											<?php endif;?>
											<ul>
												<?php if( have_rows('box_overlay_links') ): 
												while ( have_rows('box_overlay_links') ) : the_row();
													$link_text = get_sub_field('link_text');
													$link_url = get_sub_field('link_url');
													?>
												<?php if( !empty($link_url)):?>
													<li>
														<a href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a>
													</li>
												<?php endif;?>
												<?php endwhile; endif;?>	
											</ul>
											<?php if( !empty($box_overlay_button_url)):?>
												<a class="btn explore-btn float-right m_r10" href="<?php echo $box_overlay_button_url; ?>"><?php echo $box_overlay_button_text; ?></a>	
											<?php endif;?>

									   <div class="clerfix"></div>
									    </div>
									</div>									
							   </div>
							</div>	
						</div>	
					</div>	
					<?php $i++; endwhile; endif;?>

					
				</div>
			</div>		

		</section>

		<section class="bluecontainer">		
				<div class="row">
					<div class="col-md-12 text-center">
				 <?php $get_to_know_title = get_field('get_to_know_title');?>	
				 <?php $get_to_know_sub_title = get_field('get_to_know_sub_title');?>	
	         	 <?php if( !empty($get_to_know_title)):?>
		         		<label><?php echo $get_to_know_title; ?></label>
		         <?php endif;?>	
				 <?php if( !empty($get_to_know_sub_title)):?>
		         		<h3><?php echo $get_to_know_sub_title; ?></h3>
		         <?php endif;?>	

						<div class="processBox" id="microsoftcounter">
							<?php if( have_rows('get_to_know_repeater') ): $i = 1;
							while ( have_rows('get_to_know_repeater') ) : the_row();
								$icon = get_sub_field('icon');
								$number = get_sub_field('number');
								$description = get_sub_field('description');
								if($i == 1 || $i == 3){
									$extention = '+';
								}elseif($i == 2){
									$extention = 'nd';
								}else{
									$extention = '%';
								}
								?>
							<div class="processBoxCont">
								<div class="imgBoxMain">
									<div class="imgBox">
							 	     <?php if( !empty($icon)):?> <img src="<?php echo $icon['url']; ?>" alt=""><?php endif;?>	

							        </div>
							        <div class="counterpart">
							        	<?php if( !empty($number)):?> <div class="count" data-count="<?php echo $number; ?>">0</div><div class="extention"><?php echo $extention; ?></div>
							        	
							        	<?php endif;?>	

							        <div class="clerfix"></div>
							        </div>
							        <?php if( !empty($description)):?><label><?php echo $description; ?></label><?php endif;?>
								</div>
							</div>
							<?php $i++; endwhile; endif;?>

							
						</div>	
						<div class="clerfix"></div>
					</div>	
				</div>		
		</section>

		<section class="inner-container newspart">	
		<?php $news_event_section_title = get_field('news_event_section_title');?>	
	         <?php if( !empty($news_event_section_title)):?>
		         <div class="ss-style-triangles news">		   
		         	<h2><?php echo $news_event_section_title; ?> </h2>	
		         </div>	 
		         
	         <?php endif;?>	
	        
	        <div class="newspart">
		        <div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Recent news</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link myborder"><a href="<?php echo esc_url( home_url( '/' ) ); ?>news">see all</a></div>
		         	</div>		
		        </div>

		         <div class="row">
         			<?php 			     	  
	       			  $args = array( 'post_type' => 'news', 'posts_per_page' => 4,  'orderby' => 'DESC');

	        		  $loop = new WP_Query( $args );
	        		  if ( $loop->have_posts() ) {
	       			  while ( $loop->have_posts() ) : $loop->the_post();?>
	       		
						<div class="col-12 col-md-12 col-lg-12 col-xl-6">
		         		<div class="row">
		         			<div class="col-12 col-md-8 spacer">
		         				<a href="<?php the_permalink();?>">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         						<?php	if ( has_post_thumbnail() ) {  
								                        	the_post_thumbnail();  
								          		}elseif( get_theme_mod( 'news_default_image' ) ){
								          						 
					          						echo '<img src="' . get_theme_mod( 'news_default_image' ).'" alt=""/>';
													} else {

					          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" alt="" />';
								          					
								          			}
								          ?> 
			         				    </div>
		         					</div>	
		         			 	</a>
		         			</div>	
		         			<div class="col-12 col-md-4 spacer">
		         				<a href="<?php the_permalink();?>"><h2><?php the_title();?></h2></a>
		         				<?php $tags =  get_the_tags();?>
		         				<p><span style="color: <?php the_field('tag_color',  'term_'.$tags[0]->term_id); ?>;"><?php  echo $tags[0]->name; ?></span>  <?php echo get_the_date('M. j Y');?></p>
		         				 
		         				<p class="sortcontent">
		         					<?php $content = get_the_content();
            						$content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,116); ?>...
		         				</p>	
		         				<a href="<?php the_permalink();?>" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         		</div>	

			   		<?php  wp_reset_query(); endwhile; 
			   		} else {
						echo "No News Found";
					}?>

		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link"><a href="<?php echo esc_url( home_url( '/' ) ); ?>news">see all</a></div>
		         	</div>
		         </div>

	     	</div>
	    	
	    	

	     	<div class="eventpart myborder">
	     		<div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Events</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link"><a href="<?php echo esc_url( home_url( '/' ) ); ?>events">see all</a></div>
		         	</div>	
		        </div>

		        <div class="row">
		        	<?php 			     	  
	       			  $args = array( 'post_type' => 'tribe_events', 'posts_per_page' => 2, 'orderby' => 'DESC');

	        		  $loop = new WP_Query( $args );
	        		  if ( $loop->have_posts() ) {
	       			  while ( $loop->have_posts() ) : $loop->the_post();?>
			         	<div class="col-12 col-md-12 col-lg-12 col-xl-6">
			         		<div class="row">
			         			<div class="col-12 col-md-8 spacer">
			         				<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         						
			         					  <?php	if ( has_post_thumbnail() ) {  
								                        	the_post_thumbnail();  
								          		}elseif( get_theme_mod( 'event_default_image' ) ){
								          						 
					          						echo '<img src="' .get_theme_mod( 'event_default_image' ).'" alt=""/>';
													} else {

					          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" alt=""/>';
								          					
								          			}
								          ?> 
			         				<div class="ribbon">

											  <span class="ribbon2">
											  	<?php echo tribe_get_start_date( $post, false, 'M' );?> <br><strong><?php echo tribe_get_start_date( $event, false, 'j' );?> </strong></span>
											</div>
			         				    </div>
			         				</div>	
			         			</div>	
			         			<div class="col-12 col-md-4 spacer">
			         				<a href="<?php the_permalink();?>"><h2><?php the_title();?></h2></a>		        				
			         				<p class="sortcontent">
			         					<?php $content = get_the_content();
	                    						$content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,130); ?>...
			         				</p>	
			         				<a href="<?php the_permalink();?>" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
			         			</div>	
			         		</div>
			         	</div>

			   		<?php  wp_reset_query(); endwhile; 
			   		} else {
						echo "No Events Found";
					}?>
		         			
		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link myborder"><a href="<?php echo esc_url( home_url( '/' ) ); ?>events">see all</a></div>
		         	</div>
		         </div>
	     	</div>
	     	
		</section>

		<?php $li = get_field('bottom_slider_shortcode'); ?>
		<?php echo do_shortcode($li); ?>

		<div class="inner-container instagram">
			 <div class="row">
			 	<?php $social_section_title = get_field('social_section_title');?>	
	         	<?php if( !empty($social_section_title)):?>
		         	<div class="col-12 text-center">
		         		<label><?php echo $social_section_title; ?></label>
		         	</div>
		         <?php endif;?>		

		     </div>


		</div>	


		<div class="clerfix"></div>
				
	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
