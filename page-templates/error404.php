<?php
/**
 * Template Name: Error 404 page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		

		<section class="paragraphpart p_tb_50 error_part" style="background-image: url(<?php echo $path; ?>/img/story-bg.jpg);">	
				<div class="row">
					<div class="col-md-12 text-center ">
						<h1>Oh no!</h1>
						<label>This page cannot be found</label>
						<p>Sorry! The page that you are looking for may no longer exist. </p>
						<a class="btn" href="<?php echo esc_url( home_url( '/' ) ); ?>">back to home</a>
					</div>	
				</div>		
		</section>

		
<!-- <div class="menu">
	<ul>
		<li><a href="">Menu 1</a></li>
		<li><a href="">Menu 2</a></li>
		<li ><a href="">Menu 3</a></li>
		<li><a href="">Menu 4</a></li>
		<li class="active"><a href="">Menu 5</a></li>
	</ul>
</div> -->





		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
