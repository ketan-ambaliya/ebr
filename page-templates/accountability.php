<?php
/**
 * Template Name: Accountability
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		

		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
					<label>	
						<img src="<?php echo $path; ?>/img/icon2.svg" class="svg" id="logo" /> Accountability
					</label>					
						<p>See below for the main information you will need from the Communications department. </p>
					</div>	
				</div>		
		</section>

		<section class="parallaxpart threebox" style="background-image: url(<?php echo $path; ?>/img/banner2.jpg);">
			<div class="inner-container">
			<div class="row justify-content-center display-flex">
				<div class="col-md-6 col-lg-4">
					<div class="greenbox">
						<h2> school test coordinator </h2>
						<hr>				    
						<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum temporibus labore cumque sed sint cum quasi excepturi alias praesentium nihil illum</p>
						<a href="" class="btn">Learn more</a>						   
					</div>    
				</div>	
					
				<div class="col-md-6 col-lg-4">
					<div class="greenbox">
						<h2> state testing & performance score</h2>
						<hr>				    
						<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
						<a href="" class="btn">Learn more</a>						   
					</div>    
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="greenbox">
						<h2> Benchmark tests </h2>
						<hr>				    
						<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum temporibus labore cumque sed sint cum quasi excepturi alias praesentium nihil illum</p>
						<a href="" class="btn">Read more</a>						   
					</div>    
				</div>

				</div>	
					
			</div>
		</section>

		<section class="inner-container communicationpart">	
			<div class="row">	
			    <div class="col-lg-8">
			    	<h2>Accountability Overview</h2>	
			    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ducimus amet asperiores explicabo iste odio quibusdam molestias fuga deleniti doloribus expedita modi libero, ipsum consequatur nisi ea officia accusamus vel? <br> <br>
			    	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, eaque repellendus quas, ea aliquid quae at veniam harum? Quis possimus tempora perferendis, culpa praesentium? Maxime recusandae expedita distinctio, minima nisi. <br> <br>
			    	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, eaque repellendus quas, ea aliquid quae at veniam harum? Quis possimus tempora perferendis, culpa praesentium? Maxime recusandae expedita distinctio, minima nisi.
			    	</p>
			    	<ul>
			    		<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Student breakfast : one free meal each day</a></li>
			    		<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Student lunch : one free meal each day</a></li>
			    		<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Extra milk: $1.00</a></li>
			    		<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Adult prices: $5.25 for breakfast and  $4.00 for lunch </a></li>
			    	</ul>	

			    </div>	
			   
			    <div class="col-lg-4 sidebar-blue-section">
					<div class="blue-border-box one">
						<div class="ribbonpart"><label>Other important info</label></div>
						<label>Contact:</label>
						<p>Taylor H. Gast, Director of Communication and Public Relations,
						1050 S. Foster Dr. Baton Rouge, LA 70806 </p>
						<div class="view">
							<a href="">[view map]</a>								
								<div class="row contact">
									<div class="col-md-12 col-lg-12 col-xl-6 no-padding">
										<a href="tel:225.922.5400"><strong>Ph:</strong> 225.922.5627</a>
									</div>
									<div class="col-md-12 col-lg-12 col-xl-6 no-padding">
										<a href=""><strong>fax:</strong> 225.226.3662</a>  
									</div>										
								</div>												
						</div>
						
						<hr>
						<label>Accountability Quicklinks:</label>
						<ul>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Media request form</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> flyer aprroval/ Distribution form</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Graphics Arts</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> EBR TV</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> EBR Newsletters</a></li>
						</ul>	
						<hr>
						<label>You may also interested in:</label>
						<ul>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> All form</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> all policies</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Contact direstory</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> All departments</a></li>
							<li><a class="f_links" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> all programs</a></li>
						</ul>
					</div>
			    </div>	
			</div>    
		</section>
		
		<section class="inner-container newspart">			
	         <div class="ss-style-triangles news">		   
	         	<h2>What is happening in EBr?</h2>	
	         </div>	   
	        <div class="newspart">
		        <div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Recent news</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link myborder"><a href="">see all</a></div>
		         	</div>		
		        </div>

		         <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev1.jpg"> 
			         				    </div>
		         					</div>	
		         			 	</a>
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>Biden Project Youth Leads</h2></a>
		         				<p><span>ACADEMICS</span>  DEC. 1 2018 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to enjoy a special Thanksgiving...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev2.jpg"> 
			         				    </div>
		         					</div>	
		         				</a>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>EBR School Board Recognitions at the March 21 Regular Meeting</h2></a>
		         				<p><span class="green">announcement</span>  jan. 5 2019 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev3.jpg"> 
			         				    </div>
		         					</div>	
		         				</a>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>JANUARY 2019 BOARD RECOGNITIONS</h2></a>
		         				<p><span class="blue">news</span>  Jan . 5 2019 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev4.jpg"> 
			         				    </div>
			         				</div>	
			         			</a>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>THANKSGIVING 2018 PHOTO GALLERY</h2></a>
		         				<p><span>acadamics</span>  dec. 1 2018 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to enjoy a special Thanksgiving...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link"><a href="">see all</a></div>
		         	</div>
		         </div>

	     	</div>

	     	<div class="eventpart myborder">
	     		<div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Events</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link"><a href="">see all</a></div>
		         	</div>	
		        </div>

		        <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne1.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">jun<br><strong>1</strong></span>
										</div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>I CARE PREVENTION SUMMIT</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza... and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne2.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">Nov<br><strong>7</strong></span>
										</div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>EARLY CHILDHOOD EXTRAVAGANZA</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link myborder"><a href="">see all</a></div>
		         	</div>
		         </div>
	     	</div>
		</section>

		<section class="inner-container bottom-dark-border-part">						
				<div class="blue-border-box one">
					<div class="ribbonpart"><label>Our goal</label></div>
					<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis, reprehenderit adipisci, rem officiis ut quaerat alias illo. Asperiores perspiciatis odio fugiat rerum aliquid dolorem, quas voluptates doloribus! Expedita, porro, sequi!</p>
				</div>
			
		</section>	

		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
