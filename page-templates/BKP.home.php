<?php
/**
 * Template Name: home Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<div class="wrapper" id="full-width-page-wrapper">

	<!-- <div class="<?php echo esc_attr( $container ); ?>" id="content"> -->
		<div class="container-fluid no-padding">
		<?php /*<div class="sliderpart">					
				<div class="slider">
				  <div>
				    <img src="<?php echo $path; ?>/img/banner1.jpg">
				  </div>
				  <!-- <div>
				    <img src="<?php echo $path; ?>/img/banner2.jpg">
				  </div> -->
				</div>
				<div class="slider_footer">	
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-2">
								<img src="<?php echo $path; ?>/img/slider-logo.png">
							</div>
							<div class="col-md-7">
								<p class="slider-title"> one team. one mission.</p>
								<p class="italic-title"> Building the Future of Baton Rouge.</p>
							</div>
							<div class="col-md-3">
								<a href="" class="btn">find a school</a>
							</div>	
						</div> 			  
					</div>
				</div>
		</div>*/?>

		<section class="inner-container customcontainer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 text-center">
						<h2>what can we help you with?</h2>
						<hr>
					</div>	
				</div>	
				<div class="row">
					<div class="col-md-4 ">
						<div id="yellowbox">
							<div class="contentpart">
								<div class="box1">
									<div class="row">
										<div class="col-5 col-md-5">
											<p> I am a <label>parent/ student</label> looking for...
											</p>	
											<a class="btn explore-btn">Explore</a>
										</div>	
										<div class="col-7 col-md-7 no-padding">
											<img src="<?php echo $path; ?>/img/school1.png" class="imgover"> 
											<!-- dimention must be 231px by 215px kindly mention it in acf field while development-->
										</div>	
									</div>									 
							   </div>
							   <div class="box2">
									<div class="row">
										<div class="col-md-12 text-left">
										<p> I am a looking for... </p>
											<ul>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> my child's bus route</a>
												</li>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> List of ebr schools</a>
												</li>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> lunch menus</a>
												</li>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> school policies</a>
												</li>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> holidays/testing calender</a>
												</li>
											</ul>
											<a class="btn explore-btn float-right m_r10">See more</a>	
									   <div class="clerfix"></div>
									    </div>
									</div>									
							   </div>
							</div>	
						</div>	
					</div>	
					<div class="col-md-4">
						<div id="bluebox">
							<div class="contentpart">
								<div class="box1">
									<div class="row">
										<div class="col-5 col-md-5">
											<p> I am an <label>employee/ teacher</label> looking for...
											</p>	
											<a class="btn explore-btn">Explore</a>
										</div>	
										<div class="col-7 col-md-7 no-padding">
											<img src="<?php echo $path; ?>/img/school2.png" class="imgover">
											<!-- dimention must be 231px by 218px kindly mention it in acf field while development-->
										</div>	
									</div>									 
							   </div>
							   <div class="box2">
									<div class="row">
										<div class="col-md-12 text-left">
										<p> I am a looking for... </p>
											<ul>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> my child's bus route</a>
												</li>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> List of ebr schools</a>
												</li>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> lunch menus</a>
												</li>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> school policies</a>
												</li>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> holidays/testing calender</a>
												</li>
											</ul>
											<a class="btn explore-btn float-right m_r10">See more</a>	
									   <div class="clerfix"></div>
									    </div>
									</div>									
							   </div>
							</div>	
						</div>	
					</div>
					<div class="col-md-4">
						<div id="greenbox">
							<div class="contentpart">
								<div class="box1">
									<div class="row">
										<div class="col-5 col-md-5">
											<p> I am a <label>community member</label> looking for...
											</p>	
											<a class="btn explore-btn">Explore</a>
										</div>	
										<div class="col-7 col-md-7 no-padding">
											<img src="<?php echo $path; ?>/img/school3.png" class="imgover">
											<!-- dimention must be 231px by 215px kindly mention it in acf field while development-->
										</div>	
									</div>									 
							   </div>
							   <div class="box2">
									<div class="row">
										<div class="col-md-12 text-left">
										<p> I am a looking for... </p>
											<ul>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> my child's bus route</a>
												</li>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> List of ebr schools</a>
												</li>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> lunch menus</a>
												</li>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> school policies</a>
												</li>
												<li>
													<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> holidays/testing calender</a>
												</li>
											</ul>
											<a class="btn explore-btn float-right m_r10">see more</a>	
									   <div class="clerfix"></div>
									    </div>
									</div>									
							   </div>
							</div>	
						</div>	
					</div>
					
				</div>
			</div>		

		</section>

		<section class="bluecontainer">		
				<div class="row">
					<div class="col-md-12 text-center">
						<label>get to know the</label>
						<h3>East Baton Rouge Parish School System</h3>
						<div class="processBox" id="microsoftcounter">
							<div class="processBoxCont">
								<div class="imgBoxMain">
									<div class="imgBox">
							 	      <img src="<?php echo $path; ?>/img/board.png">
							        </div>
							        <div class="counterpart">
							        	<div class="count" data-count="41000">0</div><div class="extention">+</div>
							        <div class="clerfix"></div>
							        </div>
							        <label>Students</label>
								</div>
							</div>
							<div class="processBoxCont">
								<div class="imgBoxMain">
									<div class="imgBox">
							 	      <img src="<?php echo $path; ?>/img/tag.png">
							        </div>							       
							        <div class="counterpart">
							        	 <div class="count">2nd</div>
							        <div class="clerfix"></div>
							        </div>
							        <label>largest public school in the state</label>
								</div>
							</div>
							<div class="processBoxCont">
								<div class="imgBoxMain">
									<div class="imgBox">
							 	      <img src="<?php echo $path; ?>/img/family.png">
							        </div>
							         <div class="counterpart">
							        	<div class="count" data-count="58000">0</div><div class="extention">+</div>
							        <div class="clerfix"></div>
							        </div>
							        <label>full - time employees</label>
								</div>
							</div>
							<div class="processBoxCont">
								<div class="imgBoxMain">
									<div class="imgBox">
							 	      <img src="<?php echo $path; ?>/img/cert.png">
							        </div>
							         <div class="counterpart">
							        	<div class="count" data-count="94">0</div><div class="extention">%</div>
							        <div class="clerfix"></div>
							        </div>
							        <label>of the tearchers are state certified</label>
								</div>
							</div>
						</div>	
						<div class="clerfix"></div>
					</div>	
				</div>		
		</section>

		<section class="inner-container newspart">			
	         <div class="ss-style-triangles news">		   
	         	<h2>What is happening in EBr?</h2>	
	         </div>	   
	        <div class="newspart">
		        <div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Recent news</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link myborder"><a href="">see all</a></div>
		         	</div>		
		        </div>

		         <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev1.jpg"> 
			         				    </div>
		         					</div>	
		         			 	</a>
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>Biden Project Youth Leads</h2></a>
		         				<p><span>ACADEMICS</span>  DEC. 1 2018 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to enjoy a special Thanksgiving...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev2.jpg"> 
			         				    </div>
		         					</div>	
		         				</a>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>EBR School Board Recognitions at the March 21 Regular Meeting</h2></a>
		         				<p><span class="green">announcement</span>  jan. 5 2019 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev3.jpg"> 
			         				    </div>
		         					</div>	
		         				</a>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>JANUARY 2019 BOARD RECOGNITIONS</h2></a>
		         				<p><span class="blue">news</span>  Jan . 5 2019 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<a href="">
		         					<div class="imgcontainer">
			         					<div class="img-thumbnails">
			         					  <img src="<?php echo $path; ?>/img/ev4.jpg"> 
			         				    </div>
			         				</div>	
			         			</a>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>THANKSGIVING 2018 PHOTO GALLERY</h2></a>
		         				<p><span>acadamics</span>  dec. 1 2018 </p>
		         				<p class="sortcontent">
		         					Throughout the district, elementary schools invited their students’ family members to enjoy a special Thanksgiving...
		         				</p>	
		         				<a href="#" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link"><a href="">see all</a></div>
		         	</div>
		         </div>

	     	</div>

	     	<div class="eventpart myborder">
	     		<div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Events</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link"><a href="">see all</a></div>
		         	</div>	
		        </div>

		        <div class="row">
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne1.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">jun<br><strong>1</strong></span>
										</div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>I CARE PREVENTION SUMMIT</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza... and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>
		         	<div class="col-12 col-md-12 col-lg-6">
		         		<div class="row">
		         			<div class="col-12 col-md-6 spacer">
		         				<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <img src="<?php echo $path; ?>/img/ne2.jpg"> 
		         					  <div class="ribbon">
										  <span class="ribbon2">Nov<br><strong>7</strong></span>
										</div>
		         				    </div>
		         				</div>	
		         			</div>	
		         			<div class="col-12 col-md-6 spacer">
		         				<a href=""><h2>EARLY CHILDHOOD EXTRAVAGANZA</h2></a>		         				
		         				<p class="sortcontent">
		         					Baton Rouge and Baker families with children up to five years old are invited to join us for the Early Childhood Extravaganza and Early Bird...
		         				</p>	
		         				<a href="#" class="enlink">See details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
		         			</div>	
		         		</div>
		         	</div>		
		         </div>
		         <div class="row">
		         <div class="col-12 d-block d-lg-none">
		         		<div class="link myborder"><a href="">see all</a></div>
		         	</div>
		         </div>
	     	</div>
		</section>

		<?php echo do_shortcode("[shortcode category='home']");?>

		<section class="inner-container instagram">
			 <div class="row">
		         	<div class="col-12 text-center">
		         		<label>let's get social !</label>
		         	</div>		         		
		        </div>
		</section>	

		<div class="clerfix"></div>
				<!-- <div class="col-md-12 content-area" id="primary"> -->
				<!-- <main class="site-main" id="main" role="main">

					<?php //while ( have_posts() ) : the_post(); ?>

						<?php// get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						
						//if ( comments_open() || get_comments_number() ) :
							//comments_template();
						//endif;
						?>

					<?php //endwhile; ?>

				</main> --><!-- #main -->

			<!-- </div> --><!-- #primary -->

	

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>
