<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Display -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.23
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
$path = get_template_directory_uri();
get_header();?>


<?php if(!is_singular()){?>

<div class="container-fluid no-padding">
		
<?php if(!empty(get_field('e_heading', 'option')) || !empty(get_field('e_sub_heading', 'option')) || !empty(get_field('e_description', 'option')) || !empty(get_field('e_heading_icon', 'option'))) {?>
	<section class="inner-container paragraphpart">	
			<div class="row">
				<div class="col-md-12 text-center">
					<?php $heading_icon = get_field('e_heading_icon', 'option');?>
					<?php if(!empty($heading_icon)): ?>
						<img src="<?php echo $heading_icon['url']; ?>">
					<?php endif;?>
					<?php if(!empty(get_field('e_heading', 'option'))): ?><label><?php the_field('e_heading', 'option');?></label><?php endif;?>
					<?php if(!empty(get_field('e_sub_heading', 'option'))): ?><h3><?php the_field('e_sub_heading', 'option');?></h3><?php endif;?>
					<?php if(!empty(get_field('e_description', 'option'))): ?><p><?php the_field('e_description', 'option');?></p><?php endif;?>
				</div>	
			</div>		
	</section>
    <?php } ?>
    
</div>
<div class="wrapper" id="full-width-page-wrapper">
	<div class="container-fluid no-padding">
		<section class="inner-container news_content">	
			<div class="row">	
			    <div class="col-lg-8">
			    	
    	<label>Upcoming Events </label>
	    	<div class="tab-content">
	    		<div class="eventpart">

			    	<?php global $post;
			    	$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
			    	
					$get_posts = tribe_get_events(array('posts_per_page'=>'2', 'eventDisplay'=>'upcoming', 'paged' => $paged) );
						if($get_posts){
					foreach($get_posts as $post) { setup_postdata($post);
					        ?>
	        			<div class="row">
				        <div class="col-12 col-md-5 spacer">
					        <a href="<?php the_permalink(); ?>">
					        	<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					 <?php	if ( has_post_thumbnail() ) {  
								                        	the_post_thumbnail();  
								          		}elseif( get_theme_mod( 'event_default_image' ) ){
								          						 
					          						echo '<img src="' .get_theme_mod( 'event_default_image' ).'" />';
													} else {

					          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
								          					
								          			}
								          ?> 
		         					<div class="ribbon">
										  <span class="ribbon2"><?php echo tribe_get_start_date( $post, false, 'M' );?> <br><strong><?php echo tribe_get_start_date( $post, false, 'j' );?> </strong></span>
										</div>
		         				    </div>
		         				</div>
			         		</a> 	
						</div>
						<div class="col-12 col-md-7 spacer">
							<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
							<p><span>Events</span>  <?php echo tribe_get_start_date( $post, false, 'M. d Y');?></p>
							<p class="sortcontent">
								<?php $content = get_the_content();
					                  $content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,270); ?>...
							</p>	
							<a href="<?php the_permalink(); ?>" class="enlink">See Details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				                           
				        </div>
				      <div class="col-12"><hr/></div>
				     </div>

					<?php } }else {
						echo "No Upcomin Events";
					}?>
					<?php wp_reset_query(); ?>
					

        </div>

			<label>Past Events </label>
		<div class="tab-content">
	    		<div class="eventpart">
			    	<?php global $post;
			    	$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
			    	
					$get_posts = tribe_get_events(array('posts_per_page'=>'2', 'eventDisplay'=>'past', 'paged' => $paged) );
					if($get_posts){
					foreach($get_posts as $post) { setup_postdata($post);
					        ?>
	        			<div class="row">
				        <div class="col-12 col-md-5 spacer">
					        <a href="<?php the_permalink(); ?>">
					        	<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <?php	if ( has_post_thumbnail() ) {  
								                        	the_post_thumbnail();  
								          		}elseif( get_theme_mod( 'event_default_image' ) ){
								          						 
					          						echo '<img src="' .get_theme_mod( 'event_default_image' ).'" />';
													} else {

					          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
								          					
								          			}
								          ?> 
		         					<div class="ribbon">
										  <span class="ribbon2"><?php echo tribe_get_start_date( $post, false, 'M' );?> <br><strong><?php echo tribe_get_start_date( $post, false, 'j' );?> </strong></span>
										</div>
		         				    </div>
		         				</div>
			         		</a> 	
						</div>
						<div class="col-12 col-md-7 spacer">
							<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
							<p><span>Events</span>  <?php echo tribe_get_start_date( $post, false, 'M. d Y');?></p>
							<p class="sortcontent">
								<?php $content = get_the_content();
					                  $content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,270); ?>...
							</p>	
							<a href="<?php the_permalink(); ?>" class="enlink">See Details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				                           
				        </div>
				      <div class="col-12"><hr/></div>
				     </div>

					<?php } }else {
						echo "No Past Events";
					}?>
					<?php wp_reset_query(); ?>
 			</div>
        </div>
        <div class="pagination_event">
        <?php
/** 
 * Create numeric pagination in WordPress
 */
 
// Get total number of pages
$get_events = tribe_get_events(array('posts_per_page'=>'-1', 'eventDisplay'=>'upcoming,past') );

$total= count($get_events);
$totalpage = ceil($total/4);


// Only paginate if we have more than one page
if ( $total > 4 )  {
     // Get the current page
     if ( !$paged = get_query_var('paged') )
          $paged = 1;
     // Structure of “format” depends on whether we’re using pretty permalinks
     $format = empty( get_option('permalink_structure') ) ? '&page=%#%' : 'page/%#%/';
     echo paginate_links(array(
          'base' => get_pagenum_link(1) . '%_%',
          'format' => 'page/%#%/',
          'current' => $paged,
          'total' => $totalpage,
          'mid_size' => 4,
          'end_size' => 4,
          'type' => 'plain',
          'prev_text'          => __(''),
		  'next_text'          => __('')
     ));
}?>
        
			</div>
		</div>

		 </div>	
			   
			    <div class="col-lg-4 sidebar-blue-section">
					<div class="blue-border-box one">
						<div class="ribbonpart"><label>Learn more</label></div>

						<label>Find us & Follow us</label>
						<ul>
						
							<?php if ( get_theme_mod( 'facebook_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'facebook_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'facebook_icon_class' );?>" aria-hidden="true"></i>Facebook
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'twitter_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'twitter_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'twitter_icon_class' );?>" aria-hidden="true"></i>Twitter
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'youtube_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'youtube_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'youtube_icon_class' );?>" aria-hidden="true"></i>Youtube
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'instagram_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'instagram_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'instagram_icon_class' );?>" aria-hidden="true"></i>Instagram
						    			</a>
						    		</li>
						    	<?php endif; ?>
						</ul>
						<hr>
						<?php $interested_in_title = get_field('e_interested_in_title', 'option');?>
						<?php if( !empty($interested_in_title)):?><label> <?php echo $interested_in_title; ?> </label><?php endif;?>
						<ul>
							<?php if( have_rows('e_interested_in_links', 'option') ):
								while ( have_rows('e_interested_in_links', 'option') ) : the_row();
								$link_text = get_sub_field('link_text', 'option');
								$link_url = get_sub_field('link_url', 'option');?>
							<?php if( !empty($link_url)):?><li><a class="f_links" href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a></li><?php endif;?>
							<?php endwhile; endif;?>
						</ul>
						
					
					</div>

					<div class="newspart">
			         	<label>News</label>	
			         	<?php $args = array ('post_type' => 'news', 'posts_per_page' => '2', 'order' => 'DESC');?>

    <?php $posts = new WP_Query($args);
      if ($posts->have_posts()){?>
      <?php  while ($posts->have_posts()){ $posts->the_post(); ?>

       <div class="row">
        <div class="col-12 col-md-12 spacer">
	        <a href="<?php the_permalink(); ?>">        
	            <div class="imgcontainer"> 
					<div class="img-thumbnails">
		         	<?php	if ( has_post_thumbnail() ) {  
								                        	the_post_thumbnail();  
								          		}elseif( get_theme_mod( 'news_default_image' ) ){
								          						 
					          						echo '<img src="' .get_theme_mod( 'news_default_image' ).'" />';
													} else {

					          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
								          					
								          			}
								          ?>
	                </div>
				</div>	
			</a> 	
		</div>
		<div class="col-12 col-md-12 spacer">
			<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
			<?php $tags =  get_the_tags();?>
		         				<p class="n_date"><span style="color: <?php the_field('tag_color',  'term_'.$tags[0]->term_id); ?>;"><?php  echo $tags[0]->name; ?></span>  <?php echo get_the_date('M. j Y');?></p>
			<p class="sortcontent">
				<?php $content = get_the_content();
	                  $content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,270); ?>...
			</p>	
			<a href="<?php the_permalink(); ?>" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                           
        </div>
      
     </div>
   <?php } }?>
		         		<a href="<?php echo esc_url( home_url( '/' ) ); ?>news" class="btn">see all news</a>       	
				    </div>
				</div>	

			    	
			</div>    
		</section>

						
		<section class="inner-container instagram">
			 <div class="row">
		         	<div class="col-12 text-center">
		         		<label>let's get social !</label>
		         	</div>		         		
		     </div>
		</section>
					

	</div>

</div>


 <!-- #tribe-events-pg-template -->
<?php } else {?>
<main id="tribe-events-pg-template" class="tribe-events-pg-template">
	<?php tribe_events_before_html(); ?>
	<?php tribe_get_view(); ?>
	<?php tribe_events_after_html(); ?>
</main> <!-- #tribe-events-pg-template -->
<?php }
get_footer();
