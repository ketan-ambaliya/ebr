<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>
<?php /*<div id="tribe-events-content" class="tribe-events-single">

	<p class="tribe-events-back">
		<a href="<?php echo esc_url( tribe_get_events_link() ); ?>"> <?php printf( '&laquo; ' . esc_html_x( 'All %s', '%s Events plural label', 'the-events-calendar' ), $events_label_plural ); ?></a>
	</p>

	<!-- Notices -->
	<?php tribe_the_notices() ?>

	<?php the_title( '<h1 class="tribe-events-single-event-title">', '</h1>' ); ?>

	<div class="tribe-events-schedule tribe-clearfix">
		<?php echo tribe_events_event_schedule_details( $event_id, '<h2>', '</h2>' ); ?>
		<?php if ( tribe_get_cost() ) : ?>
			<span class="tribe-events-cost"><?php echo tribe_get_cost( null, true ) ?></span>
		<?php endif; ?>
	</div>

	<!-- Event header -->
	<div id="tribe-events-header" <?php tribe_events_the_header_attributes() ?>>
		<!-- Navigation -->
		<nav class="tribe-events-nav-pagination" aria-label="<?php printf( esc_html__( '%s Navigation', 'the-events-calendar' ), $events_label_singular ); ?>">
			<ul class="tribe-events-sub-nav">
				<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
				<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
			</ul>
			<!-- .tribe-events-sub-nav -->
		</nav>
	</div>
	<!-- #tribe-events-header -->

	<?php while ( have_posts() ) :  the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<!-- Event featured image, but exclude link -->
			<?php echo tribe_event_featured_image( $event_id, 'full', false ); ?>

			<!-- Event content -->
			<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
			<div class="tribe-events-single-event-description tribe-events-content">
				<?php the_content(); ?>
			</div>
			<!-- .tribe-events-single-event-description -->
			<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>

			<!-- Event meta -->
			<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
			<?php tribe_get_template_part( 'modules/meta' ); ?>
			<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
		</div> <!-- #post-x -->
		<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
	<?php endwhile; ?>

	<!-- Event footer -->
	<div id="tribe-events-footer">
		<!-- Navigation -->
		<nav class="tribe-events-nav-pagination" aria-label="<?php printf( esc_html__( '%s Navigation', 'the-events-calendar' ), $events_label_singular ); ?>">
			<ul class="tribe-events-sub-nav">
				<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
				<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
			</ul>
			<!-- .tribe-events-sub-nav -->
		</nav>
	</div>
	<!-- #tribe-events-footer -->

</div><!-- #tribe-events-content -->
*/?>

<div class="wrapper" id="full-width-page-wrapper">
	<div class="container-fluid no-padding">
		<section class="inner-container news_content">	
			 <div class="eventpart single-event"> 	
			<div class="row">	
			    <div class="col-lg-8">					 
			    					    	
			    		<div class="ev-date">
						  <span class="ribbon2"><?php echo tribe_get_start_date( $post, false, 'M' );?> <br><strong><?php echo tribe_get_start_date( $post, false, 'j' );?> </strong></span>
						</div>
						
						<div class="ev-title">
							<h2 class="toplink"><?php the_title(); ?></h2>
							
							<p class="myalign">
								<span><?php $currentdat = date('m d y');
								 $startdate = tribe_get_start_date( $post, false, 'm d y');
								if( $startdate < $currentdat){
									echo "Past Event";
								}
								else {
									echo "Upcoming Event";
								}
								?> </span>  
							<?php echo tribe_get_start_date( $post, false, 'M. d Y');?></p>
						</div>
					
					
					<div class="imgcontainer">
			    	<div class="img-thumbnails">
 					 <?php	if ( has_post_thumbnail() ) {  
				                        	the_post_thumbnail();  
				          		}elseif( get_theme_mod( 'event_default_image' ) ){
				          						 
	          						echo '<img src="' .get_theme_mod( 'event_default_image' ).'" />';
									} else {

	          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
				          					
				          			}
				          ?> 
 					
 				    </div>
 					</div>
 				    <h2 class="toplink">Event Details</h2>
 				    <p class="commontext"><?php echo get_the_content();?></p>
 				    <?php if(empty(tribe_get_start_time ()) && empty(tribe_get_end_time ())){?>
 				    	<p class="commontext">Time: All Day Event</p>
 				    <?php } else {?> 
 				     Time: <?php echo tribe_get_start_time ( $event_id, 'h:ia'); echo " - ";  echo tribe_get_end_time ( $event_id, 'h:ia');?></p>
 					<?php } ?>
 				    <h2 class="toplink">Add To Your Calendar</h2>
 				    <?php do_action( 'tribe_events_single_event_after_the_content' ) ?>

			<!-- Event meta -->
			<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
			<?php //tribe_get_template_part( 'modules/meta' ); ?>
			<?php //do_action( 'tribe_events_single_event_after_the_meta' ) ?>


			    </div>	
			    <div class="col-lg-4 sidebar-blue-section">
			    	<div class="blue-border-box one">
						<div class="ribbonpart"><label>Learn more</label></div>

						<label>Find us & Follow us</label>
						<ul>
						
							<?php if ( get_theme_mod( 'facebook_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'facebook_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'facebook_icon_class' );?>" aria-hidden="true"></i>Facebook
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'twitter_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'twitter_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'twitter_icon_class' );?>" aria-hidden="true"></i>Twitter direstory
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'youtube_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'youtube_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'youtube_icon_class' );?>" aria-hidden="true"></i>You tube
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'instagram_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'instagram_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'instagram_icon_class' );?>" aria-hidden="true"></i>Instagram
						    			</a>
						    		</li>
						    	<?php endif; ?>
						</ul>
						<hr>
						<?php $interested_in_title = get_field('e_interested_in_title', 'option');?>
						<?php if( !empty($interested_in_title)):?><label> <?php echo $interested_in_title; ?> </label><?php endif;?>
						<ul>
							<?php if( have_rows('e_interested_in_links', 'option') ):
								while ( have_rows('e_interested_in_links', 'option') ) : the_row();
								$link_text = get_sub_field('link_text', 'option');
								$link_url = get_sub_field('link_url', 'option');?>
							<?php if( !empty($link_url)):?><li><a class="f_links" href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a></li><?php endif;?>
							<?php endwhile; endif;?>
						</ul>
						
					</div>
			    </div>	  	
			    
				</div>	
			</div>

				<div class="eventpart">
					<div class="ss-style-triangles news">		   
	         			<h2>Other events you may be interested in...</h2>	
	         		</div>
	         		<div class="row">
		         	<div class="col-12 col-md-6">
		         		<label>Events</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link"><a href="<?php echo esc_url( home_url( '/' ) ); ?>events">see all</a></div>
		         	</div>	
		       		</div>
				
					<div class="row">
			    	<?php global $post;
			    	
			    	
					$get_posts = tribe_get_events(array('posts_per_page'=>'2', 'eventDisplay'=>'upcoming', 'post__not_in' => array( $post->ID)) );

						if($get_posts){
					foreach($get_posts as $post) { setup_postdata($post);
					        ?>
	        			
	        				<div class="col-12 col-md-12 col-lg-12 col-xl-6">
	        					<div class="row">
				        <div class="col-12 col-md-8 spacer">
					        <a href="<?php the_permalink(); ?>">
					        	<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <?php	if ( has_post_thumbnail() ) {  
								                        	the_post_thumbnail();  
								          		}elseif( get_theme_mod( 'event_default_image' ) ){
								          						 
					          						echo '<img src="' .get_theme_mod( 'event_default_image' ).'" />';
													} else {

					          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
								          					
								          			}
								          ?> 
		         					<div class="ribbon">
										  <span class="ribbon2"><?php echo tribe_get_start_date( $post, false, 'M' );?> <br><strong><?php echo tribe_get_start_date( $post, false, 'j' );?> </strong></span>
										</div>
		         				    </div>
		         				</div>
			         		</a> 	
						</div>
						<div class="col-12 col-md-4 spacer">
							<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
							
							<p class="sortcontent">
								<?php $content = get_the_content();
					                  $content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,270); ?>...
							</p>	
							<a href="<?php the_permalink(); ?>" class="enlink">See Details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				                           
				        </div>
				      
				     </div>
</div>  

					<?php } }else {
						echo "No Events";
					}?>
					<?php wp_reset_query(); ?>
		         			

		         		   </div>    	
				    </div>
			   
			</div>
			</div>    
		</section>
	</div>
</div>