<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
$url = home_url();
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper no-padding" id="wrapper-footer">

	<div class="container-fluid no-padding">
    <div class="row">
        <div class="col-md-12 ">
				<footer class="site-footer" id="colophon">					
					<div class="newsletter">
						<div class="content-conainer">
							<div class="row">
							<div class="col-md-6 col-lg-2">
							 <?php if ( get_theme_mod( 'Footer_social_media_label' ) ):?>
                       			 <?php echo get_theme_mod( 'Footer_social_media_label' );?>
                  			 <?php endif; ?>	 
							</div>
							<div class="col-md-6 col-lg-4">
								<ul class="social">
							    	<?php if ( get_theme_mod( 'facebook_url' ) ):?>
			    		<li>
			    			<a href="<?php echo get_theme_mod( 'facebook_url' );?>" target="_blank">
			    				<i class="fa <?php echo get_theme_mod( 'facebook_icon_class' );?>" aria-hidden="true"></i>
			    			</a>
			    		</li>
			    	<?php endif; ?>
			    	<?php if ( get_theme_mod( 'twitter_url' ) ):?>
			    		<li>
			    			<a href="<?php echo get_theme_mod( 'twitter_url' );?>" target="_blank">
			    				<i class="fa <?php echo get_theme_mod( 'twitter_icon_class' );?>" aria-hidden="true"></i>
			    			</a>
			    		</li>
			    	<?php endif; ?>
			    	<?php if ( get_theme_mod( 'youtube_url' ) ):?>
			    		<li>
			    			<a href="<?php echo get_theme_mod( 'youtube_url' );?>" target="_blank">
			    				<i class="fa <?php echo get_theme_mod( 'youtube_icon_class' );?>" aria-hidden="true"></i>
			    			</a>
			    		</li>
			    	<?php endif; ?>
			    	<?php if ( get_theme_mod( 'instagram_url' ) ):?>
			    		<li>
			    			<a href="<?php echo get_theme_mod( 'instagram_url' );?>" target="_blank">
			    				<i class="fa <?php echo get_theme_mod( 'instagram_icon_class' );?>" aria-hidden="true"></i>
			    			</a>
			    		</li>
			    	<?php endif; ?>
							    </ul>
							</div>
							<div class="col-md-6 col-lg-2">
							 <?php if ( get_theme_mod( 'footer_subscribe_title' ) ):?>
                       			 <?php echo get_theme_mod( 'footer_subscribe_title' );?>
                  			 <?php endif; ?>	 
							</div>
							<div class="col-md-6 col-lg-4">
								<form class="f_news" action="<?php echo $url;?>">
								  <div class="input-container">								   
								    <input class="input-field" type="text" placeholder="Email" name="email">
								    <a href="" class="icon"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
								  </div>
								</form>	
							</div>
							<div class="clerfix"></div>
						</div>
						</div>	
					</div>	
					<div class="footer-part">
						<div class="container-fluid">
						<div class="row">
							<div class="col-12 col-md-6 col-lg-4 col-xl-2">
								<?php if ( get_theme_mod( 'footer_site_logo' ) ){ ?>
							        <a class="project_logo" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" ><img src="<?php echo get_theme_mod( 'footer_site_logo' );?>" alt="<?php bloginfo( 'name' ); ?>" /></a>
							    <?php }?>
								
								<ul class="addresspart">
									<?php if ( get_theme_mod( 'phone_label' ) ):?>
										<li><label>phone</label>:<a href="tel:<?php echo get_theme_mod( 'phone_label' );?>"> <?php echo get_theme_mod( 'phone_label' );?></a></li>
									<?php endif; ?>
									<?php if ( get_theme_mod( 'fax_label' ) ):?>
										<li><label>fax</label> : <?php echo get_theme_mod( 'fax_label' );?></li>
									<?php endif; ?>
									<?php if ( get_theme_mod( 'address_label' ) ):?>
										<li><label>address</label> : <?php echo get_theme_mod( 'address_label' );?></li>
									<?php endif; ?>
									
									<?php if ( get_theme_mod( 'email_label' ) ):?>
										<li><label>email</label>:<a href="mailto:<?php echo get_theme_mod( 'email_label' );?>"> <?php echo get_theme_mod( 'email_label' );?></a></li>
									<?php endif; ?>
								</ul>
								
								<form action="<?php echo $url;?>" method="get" class="f_news">
									<div class="input-container">	
									<button type="submit" class="icon"><i class="fa fa-search" aria-hidden="true"></i></button>
					  	      		<input type="text" name="s" id="search1" class="input-field" value="<?php the_search_query(); ?>" placeholder="search the site"/>
					  	      		<input type="hidden" value="search.php">
					  	      		</div>
					  	   		</form>
							</div>
							<div class="col-md-12 col-lg-12 col-xl-6">
								<?php if ( get_theme_mod( 'footer_title' ) ):?><h2 class="no-background"><span><?php echo get_theme_mod( 'footer_title' );?></span></h2><?php endif; ?>

								<div class="row">
									<div class="col-md-6">
										
										<?php if ( get_theme_mod( 'footer_department_menu_title' ) ):?>
											<label class="quick-links"><?php echo get_theme_mod( 'footer_department_menu_title' );?></label>
										<?php endif; ?>
										<?php wp_nav_menu(
											array(
												'theme_location'  => 'footer_departments',
												'menu'            => '',
												'container_class' => '',
												'container_id'    => '',
												'menu_class'      => '',
												'fallback_cb'     => '',
												'menu_id'         => 'main-menu1',
												'depth'           => 0,
												'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
											)
										); ?>
										
								   </div>
								   <div class="col-md-6">
										<?php if ( get_theme_mod( 'footer_programs_menu_title' ) ):?>
											<label class="quick-links"><?php echo get_theme_mod( 'footer_programs_menu_title' );?></label>
										<?php endif; ?>
										<?php wp_nav_menu(
											array(
												'theme_location'  => 'footer_programs',
												'menu'            => '',
												'container_class' => '',
												'container_id'    => '',
												'menu_class'      => '',
												'fallback_cb'     => '',
												'menu_id'         => 'main-menu2',
												'depth'           => 0,
												'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
											)
										); ?>
								   </div>
							    </div>
							</div>	
							<div class="col-12 col-md-6 col-lg-8 col-xl-4">
							 <div class="videopart">
							 	<?php if ( get_theme_mod( 'footer_video_image' ) ): ?>
							        <img src="<?php echo get_theme_mod( 'footer_video_image' );?>" alt="" />
							    <?php endif; ?>
							 	
							 	<div class="videocontent">
							 		<?php if ( get_theme_mod( 'footer_video_title' ) ): ?>
							       		 <?php echo get_theme_mod( 'footer_video_title' );?>
							    	<?php endif; ?>
							    	<?php if ( get_theme_mod( 'footer_video_url' ) ): ?>
							 		<a class="fancybox fancybox.iframe" href="<?php echo get_theme_mod( 'footer_video_url' );?>"><i class="fa <?php echo get_theme_mod( 'footer_video_play_icon' );?>" aria-hidden="true"></i></a>
							 		<?php endif; ?>
							 </div>
							 </div>
							</div>
							<div class="clerfix"></div>
						</div>	
						<hr>
						<?php if ( get_theme_mod( 'footer_notice' ) ): ?>
							<p class="privacy">
								<?php echo get_theme_mod( 'footer_notice' );?>
							</p>
						<?php endif; ?>
						<hr>
						<?php if ( get_theme_mod( 'footer_notice1' ) ): ?>
							<p class="privacy">
								<?php echo get_theme_mod( 'footer_notice1' );?>
							</p>
						<?php endif; ?>
						<div class="container brand_section">
	                        <div class="row">
	                            <div class="footer_slider">
	                             
	                                <?php if ( get_theme_mod( 'footer_image1' ) ): ?>
	                                <div>
										<a <?php if ( get_theme_mod( 'footer_image1_url' ) ):?>href="<?php echo get_theme_mod( 'footer_image1_url' ); ?>" <?php endif; ?>><img src="<?php echo get_theme_mod( 'footer_image1' );?>" alt=""></a>
									</div>
									<?php endif; ?>
	                              
	                                <?php if ( get_theme_mod( 'footer_image2' ) ): ?>
	                                <div>
										<a <?php if ( get_theme_mod( 'footer_image2_url' ) ):?>href="<?php echo get_theme_mod( 'footer_image2_url' );?>" <?php endif; ?>><img src="<?php echo get_theme_mod( 'footer_image2' );?>" alt=""></a>
									</div>
									<?php endif; ?>
									
	                                <?php if ( get_theme_mod( 'footer_image3' ) ): ?>
	                                <div>
										<a <?php if ( get_theme_mod( 'footer_image3_url' ) ):?>href="<?php echo get_theme_mod( 'footer_image3_url' );?>" <?php endif; ?>><img src="<?php echo get_theme_mod( 'footer_image3' );?>" alt=""></a>
									</div>
									<?php endif; ?>
	                              
	                                <?php if ( get_theme_mod( 'footer_image4' ) ): ?>
	                                <div>
										<a <?php if ( get_theme_mod( 'footer_image4_url' ) ):?>href="<?php echo get_theme_mod( 'footer_image4_url' );?>" <?php endif; ?>><img src="<?php echo get_theme_mod( 'footer_image4' );?>" alt=""></a>
									</div>
									<?php endif; ?>
	                              
	                                <?php if ( get_theme_mod( 'footer_image5' ) ): ?>
	                                <div>
										<a <?php if ( get_theme_mod( 'footer_image5_url' ) ):?>href="<?php echo get_theme_mod( 'footer_image5_url' );?>" <?php endif; ?>><img src="<?php echo get_theme_mod( 'footer_image5' );?>" alt=""></a>
									</div>
									<?php endif; ?>
	                            
	                                <?php if ( get_theme_mod( 'footer_image6' ) ): ?>
	                                <div>
										<a <?php if ( get_theme_mod( 'footer_image6_url' ) ):?>href="<?php echo get_theme_mod( 'footer_image6_url' );?>" <?php endif; ?>><img src="<?php echo get_theme_mod( 'footer_image6' );?>" alt=""></a>
									</div>
									<?php endif; ?>
	                             
	                             
	                                <?php if ( get_theme_mod( 'footer_image7' ) ): ?>
	                                <div>
										<a <?php if ( get_theme_mod( 'footer_image7_url' ) ):?>href="<?php echo get_theme_mod( 'footer_image7_url' );?>" <?php endif; ?>><img src="<?php echo get_theme_mod( 'footer_image7' );?>" alt=""></a>
									</div>
									<?php endif; ?>
	                              
	                            </div>
	                        </div>
	                    </div>
						
					</div>	
				</div>			

				</footer><!-- #colophon -->
				</div>
			</div>
<a id="button" class="scrollTop"></a>
	</div><!-- container end -->

<?php if ( get_theme_mod( 'copyright' ) ): ?>
	<div class="copy text-center">
		<?php echo get_theme_mod( 'copyright' );?>
	</div>
<?php endif; ?>

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

