<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
$url = home_url();

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

		<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>
		<div id="header-2" class="header">
			<nav class="navbar navbar-expand-sm bg-dark navbar-dark top-nav header-nav">

				<div class="container-fluid">
			  <!-- Brand/logo -->
			  <div class="navbar-brand">
			  	<a  href="#"> <h4>Stay connected :</h4></a>
			  	<ul class="social">
			    	<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			    	<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			    	<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
			    	<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			    </ul>
			</div>
			  			  
			  <!-- Links -->
			  <div class="view-only-tablet dropdown ">
			  <a href="" class="dropdown-toggle" data-toggle="dropdown">Quick Links</a>
			  <div class="search-button">
			      <a href="#" class="search-toggle" data-selector="#header-2"></a>
			    </div>
			  <ul class="navbar-nav menu dropdown-menu">
			    <li class="nav-item yellow">
			      <a class="nav-link " href="<?php echo $url; ?>/join-our-team"> <i class="fa fa-book" aria-hidden="true"></i> join our team</a>
			    </li>
			    <li class="nav-item light_blue"> 
			    	<a class="nav-link" href="<?php echo $url; ?>/get-involved"><i class="fa fa-users" aria-hidden="true"></i> get involved</a>
			     </li>
			    <li class="nav-item light_green">
			      <a class="nav-link" href="<?php echo $url; ?>/enrollment"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Enroll Now</a>
			    </li>
			    <li class="nav-item yellow2">
			      <a class="nav-link" href="#"><i class="fa fa-bus" aria-hidden="true"></i> transportation</a>
			    </li>
			    <li class="nav-item"><a class="nav-link" href="#"> calendars</a>  </li>
			    <li class="nav-item"><a class="nav-link" href="<?php echo $url; ?>/news"> news</a>  </li>
			    <li class="nav-item"><a class="nav-link" href="#"> links</a>  </li>
			    <li class="nav-item"><a class="nav-link" href="<?php echo $url; ?>/contact-directory"> contact</a>  </li>
			    <!-- <li class="nav-item search-button" id="header-2">   <a href="#" class="search-toggle" data-selector="#header-2"></a> </li> -->
			  </ul>
			  <form action="" class="search-box">
			      <input type="text" class="text search-input" placeholder="Type here to search..." />
			    </form>
			</div>			 
			    
			</div>
			</nav>
		</div>

<nav class="navbar navbar-expand-lg navbar-dark primary-menu" role="navigation">
	<div class="container-fluid">
  <a class="navbar-brand project_logo" href="<?php echo $url; ?>"><img src="<?php echo $path; ?>/img/logo.png"></a>
  <button class="navbar-toggler second-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent23"
    aria-controls="navbarSupportedContent23" aria-expanded="false" aria-label="Toggle navigation">
    <div class="animated-icon2"><span></span><span></span><span></span><span></span></div>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent23">
  	<ul class="navbar-nav ml-auto">
  		<li class="nav-item dropdown view-only-mobile">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Quick Links
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item nav-link" href="<?php echo $url; ?>/join-our-team"><i class="fa fa-book" aria-hidden="true"></i> join our team</a>
          <a class="dropdown-item nav-link" href="<?php echo $url; ?>/get-involved"><i class="fa fa-users" aria-hidden="true"></i> get involved</a>       
          <a class="dropdown-item nav-link" href="<?php echo $url; ?>/enrollment"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Enroll Now</a>
          <a class="dropdown-item nav-link" href="#"><i class="fa fa-bus" aria-hidden="true"></i> transportation</a>
          <a class="dropdown-item nav-link" href="#">calendars</a>       
          <a class="dropdown-item nav-link" href="<?php echo $url; ?>/news">news</a>
          <a class="dropdown-item nav-link" href="<?php echo $url; ?>/quick-links">links</a>       
          <a class="dropdown-item nav-link" href="#">contact</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo $url; ?>/home">Home</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link " href="<?php echo $url; ?>/about">  about  <i class='fa fa-angle-double-down'></i>  </a>
        <div class="dropdown-menu one">
          <a class="dropdown-item" href="<?php echo $url; ?>/supertintendent">superintendent & leadership</a>
          <a class="dropdown-item" href="<?php echo $url; ?>/about">Our boards</a>          
          <a class="dropdown-item" href="<?php echo $url; ?>/form-page">policies</a>
          <a class="dropdown-item" href="<?php echo $url; ?>/our-story">our story</a>
          <a class="dropdown-item" href="<?php echo $url; ?>/communication">communication</a>          
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link " href="<?php echo $path; ?>/about-us">  departments  <i class='fa fa-angle-double-down'></i>  </a>
        <div class="dropdown-menu one">
          <a class="dropdown-item" href="<?php echo $url; ?>/accountability">Accountability</a>
          <a class="dropdown-item" href="<?php echo $url; ?>/">bussiness operations</a>          
          <a class="dropdown-item" href="<?php echo $url; ?>/">child nutrition</a>
          <a class="dropdown-item" href="<?php echo $url; ?>/">curriculam & instruction</a>
          <a class="dropdown-item" href="<?php echo $url; ?>/">Fedral programs</a>  
          <a class="dropdown-item" href="<?php echo $url; ?>/">Health & Intervention Services</a>
          <a class="dropdown-item" href="<?php echo $url; ?>/">hospital homebound</a>
          <a class="dropdown-item" href="<?php echo $url; ?>/">human resources</a> 
          <a class="dropdown-item" href="<?php echo $url; ?>/">Information technology </a>
          <a class="dropdown-item" href="<?php echo $url; ?>/">instructional technology</a>
          <a class="dropdown-item" href="<?php echo $url; ?>/">school safety & security</a>  
          <a class="dropdown-item" href="<?php echo $url; ?>/transportation">transportation</a>        
        </div>
      </li>   
       <li class="nav-item dropdown">
        <a class="nav-link " href="<?php echo $url; ?>/programs"> programs<i class='fa fa-angle-double-down'></i></a>
        <div class="dropdown-menu one">
          <a class="dropdown-item" href="#">Adult Education</a>
          <a class="dropdown-item" href="<?php echo $url; ?>/programs">Alternate programs</a>          
          <a class="dropdown-item" href="#">Career & Technical education</a>
          <a class="dropdown-item" href="#">Early chilhood</a>
          <a class="dropdown-item" href="#">English as a second language (ESL) - Title III</a>
          <a class="dropdown-item" href="#">Exception Student services</a>
          <a class="dropdown-item" href="#">Fine Arts</a>
          <a class="dropdown-item" href="#">Gifted & Talented</a>
          <a class="dropdown-item" href="#">JRotc</a>
          <a class="dropdown-item" href="#">Library services</a>
          <a class="dropdown-item" href="#">Magnet</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo $url; ?>/employee">employees</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="<?php echo $url; ?>/families">families</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="<?php echo $url; ?>/school">schools</a>
      </li> 
    </ul>
  </div>  
</div>
</nav>
 
		<!-- <nav class="navbar navbar-expand-md primary-menu"> -->
         <!-- custom menu code :start -->
         
		 <!-- custom menu code :end -->


		<?php /*<?php if ( 'container' == $container ) : ?>
			<div class="container">
		<?php endif; ?>

					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

						<?php else : ?>

							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

						<?php endif; ?>


					<?php } else {
						the_custom_logo();
					} ?><!-- end custom logo -->

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'understrap' ); ?>">
					<span class="navbar-toggler-icon"></span>
				</button>

				<!-- The WordPress Menu goes here -->
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'collapse navbar-collapse',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav ml-auto',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				); ?>
			<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?> */ ?>

		<!-- </nav> --><!-- .site-navigation -->

	</div><!-- #wrapper-navbar end -->

	