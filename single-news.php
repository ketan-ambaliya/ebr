<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$url = home_url();
?>

<div class="wrapper" id="full-width-page-wrapper">
	<div class="container-fluid no-padding">
		<section class="inner-container news_content">	
			<div class="s-news">
				<div class="row">
					    <div class="col-lg-8">
							<?php while ( have_posts() ) : the_post(); ?>
								<h2><?php the_title(); ?></h2>
								<?php $tags =  get_the_tags();?>
		                    <p class="n_date"><span style="color: <?php the_field('tag_color',  'term_'.$tags[0]->term_id); ?>;"><?php  echo $tags[0]->name; ?></span>  <?php echo get_the_date('M. j Y');?></p>
							<?php endwhile;  ?>
							<p class="commontext"><?php the_content();?></p>
						</div>
						<div class="col-lg-4 sidebar-blue-section">
							<div class="blue-border-box one">
								<div class="ribbonpart"><label>Learn more</label></div>
								<?php $show_search_form_in_sidebar = get_field('show_search_form_in_sidebar', 'option');?>
								<?php $show_tags_in_sidebar = get_field('show_tags_in_sidebar', 'option');?>
								<?php $show_archive_in_sidebar = get_field('show_archive_in_sidebar', 'option');?>
								<?php if($show_search_form_in_sidebar == "Yes") {?>
								<form action="<?php echo $url;?>" method="get" class="f_news search-container">
											
							  	      		<input type="text" name="s" class="input-field" id="search-bar" value="<?php the_search_query(); ?>" placeholder="search"/>
							  	      		<input type="hidden" value="search.php">
							  	      		<button type="submit" class="searchButton"><i class="fa fa-search"></i></button>
							  	      		
							  	      	
							  	   		</form>
								<hr/>
								<?php } ?>
								<?php if($show_tags_in_sidebar == "Yes") {?>
								<label>Tags</label>
								<div class="select"><select name="tag-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
						    	<?php
					            $tax = 'post_tag';
					            $terms = get_terms( $tax );
					            $count = count( $terms );?>

				            	<?php if ( $count > 0 ):?>
				              	 <option value=""><?php echo esc_attr( __( 'Select Tag' ) ); ?></option> 
				               	<?php  foreach ( $terms as $term ) :
				                   echo '<option value = '. home_url() .'/tag/'. $term->slug .'>' . $term->name . '</option>'; 
				                endforeach;
				                
			              		endif; ?>
		              			</select>
		              		</div>
		              			<hr/>
		              			<?php } ?>
		              			<?php if($show_archive_in_sidebar == "Yes") {?>
								<label>Archives</label>
								<div class="select">
								<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
								  <option value=""><?php echo esc_attr( __( 'Select Date' ) ); ?></option> 
								  <?php wp_get_archives( array( 'post_type'    => 'news', 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
								</select>
								</div>
								<hr/>
								<?php } ?>
								<label>Find us & Follow us</label>
								<ul>
								
									<?php if ( get_theme_mod( 'facebook_url' ) ):?>
								    		<li>
								    			<a href="<?php echo get_theme_mod( 'facebook_url' );?>" target="_blank" class="f_links">
								    				<i class="fa <?php echo get_theme_mod( 'facebook_icon_class' );?>" aria-hidden="true"></i>Facebook
								    			</a>
								    		</li>
								    	<?php endif; ?>
								    	<?php if ( get_theme_mod( 'twitter_url' ) ):?>
								    		<li>
								    			<a href="<?php echo get_theme_mod( 'twitter_url' );?>" target="_blank" class="f_links">
								    				<i class="fa <?php echo get_theme_mod( 'twitter_icon_class' );?>" aria-hidden="true"></i>Twitter
								    			</a>
								    		</li>
								    	<?php endif; ?>
								    	<?php if ( get_theme_mod( 'youtube_url' ) ):?>
								    		<li>
								    			<a href="<?php echo get_theme_mod( 'youtube_url' );?>" target="_blank" class="f_links">
								    				<i class="fa <?php echo get_theme_mod( 'youtube_icon_class' );?>" aria-hidden="true"></i>Youtube
								    			</a>
								    		</li>
								    	<?php endif; ?>
								    	<?php if ( get_theme_mod( 'instagram_url' ) ):?>
								    		<li>
								    			<a href="<?php echo get_theme_mod( 'instagram_url' );?>" target="_blank" class="f_links">
								    				<i class="fa <?php echo get_theme_mod( 'instagram_icon_class' );?>" aria-hidden="true"></i>Instagram
								    			</a>
								    		</li>
								    	<?php endif; ?>
								</ul>
								<hr/>
								<?php $interested_in_title = get_field('n_interested_in_title', 'option');?>
								<?php if( !empty($interested_in_title)):?><label> <?php echo $interested_in_title; ?> </label><?php endif;?>
								<ul>
									<?php if( have_rows('n_interested_in_links', 'option') ):
										while ( have_rows('n_interested_in_links', 'option') ) : the_row();
										$link_text = get_sub_field('link_text', 'option');
										$link_url = get_sub_field('link_url', 'option');?>
									<?php if( !empty($link_url)):?><li><a class="f_links" href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a></li><?php endif;?>
									<?php endwhile; endif;?>
								</ul>
							</div>
						</div>
				</div>

				<div class="newspart">
					<div class="ss-style-triangles news">		   
	         			<h2>Other news you may be interested in...</h2>	
	         		</div>
	         		<div class="row">
		         	<div class="col-12 col-md-6">
		         		<label> Recent News</label>
		         	</div>
		         	<div class="col-6 col-md-6 d-none d-lg-block">
		         		<div class="link"><a href="<?php echo esc_url( home_url( '/' ) ); ?>news">see all</a></div>
		         	</div>	
		       		</div>
				
					<div class="row">
			    	<?php 			     	  
	       			  $args = array( 'post_type' => 'news', 'posts_per_page' => 2,  'orderby' => 'DESC', 'post__not_in' => array( $post->ID));

	        		  $loop = new WP_Query( $args );
	        		  if ( $loop->have_posts() ) {
	       			  while ( $loop->have_posts() ) : $loop->the_post();?>

	        			
	        				<div class="col-12 col-md-12 col-lg-12 col-xl-6">
	        					<div class="row">
				        <div class="col-12 col-md-8 spacer">
					        <a href="<?php the_permalink(); ?>">
					        	<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <?php  if ( has_post_thumbnail() ) {  
				                                          the_post_thumbnail();  
				                              }elseif( get_theme_mod( 'news_default_image' ) ){
				                                       
				                                echo '<img src="' .get_theme_mod( 'news_default_image' ).'" />';
				                          } else {

				                                  echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
				                                    
				                                }
				                          ?>
		         				    </div>
		         				</div>
			         		</a> 	
						</div>
						<div class="col-12 col-md-4 spacer">
							<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
							<?php $tags =  get_the_tags();?>
				                    <p class="n_date"><span style="color: <?php the_field('tag_color',  'term_'.$tags[0]->term_id); ?>;"><?php  echo $tags[0]->name; ?></span>  <?php echo get_the_date('M. j Y');?></p>
							<p class="sortcontent">	<?php $content = get_the_content();
					                  $content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,270); ?>...
							</p>	
							<a href="<?php the_permalink(); ?>" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a> 
				        </div>
				      
				     </div>
					</div>  

					<?php endwhile; }else {
						echo "No News";
					}?>
					<?php wp_reset_query(); ?>
		         			

		         		   </div>    	
				    </div>
				    </div>
		</section>
	</div>
</div>

<?php get_footer(); ?>
