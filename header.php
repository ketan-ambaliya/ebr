<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
$url = home_url();

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

		<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>
		<div id="header-2" class="header">
			<nav class="navbar navbar-expand-sm bg-dark navbar-dark top-nav header-nav">

				<div class="container-fluid">
			  <!-- Brand/logo -->
			  <div class="navbar-brand">
			  	<?php if ( get_theme_mod( 'Header_social_media_label' ) ):?>
                        <a><h4><?php echo get_theme_mod( 'Header_social_media_label' );?></h4></a>
                   <?php endif; ?>
			  
			  	<ul class="social">
			    	<?php if ( get_theme_mod( 'facebook_url' ) ):?>
			    		<li>
			    			<a href="<?php echo get_theme_mod( 'facebook_url' );?>" target="_blank">
			    				<i class="fa <?php echo get_theme_mod( 'facebook_icon_class' );?>" aria-hidden="true"></i>
			    			</a>
			    		</li>
			    	<?php endif; ?>
			    	<?php if ( get_theme_mod( 'twitter_url' ) ):?>
			    		<li>
			    			<a href="<?php echo get_theme_mod( 'twitter_url' );?>" target="_blank">
			    				<i class="fa <?php echo get_theme_mod( 'twitter_icon_class' );?>" aria-hidden="true"></i>
			    			</a>
			    		</li>
			    	<?php endif; ?>
			    	<?php if ( get_theme_mod( 'youtube_url' ) ):?>
			    		<li>
			    			<a href="<?php echo get_theme_mod( 'youtube_url' );?>" target="_blank">
			    				<i class="fa <?php echo get_theme_mod( 'youtube_icon_class' );?>" aria-hidden="true"></i>
			    			</a>
			    		</li>
			    	<?php endif; ?>
			    	<?php if ( get_theme_mod( 'instagram_url' ) ):?>
			    		<li>
			    			<a href="<?php echo get_theme_mod( 'instagram_url' );?>" target="_blank">
			    				<i class="fa <?php echo get_theme_mod( 'instagram_icon_class' );?>" aria-hidden="true"></i>
			    			</a>
			    		</li>
			    	<?php endif; ?>
			    </ul>
			</div>
			  			  
			  <!-- Links -->
			  <div class="view-only-tablet dropdown ">
			  <a href="" class="dropdown-toggle" data-toggle="dropdown">Quick Links</a>
			  <div class="search-button">
			      <a href="#" class="search-toggle" data-selector="#header-2"></a>
			    </div>
			    <?php wp_nav_menu(
					array(
						'theme_location'  => 'top_header_menu',
						'menu'            => '',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => 'navbar-nav menu dropdown-menu',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 0,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				); ?>
			  
			  <form action="<?php echo $url;?>" method="get" class="search-box">
  	      		<input type="text" name="s" id="search" class="text search-input removeXicon" value="<?php the_search_query(); ?>" placeholder="Type here to search..."/>
  	      		<input type="hidden" value="search.php">
  	   		  </form>
			</div>			 
			    
			</div>
			</nav>
		</div>

<nav class="navbar navbar-expand-lg navbar-dark primary-menu" >
	<div class="container-fluid">

  	<?php 
  	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
	
  	if ( get_theme_mod( 'custom_logo' ) ){ ?>
        <a class="navbar-brand project_logo" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" ><img src="<?php echo $image[0]; ?>" alt="<?php bloginfo( 'name' ); ?>" /></a>
    <?php }else{ ?>
        <h1 class="logo"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" ><?php bloginfo( 'name' ); ?></a></h1>
    <?php } ?>

  <button class="navbar-toggler second-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent23"
    aria-controls="navbarSupportedContent23" aria-expanded="false" aria-label="Toggle navigation">
    <span class="animated-icon2"><span></span><span></span><span></span><span></span></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent23">
  	<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'menu'            => '',
						'container_class' => 'ml-auto',
						'container_id'    => '',
						'menu_class'      => 'navbar-nav',
						'fallback_cb'     => '',
						'menu_id'         => '',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				); ?>
  	
  </div>  
</div>
</nav>
</div><!-- #wrapper-navbar end -->

<?php if(is_front_page()){?>
<div class="container-fluid no-padding">
	<div class="sliderpart">					
			<div class="slider">
				<?php 
		          	if( have_rows('slider_image') ):
		          		while ( have_rows('slider_image') ) : the_row();
          				$banner_image 	   =	get_sub_field('image');	
          			?>         		
				  <div>
				    <img src="<?php echo $banner_image['url']; ?>" alt="">
				  </div>
			  	<?php endwhile; endif; ?>
			</div>
			<div class="slider_footer">	
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-2">
							<?php
				  				$call_to_logo 		= get_field('call_to_action_logo');
				  				$call_to_title 		= get_field('call_to_action_title');
				  				$call_to_sub_title 		= get_field('call_to_action_sub_title');
				  				$button_text = get_field('button_text');
				  				$button_link  = get_field('button_link'); ?>
  					 		<?php if(!empty($call_to_logo)): ?>
  					 			<img src="<?php echo $call_to_logo['url'];?>" alt="<?php echo $call_to_logo['alt'];?>">
							<?php endif;?>
						</div>
						<div class="col-md-7">
							<?php if(!empty($call_to_title)): ?><p class="slider-title"> <?php echo $call_to_title;?></p><?php endif;?>
							<?php if(!empty($call_to_sub_title)): ?><p class="italic-title"> <?php echo $call_to_sub_title;?></p><?php endif;?>
						</div>
						<div class="col-md-3">
							<?php if(!empty($button_link)): ?><a href="<?php echo $button_link;?>" class="btn"><?php echo $button_text;?></a><?php endif;?>
						</div>	
					</div> 			  
				</div>
			</div>
	</div>
</div>
<?php } else { ?>
	<div class="container-fluid no-padding">
		<?php if(is_singular('tribe_events') ){ 

			$banner_image = $path."/img/breadcrumb-ev.jpg";

		 } elseif(is_singular('news') ){ 

			$banner_image = $path."/img/breadcrumb-ne.jpg";
		 

		 } elseif(!empty(get_field('banner_image'))){ 

			$banner_image1 = get_field('banner_image');
			$banner_image = $banner_image1['url'];	

		 } else { 

			$banner_image = $path."/img/breadcrumb.jpg";

		 }?>
		<section class="overlay breadcrumbs " style="background-image: url(<?php echo $banner_image; ?>);">
			<div class="container-fluid">
				<div class="row">					
					<div class="col-md-12">
						<?php get_breadcrumb(); ?>			
					</div>
				</div>
			</div>
		</section>
		<?php if(!empty(get_field('heading')) || !empty(get_field('sub_heading')) || !empty(get_field('description')) || !empty(get_field('heading_icon'))) {?>
		<section class="inner-container paragraphpart">	
				<div class="row">
					<div class="col-md-12 text-center">
						<?php $heading_icon = get_field('heading_icon');?>
						<?php if(!empty($heading_icon)): ?>
							<img src="<?php echo $heading_icon['url']; ?>" alt="">
						<?php endif;?>
						<?php if(!empty(get_field('heading'))): ?><label><?php the_field('heading');?></label><?php endif;?>
						<?php if(!empty(get_field('sub_heading'))): ?><h3><?php the_field('sub_heading');?></h3><?php endif;?>
						<?php if(!empty(get_field('description'))): ?><p><?php the_field('description');?></p><?php endif;?>
					</div>	
				</div>		
		</section>
	    <?php } ?>
		</div>
<?php } ?>
