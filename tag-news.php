<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri(); 
$url = home_url();
?>
<div class="wrapper" id="full-width-page-wrapper">
	<div class="container-fluid no-padding">
		<section class="inner-container news_content">	
			<div class="s-news">
			<div class="row">	
			    <div class="col-lg-8">
				<div class="newspart">
			    <?php 
			       $tag_id = get_queried_object()->term_id;
			     
			    $args = array( 'post_type' => 'news', 'orderby' => 'DESC', 'tag_id' => $tag_id);

	        		  $loop = new WP_Query( $args );
	        	if (  $loop->have_posts() ) : ?>
						<?php the_archive_title( ' <label>', '</label>' ); ?>
					<?php while (  $loop->have_posts() ) :  $loop->the_post(); ?>

						<div class="row">
					        <div class="col-12 col-md-5 spacer">
						        <a href="<?php the_permalink(); ?>">        
						            <div class="imgcontainer">
										<div class="img-thumbnails">	    
							             <?php  if ( has_post_thumbnail() ) {  
					                                          the_post_thumbnail();  
					                              }elseif( get_theme_mod( 'news_default_image' ) ){
					                                       
					                                echo '<img src="' .get_theme_mod( 'news_default_image' ).'" />';
					                          } else {

					                                  echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
					                                    
					                                }
					                          ?> 
						                </div>
									</div>	
								</a> 	
							</div>
							<div class="col-12 col-md-7 spacer">
								<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
								<?php $tags =  get_the_tags();?>
					                    <p class="n_date"><span style="color: <?php the_field('tag_color',  'term_'.$tags[0]->term_id); ?>;"><?php  echo $tags[0]->name; ?></span>  <?php echo get_the_date('M. j Y');?></p>
								<p class="sortcontent">	<?php $content = get_the_content();
						                  $content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,270); ?>...
								</p>	
								<a href="<?php the_permalink(); ?>" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>          
					        </div>
					      <div class="col-12"><hr/></div>
					     </div>

					<?php endwhile; ?>

				<?php else : ?>

					<?php echo "No News Found" ?>

				<?php endif; ?>
			</div>
				<div class="pagination-sec"><?php understrap_pagination(); ?></div>
			    </div>	
			   
			    <div class="col-lg-4 sidebar-blue-section">
					<div class="blue-border-box one">
						<div class="ribbonpart"><label>Learn more</label></div>
						<?php $show_search_form_in_sidebar = get_field('show_search_form_in_sidebar', 'option');?>
								<?php $show_tags_in_sidebar = get_field('show_tags_in_sidebar', 'option');?>
								<?php $show_archive_in_sidebar = get_field('show_archive_in_sidebar', 'option');?>
								<?php if($show_search_form_in_sidebar == "Yes") {?>
								<form action="<?php echo $url;?>" method="get" class="f_news search-container">
											
							  	      		<input type="text" name="s" class="input-field" id="search-bar" value="<?php the_search_query(); ?>" placeholder="search"/>
							  	      		<input type="hidden" value="search.php">
							  	      		<button type="submit" class="searchButton"><i class="fa fa-search"></i></button>
							  	      		
							  	      	
							  	   		</form>
								<hr/>
								<?php } ?>
								<?php if($show_tags_in_sidebar == "Yes") {?>
								<label>Tags</label>
								<div class="select"><select name="tag-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
						    	<?php
					            $tax = 'post_tag';
					            $terms = get_terms( $tax );
					            $count = count( $terms );?>

				            	<?php if ( $count > 0 ):?>
				              	 <option value=""><?php echo esc_attr( __( 'Select Tag' ) ); ?></option> 
				               	<?php  foreach ( $terms as $term ) :
				                   echo '<option value = '. home_url() .'/tag/'. $term->slug .'>' . $term->name . '</option>'; 
				                endforeach;
				                
			              		endif; ?>
		              			</select>
		              		</div>
		              			<hr/>
		              			<?php } ?>
		              			<?php if($show_archive_in_sidebar == "Yes") {?>
								<label>Archives</label>
								<div class="select">
								<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
								  <option value=""><?php echo esc_attr( __( 'Select Date' ) ); ?></option> 
								  <?php wp_get_archives( array( 'post_type'    => 'news', 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
								</select>
								</div>
								<hr/>
								<?php } ?>
						<label>Find us & Follow us</label>
						<ul>
						
							<?php if ( get_theme_mod( 'facebook_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'facebook_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'facebook_icon_class' );?>" aria-hidden="true"></i>Facebook
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'twitter_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'twitter_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'twitter_icon_class' );?>" aria-hidden="true"></i>Twitter
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'youtube_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'youtube_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'youtube_icon_class' );?>" aria-hidden="true"></i>Youtube
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'instagram_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'instagram_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'instagram_icon_class' );?>" aria-hidden="true"></i>Instagram
						    			</a>
						    		</li>
						    	<?php endif; ?>
						</ul>
						<hr/>
						<?php $interested_in_title = get_field('n_interested_in_title', 'option');?>
						<?php if( !empty($interested_in_title)):?><label> <?php echo $interested_in_title; ?> </label><?php endif;?>
						<ul>
							<?php if( have_rows('n_interested_in_links', 'option') ):
								while ( have_rows('n_interested_in_links', 'option') ) : the_row();
								$link_text = get_sub_field('link_text', 'option');
								$link_url = get_sub_field('link_url', 'option');?>
							<?php if( !empty($link_url)):?><li><a class="f_links" href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a></li><?php endif;?>
							<?php endwhile; endif;?>
						</ul>
						
					
					</div>

					<div class="eventpart">
			         	<label>Events</label>	
			         	<div class="row">
			         		<?php global $post;
			    	
			    	
					$get_posts = tribe_get_events(array('posts_per_page'=>'2', 'eventDisplay'=>'upcoming') );
						if($get_posts){
					foreach($get_posts as $post) { setup_postdata($post);
					        ?>
	        			<div class="row">
				        <div class="col-12 col-md-12 spacer">
					        <a href="<?php the_permalink(); ?>">
					        	<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <?php	if ( has_post_thumbnail() ) {  
								                        	the_post_thumbnail();  
								          		}elseif( get_theme_mod( 'event_default_image' ) ){
								          						 
					          						echo '<img src="' .get_theme_mod( 'event_default_image' ).'" />';
													} else {

					          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
								          					
								          			}
								          ?> 
		         					<div class="ribbon">
										  <span class="ribbon2"><?php echo tribe_get_start_date( $post, false, 'M' );?> <br><strong><?php echo tribe_get_start_date( $post, false, 'j' );?> </strong></span>
										</div>
		         				    </div>
		         				</div>
			         		</a> 	
						</div>
						<div class="col-12 col-md-12 spacer">
							<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
							<!-- <p><span>Events</span>  <?php //echo tribe_get_start_date( $post, false, 'M. d Y');?></p> -->
							<p class="sortcontent">
								<?php $content = get_the_content();
					                  $content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,270); ?>...
							</p>	
							<a href="<?php the_permalink(); ?>" class="enlink">See Details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				                           
				        </div>
				      
				     </div>

					<?php } }else {
						echo "No Upcomin Events";
					}?>
					<?php wp_reset_query(); ?>
		         			

		         		<a href="<?php echo esc_url( home_url( '/' ) ); ?>events" class="btn">see all events</a>       	
				    </div>
				</div>	

			   </div>  	
			</div>    
		</section>
	</div>
</div>

<?php get_footer(); ?>
