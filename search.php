<?php
/**
 * The template for displaying search results pages.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="full-width-page-wrapper">
	<div class="container-fluid no-padding">
		<section class="inner-container">	
			<div class="row">	
			    <div class="col-lg-12">

				<?php if ( have_posts() ) : ?>

					<?php /*<header class="page-header">

							<h2 class="page-title">
								<?php
								printf(
								
									esc_html__( 'Search Results for: %s', 'understrap' ),
									'<span>' . get_search_query() . '</span>'
								);
								?>
							</h2>

					</header><!-- .page-header --> */?>

					
					
						<?php while ( have_posts() ) : the_post(); ?>
	 
								
						<?php if ( 'sliders' != get_post_type()){?>
							<h2><?php the_title();?></h2>
							<?php if ( 'ebrforms' == get_post_type() ||  'ebrpolicies' == get_post_type()){?>
							 <?php $attachment_url =  get_field('attachment_url');?>

								<p>Please click the below button to download.</p>
								<a href="<?php echo $attachment_url;?>" class="btn" target="_blank">Download</a> 

							<?php } else {?>

								<p><?php $content = get_the_content();
								                  $content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,270); ?>...
								</p>
								<a href="<?php the_permalink();?>" class="btn">Read More</a> 

							<?php }?>
							<hr/> 
						<?php }?>             
						<?php endwhile; ?>
					

				<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

				<?php endif; ?>

			
			<?php understrap_pagination(); ?>

		</div>
	</div>
</section>
</div>
</div>

<?php get_footer(); ?>
