<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
$url = home_url();
?>

<?php if ( is_month() ) { ?>
<div class="wrapper" id="full-width-page-wrapper">
	<div class="container-fluid no-padding">
		<section class="inner-container news_content">	
			<div class="s-news">
			<div class="row">	
			    <div class="col-lg-8">
				<div class="newspart">
			    <?php if ( have_posts() ) : ?>
						<?php the_archive_title( ' <label>', '</label>' ); ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<div class="row">
					        <div class="col-12 col-md-5 spacer">
						        <a href="<?php the_permalink(); ?>">        
						            <div class="imgcontainer">
										<div class="img-thumbnails">	    
							             <?php  if ( has_post_thumbnail() ) {  
					                                          the_post_thumbnail();  
					                              }elseif( get_theme_mod( 'news_default_image' ) ){
					                                       
					                                echo '<img src="' .get_theme_mod( 'news_default_image' ).'" />';
					                          } else {

					                                  echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
					                                    
					                                }
					                          ?> 
						                </div>
									</div>	
								</a> 	
							</div>
							<div class="col-12 col-md-7 spacer">
								<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
								<?php $tags =  get_the_tags();?>
					                    <p class="n_date"><span style="color: <?php the_field('tag_color',  'term_'.$tags[0]->term_id); ?>;"><?php  echo $tags[0]->name; ?></span>  <?php echo get_the_date('M. j Y');?></p>
								<p class="sortcontent">	<?php $content = get_the_content();
						                  $content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,270); ?>...
								</p>	
								<a href="<?php the_permalink(); ?>" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>          
					        </div>
					      <div class="col-12"><hr/></div>
					     </div>

					<?php endwhile; ?>

				<?php else : ?>

					<?php echo "No News Found" ?>

				<?php endif; ?>
			</div>
				<div class="pagination-sec"><?php understrap_pagination(); ?></div>
			    </div>	
			   
			    <div class="col-lg-4 sidebar-blue-section">
					<div class="blue-border-box one">
						<div class="ribbonpart"><label>Learn more</label></div>
						
						<?php $show_search_form_in_sidebar = get_field('show_search_form_in_sidebar', 'option');?>
								<?php $show_tags_in_sidebar = get_field('show_tags_in_sidebar', 'option');?>
								<?php $show_archive_in_sidebar = get_field('show_archive_in_sidebar', 'option');?>
								<?php if($show_search_form_in_sidebar == "Yes") {?>
								<form action="<?php echo $url;?>" method="get" class="f_news search-container">
											
							  	      		<input type="text" name="s" class="input-field" id="search-bar" value="<?php the_search_query(); ?>" placeholder="search"/>
							  	      		<input type="hidden" value="search.php">
							  	      		<button type="submit" class="searchButton"><i class="fa fa-search"></i></button>
							  	      		
							  	      	
							  	   		</form>
								<hr/>
								<?php } ?>
								<?php if($show_tags_in_sidebar == "Yes") {?>
								<label>Tags</label>
								<div class="select"><select name="tag-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
						    	<?php
					            $tax = 'post_tag';
					            $terms = get_terms( $tax );
					            $count = count( $terms );?>

				            	<?php if ( $count > 0 ):?>
				              	 <option value=""><?php echo esc_attr( __( 'Select Tag' ) ); ?></option> 
				               	<?php  foreach ( $terms as $term ) :
				                   echo '<option value = '. home_url() .'/tag/'. $term->slug .'>' . $term->name . '</option>'; 
				                endforeach;
				                
			              		endif; ?>
		              			</select>
		              		</div>
		              			<hr/>
		              			<?php } ?>
		              			<?php if($show_archive_in_sidebar == "Yes") {?>
								<label>Archives</label>
								<div class="select">
								<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
								  <option value=""><?php echo esc_attr( __( 'Select Date' ) ); ?></option> 
								  <?php wp_get_archives( array( 'post_type'    => 'news', 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
								</select>
								</div>
								<hr/>
								<?php } ?>
						<label>Find us & Follow us</label>
						<ul>
						
							<?php if ( get_theme_mod( 'facebook_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'facebook_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'facebook_icon_class' );?>" aria-hidden="true"></i>Facebook
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'twitter_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'twitter_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'twitter_icon_class' );?>" aria-hidden="true"></i>Twitter
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'youtube_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'youtube_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'youtube_icon_class' );?>" aria-hidden="true"></i>Youtube
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'instagram_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'instagram_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'instagram_icon_class' );?>" aria-hidden="true"></i>Instagram
						    			</a>
						    		</li>
						    	<?php endif; ?>
						</ul>
						<hr/>
						<?php $interested_in_title = get_field('n_interested_in_title', 'option');?>
						<?php if( !empty($interested_in_title)):?><label> <?php echo $interested_in_title; ?> </label><?php endif;?>
						<ul>
							<?php if( have_rows('n_interested_in_links', 'option') ):
								while ( have_rows('n_interested_in_links', 'option') ) : the_row();
								$link_text = get_sub_field('link_text', 'option');
								$link_url = get_sub_field('link_url', 'option');?>
							<?php if( !empty($link_url)):?><li><a class="f_links" href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a></li><?php endif;?>
							<?php endwhile; endif;?>
						</ul>
						
					
					</div>

					<div class="eventpart">
			         	<label>Events</label>	
			         	<div class="row">
			         		<?php global $post;
			    	
			    	
					$get_posts = tribe_get_events(array('posts_per_page'=>'2', 'eventDisplay'=>'upcoming') );
						if($get_posts){
					foreach($get_posts as $post) { setup_postdata($post);
					        ?>
	        			<div class="row">
				        <div class="col-12 col-md-12 spacer">
					        <a href="<?php the_permalink(); ?>">
					        	<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <?php	if ( has_post_thumbnail() ) {  
								                        	the_post_thumbnail();  
								          		}elseif( get_theme_mod( 'event_default_image' ) ){
								          						 
					          						echo '<img src="' .get_theme_mod( 'event_default_image' ).'" />';
													} else {

					          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
								          					
								          			}
								          ?> 
		         					<div class="ribbon">
										  <span class="ribbon2"><?php echo tribe_get_start_date( $post, false, 'M' );?> <br><strong><?php echo tribe_get_start_date( $post, false, 'j' );?> </strong></span>
										</div>
		         				    </div>
		         				</div>
			         		</a> 	
						</div>
						<div class="col-12 col-md-12 spacer">
							<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
							<!-- <p><span>Events</span>  <?php //echo tribe_get_start_date( $post, false, 'M. d Y');?></p> -->
							<p class="sortcontent">
								<?php $content = get_the_content();
					                  $content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,270); ?>...
							</p>	
							<a href="<?php the_permalink(); ?>" class="enlink">See Details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				                           
				        </div>
				      
				     </div>

					<?php } }else {
						echo "No Upcomin Events";
					}?>
					<?php wp_reset_query(); ?>
		         			

		         		<a href="<?php echo esc_url( home_url( '/' ) ); ?>events" class="btn">see all events</a>       	
				    </div>
				</div>	
</div>
			    	
			</div>    
		</section>
	</div>
</div>

<?php } else { ?>
<script>
jQuery("document").ready(function() {
setTimeout(function() {
   jQuery("#tag-all .nav-link").trigger('click');
},10);
});

function tag_ajax_get(tagID) {
	jQuery('.nav-link').removeClass('active');

	jQuery('#tag-'+tagID+' .nav-link').addClass('active');

    jQuery(".loading_image").show();
        var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
    jQuery.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {"action": "load-filter", tag: tagID },
        success: function(response) {
            jQuery(".newspart").html(response);
            jQuery(".loading_image").hide();
            return false;
        }
    });
}
function cptaajaxPagination(pnumber,plimit){
    var nth  = pnumber;
    var lmt  = plimit;
    var cpta = jQuery("#post").attr('data-posttype');
    var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
    jQuery.ajax({
        url     :ajaxurl,
        type    :'post',
        data    :{ 'action':'cptapagination','number':nth,'limit':lmt,'cptapost':cpta },
        beforeSend  : function(){
            jQuery(".loading_image").show();
        },
        success :function(pvalue){
            jQuery(".newspart").html(pvalue);
            jQuery(".loading_image").hide();

        }
    });
}
</script>
<div class="loading_image" style="display: none;"><img src="<?php echo $path; ?>/img/small.png"><div class="loader"></div></div>		
<div class="container-fluid no-padding">
		
	<?php if(!empty(get_field('heading', 'option')) || !empty(get_field('sub_heading', 'option')) || !empty(get_field('description', 'option')) || !empty(get_field('heading_icon', 'option'))) {?>
	<section class="inner-container paragraphpart">	
			<div class="row">
				<div class="col-md-12 text-center">
					<?php $heading_icon = get_field('heading_icon', 'option');?>
					<?php if(!empty($heading_icon)): ?>
						<img src="<?php echo $heading_icon['url']; ?>">
					<?php endif;?>
					<?php if(!empty(get_field('heading', 'option'))): ?><label><?php the_field('heading', 'option');?></label><?php endif;?>
					<?php if(!empty(get_field('sub_heading', 'option'))): ?><h3><?php the_field('sub_heading', 'option');?></h3><?php endif;?>
					<?php if(!empty(get_field('description', 'option'))): ?><p><?php the_field('description', 'option');?></p><?php endif;?>
				</div>	
			</div>		
	</section>
    <?php } ?>
    
</div>
<div class="wrapper" id="full-width-page-wrapper">
	<div class="container-fluid no-padding">
		<section class="inner-container news_content">	
			<div class="s-news">
			<div class="row">	
			    <div class="col-lg-8">
			    	
			    	<label>Recent News </label>
			    	<ul class="nav nav-tabs" id="myTab" role="tablist">
				    	<?php
			            $tax = 'post_tag';
			            $terms = get_terms( $tax );
			            $count = count( $terms );?>

			            <li class="nav-item" id="tag-all" >
						    <a class="nav-link active" onclick="tag_ajax_get();" role="tab" data-toggle="tab">All</a>
					    </li>

		            	<?php if ( $count > 0 ):
		               
		                foreach ( $terms as $term ) :
		                   echo '<li class="nav-item" id="tag-'.$term->term_id.'"><a class="nav-link" onclick="tag_ajax_get('.$term->term_id.');" role="tab" data-toggle="tab">' . $term->name . '</a></li>'; 
		                endforeach;
		                
	              		endif; ?>
              		</ul>

              		<div class="tab-content"><div class="newspart"></div>
              	<div class="pagination-sec"></div></div>

			    </div>	
			   
			    <div class="col-lg-4 sidebar-blue-section">
					<div class="blue-border-box one">
						<div class="ribbonpart"><label>Learn more</label></div>
						
						<?php $show_search_form_in_sidebar = get_field('show_search_form_in_sidebar', 'option');?>
								<?php $show_tags_in_sidebar = get_field('show_tags_in_sidebar', 'option');?>
								<?php $show_archive_in_sidebar = get_field('show_archive_in_sidebar', 'option');?>
								<?php if($show_search_form_in_sidebar == "Yes") {?>
								<form action="<?php echo $url;?>" method="get" class="f_news search-container">
											
							  	      		<input type="text" name="s" class="input-field" id="search-bar" value="<?php the_search_query(); ?>" placeholder="search"/>
							  	      		<input type="hidden" value="search.php">
							  	      		<button type="submit" class="searchButton"><i class="fa fa-search"></i></button>
							  	      		
							  	      	
							  	   		</form>
								<hr/>
								<?php } ?>
								<?php if($show_tags_in_sidebar == "Yes") {?>
								<label>Tags</label>
								<div class="select"><select name="tag-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
						    	<?php
					            $tax = 'post_tag';
					            $terms = get_terms( $tax );
					            $count = count( $terms );?>

				            	<?php if ( $count > 0 ):?>
				              	 <option value=""><?php echo esc_attr( __( 'Select Tag' ) ); ?></option> 
				               	<?php  foreach ( $terms as $term ) :
				                   echo '<option value = '. home_url() .'/tag/'. $term->name .'>' . $term->name . '</option>'; 
				                endforeach;
				                
			              		endif; ?>
		              			</select>
		              		</div>
		              			<hr/>
		              			<?php } ?>
		              			<?php if($show_archive_in_sidebar == "Yes") {?>
								<label>Archives</label>
								<div class="select">
								<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
								  <option value=""><?php echo esc_attr( __( 'Select Date' ) ); ?></option> 
								  <?php wp_get_archives( array( 'post_type'    => 'news', 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
								</select>
								</div>
								<hr/>
								<?php } ?>
						<label>Find us & Follow us</label>
						<ul>
						
							<?php if ( get_theme_mod( 'facebook_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'facebook_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'facebook_icon_class' );?>" aria-hidden="true"></i>Facebook
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'twitter_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'twitter_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'twitter_icon_class' );?>" aria-hidden="true"></i>Twitter
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'youtube_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'youtube_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'youtube_icon_class' );?>" aria-hidden="true"></i>Youstube
						    			</a>
						    		</li>
						    	<?php endif; ?>
						    	<?php if ( get_theme_mod( 'instagram_url' ) ):?>
						    		<li>
						    			<a href="<?php echo get_theme_mod( 'instagram_url' );?>" target="_blank" class="f_links">
						    				<i class="fa <?php echo get_theme_mod( 'instagram_icon_class' );?>" aria-hidden="true"></i>Instagram
						    			</a>
						    		</li>
						    	<?php endif; ?>
						</ul>
						<hr/>
						<?php $interested_in_title = get_field('n_interested_in_title', 'option');?>
						<?php if( !empty($interested_in_title)):?><label> <?php echo $interested_in_title; ?> </label><?php endif;?>
						<ul>
							<?php if( have_rows('n_interested_in_links', 'option') ):
								while ( have_rows('n_interested_in_links', 'option') ) : the_row();
								$link_text = get_sub_field('link_text', 'option');
								$link_url = get_sub_field('link_url', 'option');?>
							<?php if( !empty($link_url)):?><li><a class="f_links" href="<?php echo $link_url; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $link_text; ?></a></li><?php endif;?>
							<?php endwhile; endif;?>
						</ul>
						
					
					</div>

					<div class="eventpart">
			         	<label>Events</label>	
			         	<div class="row">
			         		<?php global $post;
			    	
			    	
					$get_posts = tribe_get_events(array('posts_per_page'=>'2', 'eventDisplay'=>'upcoming') );
						if($get_posts){
					foreach($get_posts as $post) { setup_postdata($post);
					        ?>
	        			<div class="row">
				        <div class="col-12 col-md-12 spacer">
					        <a href="<?php the_permalink(); ?>">
					        	<div class="imgcontainer">
		         					<div class="img-thumbnails">
		         					  <?php	if ( has_post_thumbnail() ) {  
								                        	the_post_thumbnail();  
								          		}elseif( get_theme_mod( 'event_default_image' ) ){
								          						 
					          						echo '<img src="' .get_theme_mod( 'event_default_image' ).'" />';
													} else {

					          					    echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
								          					
								          			}
								          ?> 
		         					<div class="ribbon">
										  <span class="ribbon2"><?php echo tribe_get_start_date( $post, false, 'M' );?> <br><strong><?php echo tribe_get_start_date( $post, false, 'j' );?> </strong></span>
										</div>
		         				    </div>
		         				</div>
			         		</a> 	
						</div>
						<div class="col-12 col-md-12 spacer">
							<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
							<!-- <p><span>Events</span>  <?php //echo tribe_get_start_date( $post, false, 'M. d Y');?></p> -->
							<p class="sortcontent">
								<?php $content = get_the_content();
					                  $content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,270); ?>...
							</p>	
							<a href="<?php the_permalink(); ?>" class="enlink">See Details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
				                           
				        </div>
				      
				     </div>

					<?php } }else {
						echo "No Upcomin Events";
					}?>
					<?php wp_reset_query(); ?>
		         			

		         		<a href="<?php echo esc_url( home_url( '/' ) ); ?>events" class="btn">see all events</a>       	
				    </div>
				</div>	

			    	</div>
			</div>    
		</section>

		<section class="inner-container instagram">
			 <div class="row">
		         	<div class="col-12 text-center">
		         		<label>let's get social !</label>
		         	</div>		         		
		     </div>
		</section>	

					

	</div>

</div>
<?php } ?>
<?php get_footer(); ?>
