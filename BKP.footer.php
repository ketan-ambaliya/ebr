<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper no-padding" id="wrapper-footer">

	<div class="container-fluid no-padding">
    <div class="row">
        <div class="col-md-12 ">
				<footer class="site-footer" id="colophon">					
					<div class="newsletter">
						<div class="content-conainer">
							<div class="row">
							<div class="col-md-6 col-lg-2">
							 <p class="italicfont">follow</p>
							 <p class="headingfont">ebrpress</p>	 
							</div>
							<div class="col-md-6 col-lg-4">
								<ul class="social">
							    	<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							    	<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							    	<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
							    	<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							    </ul>
							</div>
							<div class="col-md-6 col-lg-2">
							 <p class="italicfont">Join our</p>
							 <p class="headingfont">newsletters</p>	 
							</div>
							<div class="col-md-6 col-lg-4">
								<form class="f_news" action="">
								  <div class="input-container">								   
								    <input class="input-field" type="text" placeholder="Email" name="email">
								    <a href="" class="icon"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
								  </div>
								</form>	
							</div>
							<div class="clerfix"></div>
						</div>
						</div>	
					</div>	
					<div class="footer-part">
						<div class="container-fluid">
						<div class="row">
							<div class="col-md-12 col-lg-4 col-xl-2">
								<a class="project_logo" href="#"><img src="<?php echo $path; ?>/img/footer-logo.png"></a>
								<ul class="addresspart">
									<li><label>phone</label>:<a href="tel:225.922.5400"> 225.922.5400</a></li>
									<li><label>fax</label> : 225.922.5499</li>
									<li><label>address</label> : 1050 S. Foster Dr. Baton Rouge LA 70806</li>
									<li><label>email</label> : <a href="mailto:info@ebrschools.org">info@ebrschools.org</a></li>
								</ul>
								<form class="f_news" action="">
								  <div class="input-container">	
								  <a href="" class="icon"><i class="fa fa-search" aria-hidden="true"></i></a>			   
								    <input class="input-field" type="text" placeholder="search the site" name="search the site">
								  </div>
								</form>	
							</div>
							<div class="col-md-12 col-lg-8 col-xl-6">
								<h2 class="no-background"><span>site map</span></h2>
								<div class="row">
									<div class="col-md-6">
										<label class="quick-links">Departments</label>
										<ul> 
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> accountability</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> bussiness operations</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> child welfare & attendance</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> child nutrition</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> federal programs</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Health & Intervention</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Human Resources</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Information</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Technology</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Transportation</a></li>
										</ul>
								   </div>
								   <div class="col-md-6">
										<label class="quick-links">Programs</label>
										<ul> 
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Adult Education</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Career & Technical Education</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Magnet Programs</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Gifted & Talented</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Exceptional Student Services</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> English as a Second Language (Title III)</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Fine Arts</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Athletics</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Early Childhood</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Library Services</a></li>
											<li><a class="nav-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Summer Programs</a></li>
										</ul>
								   </div>
							    </div>
							</div>	
							<div class="col-md-12 col-lg-12 col-xl-4">
							 <div class="videopart">
							 	<img src="<?php echo $path; ?>/img/imgvideo.png">
							 	<div class="videocontent">
							 		<h3>this is it</h3>
							 		<span class="smallcontent"> The time is now.</span>
							 		<a href="https://www.youtube.com/embed/SqGlbqfTzyk" class="fancybox fancybox.iframe"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
							 </div>
							 </div>
							</div>
							<div class="clerfix"></div>
						</div>	
						<hr>
						<p class="privacy">
							The East Baton Rouge Parish School System and all of its entities (including Career and Technical Education Programs) does not discriminate on the basis of age, race, religion, national origin, disability or gender in its educational programs and activities (including employment and application for employment), and it is prohibited from discriminating on the basis of gender by Title IX (20 USC 168) and on the basis of disability by Section 504 (42 USC 794). The Title IX Coordinator is Andrew Davis (ADavis6@ebrschools.org), Director of Risk Management – phone<a href="tel:225.922.5400">(225) 929-8705</a>.  The Section 504 Coordinator is  Elizabeth Taylor Chapman (ETaylor@ebrschools.org),  Director of Exceptional Student Services – phone (225) 929-8600.
						</p>
						<div class="container brand_section">
						<div class="row">
							<div class="col-md-4 text-center">
								<a href="#"><img src="<?php echo $path; ?>/img/f-1.png"></a>
						   </div>
						   <div class="col-md-4 text-center">
								<a href="#"><img src="<?php echo $path; ?>/img/f-2.png"></a>
						   </div>
						   <div class="col-md-4 text-center">
								<a href="#"><img src="<?php echo $path; ?>/img/f-3.png"></a>
						   </div>
						   <div class="clerfix"></div>
						</div>
					</div>
					</div>	
				</div>			


					<?php /*<div class="site-info">

						<?php understrap_site_info(); ?>

					</div><!-- .site-info -->
				*/ ?>  
				</footer><!-- #colophon -->
				</div>
			</div>

	</div><!-- container end -->
<div class="copy text-center">
	&copy; 2019  East Baton Rouge Parish School System. Site Design by Torapath Technologies.
</div>

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

