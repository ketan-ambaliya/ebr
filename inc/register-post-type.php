<?php 

// Our custom post type function

function custom_news_posttype() {

 

    // Set UI labels for Custom Post Type

    $labels = array(

        'name'                => _x( 'News', 'Post Type General Name'),

        'singular_name'       => _x( 'News', 'Post Type Singular Name'),

        'menu_name'           => __( 'News'),

        'all_items'           => __( 'All News'),

        'view_item'           => __( 'View News'),

        'add_new_item'        => __( 'Add New News'),

        'add_new'             => __( 'Add New'),

        'edit_item'           => __( 'Edit News'),

        'update_item'         => __( 'Update News'),

        'search_items'        => __( 'Search News'),

        'not_found'           => __( 'Not Found'),

        'not_found_in_trash'  => __( 'Not found in Trash'),

    );

     

    // Set other options for Custom Post Type

    $args = array(

        'label'               => __( 'news'),

        'labels'              => $labels,

        'supports'            => array( 'title', 'editor', 'author', 'thumbnail' ),

        'taxonomies'          => array( 'category', 'post_tag' ),

        'hierarchical'        => true,

        'public'              => true,

        'show_ui'             => true,

        'show_in_menu'        => true,

        'show_in_nav_menus'   => true,

        'show_in_admin_bar'   => true,

        'menu_position'       => 5,

        'can_export'          => true,

        'has_archive'         => true,

        'exclude_from_search' => false,

        'publicly_queryable'  => true,

        'capability_type'     => 'page',

    );

     

    register_post_type( 'news', $args );

 

}

add_action( 'init', 'custom_news_posttype', 0 );



function custom_contact_posttype() {



     // Set UI labels for Custom Post Type

    $labels = array(

        'name'                => _x( 'Department Contacts', 'Post Type General Name'),

        'singular_name'       => _x( 'Department Contact', 'Post Type Singular Name'),

        'menu_name'           => __( 'Department Contacts'),

        'all_items'           => __( 'All Department Contacts'),

        'view_item'           => __( 'View Department Contacts'),

        'add_new_item'        => __( 'Add New Department Contact'),

        'add_new'             => __( 'Add New'),

        'edit_item'           => __( 'Edit Department Contact'),

        'update_item'         => __( 'Update Department Contact'),

        'search_items'        => __( 'Search Department Contact'),

        'not_found'           => __( 'Not Found'),

        'not_found_in_trash'  => __( 'Not found in Trash'),

    );

     

    // Set other options for Custom Post Type

    $args = array(

        'label'               => __( 'department-contacts'),

        'labels'              => $labels,

        'supports'            => array( 'title', 'editor', 'author', 'thumbnail' ),

        'taxonomies'          => array( 'category' ),

        'hierarchical'        => true,

        'public'              => true,

        'show_ui'             => true,

        'show_in_menu'        => true,

        'show_in_nav_menus'   => true,

        'show_in_admin_bar'   => true,

        'menu_position'       => 5,

        'can_export'          => true,

        'has_archive'         => true,

        'exclude_from_search' => true,


        'publicly_queryable'  => true,

        'capability_type'     => 'page',

    );

     

    register_post_type( 'department-contacts', $args );



}

add_action( 'init', 'custom_contact_posttype', 1 );



function custom_school_posttype() {



     // Set UI labels for Custom Post Type

    $labels = array(

        'name'                => _x( 'Schools', 'Post Type General Name'),

        'singular_name'       => _x( 'School', 'Post Type Singular Name'),

        'menu_name'           => __( 'Schools'),

        'all_items'           => __( 'All Schools'),

        'view_item'           => __( 'View Schools'),

        'add_new_item'        => __( 'Add New School'),

        'add_new'             => __( 'Add New'),

        'edit_item'           => __( 'Edit School'),

        'update_item'         => __( 'Update School'),

        'search_items'        => __( 'Search School'),

        'not_found'           => __( 'Not Found'),

        'not_found_in_trash'  => __( 'Not found in Trash'),

    );

     

    // Set other options for Custom Post Type

    $args = array(

        'label'               => __( 'ebrschools'),

        'labels'              => $labels,

        'supports'            => array( 'title', 'editor', 'author', 'thumbnail' ),

        'taxonomies'          => array( 'category' ),

        'hierarchical'        => true,

        'public'              => true,

        'show_ui'             => true,

        'show_in_menu'        => true,

        'show_in_nav_menus'   => true,

        'show_in_admin_bar'   => true,

        'menu_position'       => 5,

        'can_export'          => true,

        'has_archive'         => true,

        'exclude_from_search' => false,

        'publicly_queryable'  => true,

        'capability_type'     => 'page',

    );

     

    register_post_type( 'ebrschools', $args );



}

add_action( 'init', 'custom_school_posttype', 2 );



function custom_ebrpolicies_posttype() {



     // Set UI labels for Custom Post Type

    $labels = array(

        'name'                => _x( 'Policies', 'Post Type General Name'),

        'singular_name'       => _x( 'Policy', 'Post Type Singular Name'),

        'menu_name'           => __( 'Policies'),

        'all_items'           => __( 'All Policies'),

        'view_item'           => __( 'View Policies'),

        'add_new_item'        => __( 'Add New Policy'),

        'add_new'             => __( 'Add New'),

        'edit_item'           => __( 'Edit Policy'),

        'update_item'         => __( 'Update Policy'),

        'search_items'        => __( 'Search Policy'),

        'not_found'           => __( 'Not Found'),

        'not_found_in_trash'  => __( 'Not found in Trash'),

    );

     

    // Set other options for Custom Post Type

    $args = array(

        'label'               => __( 'ebrpolicies'),

        'labels'              => $labels,

        'supports'            => array( 'title', 'author', 'thumbnail' ),

        'taxonomies'          => array( 'category' ),

        'hierarchical'        => true,

        'public'              => true,

        'show_ui'             => true,

        'show_in_menu'        => true,

        'show_in_nav_menus'   => true,

        'show_in_admin_bar'   => true,

        'menu_position'       => 5,

        'can_export'          => true,

        'has_archive'         => true,

        'exclude_from_search' => false,

        'publicly_queryable'  => true,

        'capability_type'     => 'page',

    );

     

    register_post_type( 'ebrpolicies', $args );



}

add_action( 'init', 'custom_ebrpolicies_posttype', 3 );



function custom_ebrforms_posttype() {



     // Set UI labels for Custom Post Type

    $labels = array(

        'name'                => _x( 'Forms', 'Post Type General Name'),

        'singular_name'       => _x( 'Form', 'Post Type Singular Name'),

        'menu_name'           => __( 'Forms'),

        'all_items'           => __( 'All Forms'),

        'view_item'           => __( 'View Forms'),

        'add_new_item'        => __( 'Add New Form'),

        'add_new'             => __( 'Add New'),

        'edit_item'           => __( 'Edit Form'),

        'update_item'         => __( 'Update Form'),

        'search_items'        => __( 'Search Form'),

        'not_found'           => __( 'Not Found'),

        'not_found_in_trash'  => __( 'Not found in Trash'),

    );

     

    // Set other options for Custom Post Type

    $args = array(

        'label'               => __( 'ebrforms'),

        'labels'              => $labels,

        'supports'            => array( 'title', 'author', 'thumbnail' ),

        'taxonomies'          => array( 'category' ),

        'hierarchical'        => true,

        'public'              => true,

        'show_ui'             => true,

        'show_in_menu'        => true,

        'show_in_nav_menus'   => true,

        'show_in_admin_bar'   => true,

        'menu_position'       => 5,

        'can_export'          => true,

        'has_archive'         => true,

        'exclude_from_search' => false,

        'publicly_queryable'  => true,

        'capability_type'     => 'page',

    );

     

    register_post_type( 'ebrforms', $args );



}

add_action( 'init', 'custom_ebrforms_posttype', 4 );



function add_categories_to_events() {

    register_taxonomy_for_object_type( 'category', 'tribe_events' );

}

add_action( 'init', 'add_categories_to_events' );