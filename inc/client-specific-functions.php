<?php

/**

 * You client/theme specific functions go here

*/



// Include custom post

include_once('register-post-type.php');



//Generate breadcrumbs

function get_breadcrumb() {

  $showOnHome = 1; // 1 - show breadcrumbs on the homepage, 0 - don't show

  $delimiter = ''; // delimiter between crumbs

  $home = 'Home'; // text for the 'Home' link

  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show

  $before = '<li class="active">'; // tag before the current crumb

  $after = '</li>'; // tag after the current crumb

 

  global $post;

  $homeLink = get_bloginfo('url');

  $blogLink = get_permalink( get_option( 'page_for_posts' ) ); 

  

  if (is_home()) { 

 

    if ($showOnHome == 1) echo '<div id="crumbs" class="breadcrumblink"><ul><li><a  href="' . $homeLink . '">' . $home . '</a></li><li><span class="current">Blog</span></li></ul></div>';

 

  } else {

 

    echo '<div id="crumbs" class="breadcrumblink"><ul><li><a href="' . $homeLink . '">' . $home . '</a></li>' . $delimiter . ' ';

 

  

    if ( is_category() ) {

      $thisCat = get_category(get_query_var('cat'), false);

      if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');

      echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;

 

    } elseif ( is_search() ) {

      echo $before . 'Search results for "' . get_search_query() . '"' . $after;

 

    } elseif ( is_day() ) {

      echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';

      echo '<li><a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a></li>' . $delimiter . ' ';

      echo $before . get_the_time('d') . $after;

 

    } elseif ( is_month() ) {

      echo '<li>' . get_the_time('Y') . '</li>' . $delimiter . ' ';

      echo $before . get_the_time('F') . $after;

 

    } elseif ( is_year() ) {

      echo $before . get_the_time('Y') . $after;

 

    } elseif ( is_single() && !is_attachment() ) {

      if ( get_post_type() != 'post' ) {

        $post_type = get_post_type_object(get_post_type());

    
        $slug = strtolower($post_type->label);


        echo '<li><a href="' . $homeLink.'/'.$slug.'">' . $post_type->label . '</a></li>';

        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;

      } else {

        $cat = get_the_category(); $cat = $cat[0];

        $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');

        if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);

        //echo $cats;

        if ($showCurrent == 1) echo $before . get_the_title() . $after;

      }

 

    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {

      $post_type = get_post_type_object(get_post_type());

      echo $before . $post_type->label . $after;

 

    } elseif ( is_attachment() ) {

      $parent = get_post($post->post_parent);

      $cat = get_the_category($parent->ID); $cat = $cat[0];

      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');

      echo '<li><a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a><li>';

      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;

 

    } elseif ( is_page() && !$post->post_parent ) {

      if ($showCurrent == 1) echo $before . get_the_title() . $after;

 

    } elseif ( is_page() && $post->post_parent ) {

      $parent_id  = $post->post_parent;

      $breadcrumbs = array();

      while ($parent_id) {

        $page = get_page($parent_id);

        $breadcrumbs[] = '<li><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li>';

        $parent_id  = $page->post_parent;

      }

      $breadcrumbs = array_reverse($breadcrumbs);

      for ($i = 0; $i < count($breadcrumbs); $i++) {

        echo $breadcrumbs[$i];

        if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';

      }

      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;

 

    } elseif ( is_tag() ) {

      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;

 

    } elseif ( is_author() ) {

       global $author;

      $userdata = get_userdata($author);

      echo $before . 'Articles posted by ' . $userdata->display_name . $after;

    

    }elseif ( get_query_var('paged') ) {

      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';

      echo $before.' Event'.$before. __('Page') . ' ' . get_query_var('paged');

      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';

    }

     elseif ( is_404() ) {

      echo $before . 'Error 404' . $after;

    }

 

    

 

    echo '</ul></div>';

 

  }

}





//Include custom menu location 

if ( ! function_exists( 'ebr_register_nav_menu' ) ) {

  function ebr_register_nav_menu(){

    register_nav_menus( array(

      'top_header_menu' => __( 'Top header menu', 'text_domain' ),

    ) );

    register_nav_menus( array(

      'footer_departments' => __( 'Footer Departments', 'text_domain' ),

    ) );

    register_nav_menus( array(

      'footer_programs' => __( 'Footer Programs', 'text_domain' ),

    ) );

  }

  add_action( 'after_setup_theme', 'ebr_register_nav_menu', 0 );

}





function ebr_customize($wp_customize) { 

  //create panel for EBR Foundationpress

   $wp_customize->add_panel('ebr_foundationpress_options', array(

       'priority'          => 2002,

       'title'             => __('Theme Options', $text_domain),

       'description'       => __('Common Options', $text_domain)

   ));

   //Create Custom section for Social Media Accounts

        $wp_customize->add_section('social_media', array(

            'priority'          => 1003,

            'panel'             => 'ebr_foundationpress_options',

            'title'             => __('Social Media', $text_domain),

            'description'       => __('Social Media Accounts', $text_domain)

        ));


        



       //Create Custom section for Company Information

       $wp_customize->add_section('company_info', array(

           'priority'          => 1004,

           'panel'             => 'ebr_foundationpress_options',

           'title'             => __('Company Information', $text_domain),

           'description'       => __('Edit Company Information', $text_domain)

       ));



       //Create Custom section for Company Address

       $wp_customize->add_section('company_address', array(

           'priority'          => 1005,

           'panel'             => 'ebr_foundationpress_options',

           'title'             => __('Company Address', $text_domain),

           'description'       => __('Edit Company Address', $text_domain)

       ));



       //Create Custom section for Mailing Address

       $wp_customize->add_section('mailing_address', array(

           'priority'          => 1006,

           'panel'             => 'ebr_foundationpress_options',

           'title'             => __('Mailing Address', $text_domain),

           'description'       => __('Edit Mailing Address', $text_domain)

       ));



  //Create custom section for Social Media Links

  $wp_customize->add_section( 'Social_media', array(

    'priority'          => 1001,

    'panel'             => 'ebr_foundationpress_options',

    'title'             => __( 'Social Media Links', $text_domain ),

    'description'       => __( 'Update the Social Media Links', $text_domain )

  ));

  //Create Custom section for default image

        $wp_customize->add_section('news_event_image', array(

            'priority'          => 1006,

            'panel'             => 'ebr_foundationpress_options',

            'title'             => __('News & Events Image', $text_domain),

            'description'       => __('', $text_domain)

        ));



  $wp_customize->add_setting('news_default_image');

  //Add option to upload news default image
  $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'news_default_image', array(

    'label'             => __('News Default Image', $text_domain),

    'section'           => 'news_event_image',

    'settings'          => 'news_default_image',

    'description'       => __('Best Image Size (580 X 435)', $text_domain)
    

  )));

  $wp_customize->add_setting('event_default_image');

  //Add option to upload events default image

  $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'event_default_image', array(

    'label'             => __('Events Default Image', $text_domain),

    'section'           => 'news_event_image',

    'settings'          => 'event_default_image',
    
    'description'       => __('Best Image Size (580 X 435)', $text_domain)

  )));

  $wp_customize->add_setting('Header_social_media_label');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'Header_social_media_label', array(

    'label'             => __('Header Social Media label', $text_domain),

    'section'           => 'Social_media',

    'settings'          => 'Header_social_media_label'

  )));



  $wp_customize->add_setting('Footer_social_media_label');

  $wp_customize->add_control(new WP_Customize_Code_Editor_Control($wp_customize, 'Footer_social_media_label', array(

    'label'             => __('Footer Social Media label', $text_domain),

    'section'           => 'Social_media',

    'settings'          => 'Footer_social_media_label'

  )));



  $wp_customize->add_setting('facebook_icon_class');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'facebook_icon_class', array(

    'label'             => __('Facebook Icon Class', $text_domain),

    'section'           => 'Social_media',

    'settings'          => 'facebook_icon_class'

  )));



  $wp_customize->add_setting('facebook_url');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'facebook_url', array(

    'label'             => __('Facebook Url', $text_domain),

    'section'           => 'Social_media',

    'settings'          => 'facebook_url'

  )));



  $wp_customize->add_setting('twitter_icon_class');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'twitter_icon_class', array(

    'label'             => __('Twitter Icon Class', $text_domain),

    'section'           => 'Social_media',

    'settings'          => 'twitter_icon_class'

  )));



  $wp_customize->add_setting('twitter_url');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'twitter_url', array(

    'label'             => __('Twitter Url', $text_domain),

    'section'           => 'Social_media',

    'settings'          => 'twitter_url'

  )));



  $wp_customize->add_setting('youtube_icon_class');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'youtube_icon_class', array(

    'label'             => __('YouTube Icon Class', $text_domain),

    'section'           => 'Social_media',

    'settings'          => 'youtube_icon_class'

  )));



  $wp_customize->add_setting('youtube_url');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'youtube_url', array(

    'label'             => __('YouTube Url', $text_domain),

    'section'           => 'Social_media',

    'settings'          => 'youtube_url'

  )));



  $wp_customize->add_setting('instagram_icon_class');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'instagram_icon_class', array(

    'label'             => __('Instagram Icon Class', $text_domain),

    'section'           => 'Social_media',

    'settings'          => 'instagram_icon_class'

  )));



  $wp_customize->add_setting('instagram_url');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'instagram_url', array(

    'label'             => __('Instagram Url', $text_domain),

    'section'           => 'Social_media',

    'settings'          => 'instagram_url'

  )));





  $wp_customize->add_section( 'footer_site_logo', array(

    'priority'          => 1002,

    'panel'             => 'ebr_foundationpress_options',

    'title'             => __( 'Footer', $text_domain ),

    'description'       => __( 'Upload the site logo', $text_domain )

  ));



  $wp_customize->add_setting('footer_subscribe_title');

  $wp_customize->add_control(new WP_Customize_Code_Editor_Control($wp_customize, 'footer_subscribe_title', array(

    'label'             => __('Newsletter Subscribe Title', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_subscribe_title'

  )));



  //setting for Footer_site_logo

  $wp_customize->add_setting('footer_site_logo');

  //Add option to upload main site logo

  $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'footer_site_logo', array(

    'label'             => __('Footer Site Logo', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_site_logo'

  )));




  $wp_customize->add_setting('footer_title');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_title', array(

    'label'             => __('Footer sitemap title text', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_title'

  )));



  $wp_customize->add_setting('footer_department_menu_title');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_department_menu_title', array(

    'label'             => __('Footer department menu title text', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_department_menu_title'

  )));



  $wp_customize->add_setting('footer_programs_menu_title');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_programs_menu_title', array(

    'label'             => __('Footer programs menu title text', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_programs_menu_title'

  )));



  $wp_customize->add_setting('footer_video_image');

  $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'footer_video_image', array(

    'label'             => __('Footer Video image', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_video_image'

  )));



  $wp_customize->add_setting('footer_video_title');

  $wp_customize->add_control(new WP_Customize_Code_Editor_Control($wp_customize, 'footer_video_title', array(

    'label'             => __('Footer Video Title', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_video_title'

  )));



  $wp_customize->add_setting('footer_video_play_icon');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_video_play_icon', array(

    'label'             => __('Footer Video Play Icon Class', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_video_play_icon'

  )));



  $wp_customize->add_setting('footer_video_url');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_video_url', array(

    'label'             => __('Footer Video Url', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_video_url'

  )));



  $wp_customize->add_setting('footer_notice');

  $wp_customize->add_control(new WP_Customize_Code_Editor_Control($wp_customize, 'footer_notice', array(

    'label'             => __('Footer Notice', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_notice'

  )));



  $wp_customize->add_setting('footer_notice1');

  $wp_customize->add_control(new WP_Customize_Code_Editor_Control($wp_customize, 'footer_notice1', array(

    'label'             => __('Footer Notice Second', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_notice1'

  )));



  $wp_customize->add_setting('footer_image1');

  $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'footer_image1', array(

    'label'             => __('Footer image one', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_image1'

  )));



  $wp_customize->add_setting('footer_image1_url');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_image1_url', array(

    'label'             => __('Footer image one url', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_image1_url'

  )));



  $wp_customize->add_setting('footer_image2');

  $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'footer_image2', array(

    'label'             => __('Footer image two', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_image2'

  )));



  $wp_customize->add_setting('footer_image2_url');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_image2_url', array(

    'label'             => __('Footer image two url', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_image2_url'

  )));



  $wp_customize->add_setting('footer_image3');

  $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'footer_image3', array(

    'label'             => __('Footer image three', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_image3'

  )));



  $wp_customize->add_setting('footer_image3_url');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_image3_url', array(

    'label'             => __('Footer image three url', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_image3_url'

  )));



  $wp_customize->add_setting('footer_image4');

  $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'footer_image4', array(

    'label'             => __('Footer image four', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_image4'

  )));



  $wp_customize->add_setting('footer_image4_url');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_image4_url', array(

    'label'             => __('Footer image four url', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_image4_url'

  )));



  $wp_customize->add_setting('footer_image5');

  $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'footer_image5', array(

    'label'             => __('Footer image five', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_image5'

  )));



  $wp_customize->add_setting('footer_image5_url');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_image5_url', array(

    'label'             => __('Footer image five url', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_image5_url'

  )));



  $wp_customize->add_setting('footer_image6');

  $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'footer_image6', array(

    'label'             => __('Footer image six', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_image6'

  )));



  $wp_customize->add_setting('footer_image6_url');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_image6_url', array(

    'label'             => __('Footer image six url', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_image6_url'

  )));



  $wp_customize->add_setting('footer_image7');

  $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'footer_image7', array(

    'label'             => __('Footer image seven', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_image7'

  )));



  $wp_customize->add_setting('footer_image7_url');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_image7_url', array(

    'label'             => __('Footer image seven url', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'footer_image7_url'

  )));



  $wp_customize->add_setting('copyright');

  $wp_customize->add_control(new WP_Customize_Code_Editor_Control($wp_customize, 'copyright', array(

    'label'             => __('Copyright', $text_domain),

    'section'           => 'footer_site_logo',

    'settings'          => 'copyright'

  )));





  $wp_customize->add_setting('address_label');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'address_label', array(

    'label'             => __('Address:', $text_domain),

    'section'           => 'company_address',

    'settings'          => 'address_label'

  )));



  

  $wp_customize->add_setting('phone_label');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'phone_label', array(

    'label'             => __('Phone Number:', $text_domain),

    'section'           => 'company_info',

    'settings'          => 'phone_label'

  )));



  $wp_customize->add_setting('fax_label');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'fax_label', array(

    'label'             => __('Fax Number:', $text_domain),

    'section'           => 'company_info',

    'settings'          => 'fax_label'

  )));



  $wp_customize->add_setting('address_label');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'address_label', array(

    'label'             => __('Company Address:', $text_domain),

    'section'           => 'company_info',

    'settings'          => 'address_label'

  )));



  $wp_customize->add_setting('email_label');

  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'email_label', array(

    'label'             => __('Email Address:', $text_domain),

    'section'           => 'company_info',

    'settings'          => 'email_label'

  )));





}

add_action( 'customize_register', 'ebr_customize' );



