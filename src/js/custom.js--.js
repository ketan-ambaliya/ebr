jQuery(document).ready(function($) {

    $('.tab2').tab({
        trigger_event_type: 'mouseover' //mouseover
    });

    $('.footer_slider').slick({
        autoplay: false,
        slidesToShow: 6,
        slidesToScroll: 6,
        dots: true,
        infinite: true,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 500,
            settings: {             
                infinite: false,
                slidesToShow: 2,
                slidesToScroll: 2
            }            
        },
            {
               breakpoint: 991,
               settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
               }
       },
            {
               breakpoint: 1279,
               settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
               }
       }
        ]

    });

    $(".slider").slick({ 
        autoplay: false,
        dots: true,
        responsive: [{
            breakpoint: 500,
            settings: {
                dots: false,
                arrows: false,
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });

    

    // For each image with an SVG class, execute the following function.
    $("img.svg").each(function() {
        // Perf tip: Cache the image as jQuery object so that we don't use the selector muliple times.
        var $img = jQuery(this);
        // Get all the attributes.
        var attributes = $img.prop("attributes");
        // Get the image's URL.
        var imgURL = $img.attr("src");
        // Fire an AJAX GET request to the URL.
        $.get(imgURL, function(data) {
            // The data you get includes the document type definition, which we don't need.
            // We are only interested in the <svg> tag inside that.
            var $svg = $(data).find('svg');
            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');
            // Loop through original image's attributes and apply on SVG
            $.each(attributes, function() {
                $svg.attr(this.name, this.value);
            });
            // Replace image with new SVG
            $img.replaceWith($svg);
        });
    });


  

    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });


    $('.header').on('click', '.search-toggle', function(e) {
        var selector = $(this).data('selector');

        $(selector).toggleClass('show').find('.search-input').focus();
        $(this).toggleClass('active');

        e.preventDefault();
    });

    $('.navbar-toggle').click(function() {
        $('.navbar-collapse').toggleClass('right');
        $('.navbar-toggle').toggleClass('indexcity');

    });

    $('.second-button').on('click', function() {
        $('.animated-icon2').toggleClass('open');
        $('body').toggleClass('bodyfix');
    });

    $("#yellowbox").click(function() {
        $('#yellowbox').find('.box1, .box2').toggle("slow")
        //$('#yellowbox').find(".box2").toggle();
        // $('#yellowbox').find(".box1").hide(500);
    });
    $("#bluebox").click(function() {
        $('#bluebox').find('.box1, .box2').toggle("slow")
        //    $('#bluebox').find(".box2").show();
        //    $('#bluebox').find(".box1").hide(500);
    });

    $("#greenbox").click(function() {
        $('#greenbox').find('.box1, .box2').toggle("slow")
        //    $('#greenbox').find(".box2").show();
        //    $('#greenbox').find(".box1").hide(500);
    });

    var a = 0;
    jQuery(window).scroll(function() {
        var oTop = jQuery('#microsoftcounter').offset().top - window.innerHeight;
        if (a == 0 && jQuery(window).scrollTop() > oTop) {
            jQuery('.count').each(function() {
                var $this = jQuery(this),
                    countTo = $this.attr('data-count');
                jQuery({
                    countNum: $this.text()
                }).animate({
                    countNum: countTo
                }, {
                    duration: 4000,
                    easing: 'swing',
                    step: function() {
                        $this.text(commaSeparateNumber(Math.floor(this.countNum)));
                    },
                    complete: function() {
                        $this.text(commaSeparateNumber(this.countNum));
                        //alert('finished');
                    }
                });
            });
            a = 1;
        }

    });

    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
        }
        return val;
    }    

    var menu = $(".bottom_menu");
    var indicator = $('<span class="indicator"></span>');
    menu.append(indicator);
    position_indicator(menu.find("li.active"));
    setTimeout(function() {
        indicator.css("opacity", 1);
    }, 500);
    menu.find("li").mouseenter(function() {
        position_indicator($(this));
    });
    menu.find("li").mouseleave(function() {
        position_indicator(menu.find("li.active"));
    });

    function position_indicator(ele) {
        var left = ele.offset().left - 10;
        var width = ele.width();
        indicator.stop().animate({
            left: left,
            width: width
        });
    }

    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        focusOnSelect: true
    });

     

});



/*jQuery(document).on('click', '.navbar .navbar-toggler', function(e) {
  e.preventDefault();
  var body = jQuery('body');
  if(!body.attr('style')){
    goodhack(body);
  } else {
    goodhack(body);
  }
});

function goodhack(body){
  if(body.css('overflow') == 'hidden'){
    body.css('overflow', 'auto');
  } else {
    body.css('overflow', 'hidden');
  }
}*/