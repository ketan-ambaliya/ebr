<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$understrap_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
	'/woocommerce.php',                     // Load WooCommerce functions.
	'/editor.php',                          // Load Editor functions.
	'/deprecated.php',   					// Load deprecated functions.
	'/client-specific-functions.php',       // Customizer Option functions.             
);

foreach ( $understrap_includes as $file ) {
	$filepath = locate_template( 'inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}


// Allow SVG to media uplode. 
function ebr_add_file_types_to_uploads($file_type){
  $new_filetype = array();
  $new_filetype['svg'] = 'image/svg+xml';
  $file_type = array_merge($file_type, $new_filetype );
  return $file_type;
}
add_action('upload_mimes', 'ebr_add_file_types_to_uploads');

// Login Page logo change
function ebr_loginlogo() {
echo '<style type="text/css">
h1 a {background-image: url('.get_bloginfo('template_directory').'/img/logo.png) !important; width: 250px !important; background-size:250px !important;margin-bottom:-20px !important;}
</style>';
}
add_action('login_head', 'ebr_loginlogo'); 

// changing the logo link from wordpress.org to your site
function ebr_login_url() {  return home_url(); }
add_filter( 'login_headerurl', 'ebr_login_url' );

// changing the alt text on the logo to show your site name
function ebr_login_title() { return get_option( 'blogname' ); }
add_filter( 'login_headertitle', 'ebr_login_title' );

// Common options menu
if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Common Options',
        'menu_title'    => 'Common Options',
        'menu_slug'     => 'common-options',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));    
}


add_action( 'wp_ajax_nopriv_load-filter', 'prefix_load_cat_posts' );
add_action( 'wp_ajax_load-filter', 'prefix_load_cat_posts' );
// Function for news ajax filter
function prefix_load_cat_posts () {
    global $post;
    $tagid = $_POST[ 'tag' ];
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
         $args = array (
            'post_type' => 'news',
            'tag_id' => $tagid,
            'posts_per_page' => '5',
            'order' => 'DESC',
            'paged' => $paged

    );
    $posts = get_posts( $args );
    ob_start ();?>

    <?php $posts = new WP_Query($args);
      if ($posts->have_posts()){?>
      <?php  while ($posts->have_posts()){ $posts->the_post(); ?>

      	<div class="row">
        <div class="col-12 col-md-5 spacer">
	        <a href="<?php the_permalink(); ?>">        
	            <div class="imgcontainer">
					<div class="img-thumbnails">	    
		             <?php  if ( has_post_thumbnail() ) {  
                                          the_post_thumbnail();  
                              }elseif( get_theme_mod( 'news_default_image' ) ){
                                       
                                echo '<img src="' .get_theme_mod( 'news_default_image' ).'" />';
                          } else {

                                  echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
                                    
                                }
                          ?> 
	                </div>
				</div>	
			</a> 	
		</div>
		<div class="col-12 col-md-7 spacer">
			<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
			<?php $tags =  get_the_tags();?>
                    <p class="n_date"><span style="color: <?php the_field('tag_color',  'term_'.$tags[0]->term_id); ?>;"><?php  echo $tags[0]->name; ?></span>  <?php echo get_the_date('M. j Y');?></p>
			<p class="sortcontent">	<?php $content = get_the_content();
	                  $content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,270); ?>...
			</p>	
			<a href="<?php the_permalink(); ?>" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>          
        </div>
      <div class="col-12"><hr/></div>
     </div>
   <?php } } echo $response;?>

	<div class="pagination-sec"><?php  pagination_bar_my( $posts, $tagid ); ?></div>
    
   <?php die();
   }

// Function for news pagination setup
function pagination_bar_my( $custom_query, $tagid ) {
  $number = isset($_POST[ 'number' ]) ? $_POST[ 'number' ] : ' 1 ';
  
	$tagid = $_POST[ 'tag' ];
 	$args = array (
            'post_type' => 'news',
             'tag_id' => $tagid,
            'posts_per_page' => '-1',
            'order' => 'DESC',


    );
    $cptaLimit = 5;
    $cpta_Query = new WP_Query( $args );
    $cpta_Count = count($cpta_Query->posts);
    $cpta_Paginationlist = ceil($cpta_Count/$cptaLimit);

    $last = ceil( $cpta_Paginationlist );

    $cptaType   = "news";
    
    if($cpta_Count > 5){

    $setPagination .="<ul class='pagination'>";

    if($number != 1){
     $setPagination .="<li class='page-item $active'><a href='javascript:void(0);' id='post'  data-posttype='$cptaType' data-cattype='$tagid' onclick='cptaajaxPagination(1,$cptaLimit);' class='page-link'><i class='fa fa-angle-double-left' aria-hidden='true'></i></a></li>";
    }
    
    //for( $cpta=1; $cpta<=$cpta_Paginationlist; $cpta++){
     for($cpta = max(1, $number - 5); $cpta <= min($number + 5, $last); $cpta++){
   
          if( $number ==  $cpta ){ 
            $active="active"; 
          } elseif( 1 ==  $cpta ){ 
            if( $number ==  null ){
            $active="active"; 
            }
          } else{ 
            $active=""; 
          }
        


        $setPagination .="<li class='page-item $active' ><a href='javascript:void(0);' id='post'  data-posttype='$cptaType' data-cattype='$tagid' onclick='cptaajaxPagination($cpta,$cptaLimit);' class='page-link'>$cpta</a></li>";
        }
        if($number != $last){
        $setPagination .="<li class='page-item $active'><a href='javascript:void(0);' id='post'  data-posttype='$cptaType' data-cattype='$tagid' onclick='cptaajaxPagination($last,$cptaLimit);' class='page-link'><i class='fa fa-angle-double-right' aria-hidden='true'></i></a></li>";
      }
      $setPagination .="</ul>";
      echo $setPagination;
    }
}


add_action( 'wp_ajax_nopriv_cptapagination', 'cptapagination' );
add_action( 'wp_ajax_cptapagination', 'cptapagination' );
// Function for news pagination ajax filter
function cptapagination($atts) {
  global $wpdb;
  $number = $_POST[ 'number' ];
  $limit = $_POST[ 'limit' ];
  $cptapost = $_POST[ 'cptapost' ];
  $tagid = $_POST[ 'tag' ];
          
     $paged = $number;
         $args = array (
            'post_type' => 'news',
             'tag_id' => $tagid, 
            'posts_per_page' => $limit,
            'order' => 'DESC',
            'paged' => $paged

    );?>

    <?php $posts = new WP_Query($args);
      if ($posts->have_posts()){?>
      <?php  while ($posts->have_posts()){ $posts->the_post(); ?>

       <div class="row">
        <div class="col-12 col-md-5 spacer">
	        <a href="<?php the_permalink(); ?>">        
	            <div class="imgcontainer">
					<div class="img-thumbnails">
		         				    
		            <?php if ( has_post_thumbnail() ) {  
                                          the_post_thumbnail();  
                              }elseif( get_theme_mod( 'news_default_image' ) ){
                                       
                                echo '<img src="' .get_theme_mod( 'news_default_image' ).'" />';
                          } else {

                                  echo '<img src="' . get_stylesheet_directory_uri(). '/img/nofound.png" />';
                                    
                                }
                          ?> 
	                </div>
				</div>	
			</a> 	
		</div>
		<div class="col-12 col-md-7 spacer">
			<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
			<?php $tags =  get_the_tags();?>
                    <p class="n_date"><span style="color: <?php the_field('tag_color',  'term_'.$tags[0]->term_id); ?>;"><?php  echo $tags[0]->name; ?></span>  <?php echo get_the_date('M. j Y');?></p>
			<p class="sortcontent">
				<?php $content = get_the_content();
	                  $content1 = wp_filter_nohtml_kses( $content ); echo substr($content1,0,270); ?>...
			</p>	
			<a href="<?php the_permalink(); ?>" class="enlink">Read more <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                           
        </div>
      <div class="col-12"><hr/></div>
     </div>
   <?php } }

   echo $response;?>
    <div class="pagination-sec"><?php  pagination_bar_my( $posts, $tagid ); ?></div>
    </div>        
    </div>
<?php die();}


//WPCF7 email
add_filter( 'shortcode_atts_wpcf7', 'custom_shortcode_atts_wpcf7_filter', 10, 3 );
 
function custom_shortcode_atts_wpcf7_filter( $out, $pairs, $atts ) {
    $my_attr = 'destination-email';
 
    if ( isset( $atts[$my_attr] ) ) {
        $out[$my_attr] = $atts[$my_attr];
    }
 
    return $out;
}

// Add custom post type tags in tag page
function ebr_add_custom_types( $query ) {
    if( is_tag() && $query->is_main_query() ) {

        // this gets all post types:
        $post_types = get_post_types();

        // alternately, you can add just specific post types using this line instead of the above:
        // $post_types = array( 'post', 'your_custom_type' );

        $query->set( 'post_type', $post_types );
    }
}
add_filter( 'pre_get_posts', 'ebr_add_custom_types' );

// Add custom post type tags template
add_filter( 'template_include', 'news_tag_template' );

function news_tag_template( $template ) {
  
    $tax = 'post_tag';
    $news_tag = get_terms( $tax );
    $taglist=array();
     foreach ( $news_tag as $tag ) {
      array_push($taglist, $tag->slug);
     }
    
    if ( is_tag() && in_array( get_query_var( 'tag' ), $taglist )  ) {
        $new_template = locate_template( array( 'tag-news.php' ) );
       
        if ( '' != $new_template ) {
          
             return $new_template ;
        }
    }

    return $template;
}


/**
 * Add notice to post thumbnail meta box
 */
function ebr_admin_post_thumbnail_add_label($content, $post_id, $thumbnail_id)
{
    $post = get_post($post_id);
    if ($post->post_type == 'news' || $post->post_type == 'tribe_events') {
        $content .= '<small><i>(Best Image Size 580 X 435)</small></i>';
        return $content;
    }

    return $content;
}
add_filter('admin_post_thumbnail_html', 'ebr_admin_post_thumbnail_add_label', 10, 3);



// Remove type/javascript and css atribute from css and js
add_action( 'template_redirect', function(){
   ob_start( function( $buffer ){
       $buffer = str_replace( array( 'type="text/javascript"', "type='text/javascript'" ), '', $buffer );

       // Also works with other attributes...
       $buffer = str_replace( array( 'type="text/css"', "type='text/css'" ), '', $buffer );

       return $buffer;
   });
});