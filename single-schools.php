<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$path = get_template_directory_uri();
?>
<div class="wrapper" id="full-width-page-wrapper">
	<div class="container-fluid no-padding">
<?php if( have_rows('flexible_content_sections') ): 
		while ( have_rows('flexible_content_sections') ) : the_row(); ?>

	<?php if( get_row_layout() == 'content_area_with_1_image'):
		$image_alignment = get_sub_field('image_alignment'); 
		$image = get_sub_field('image'); 
		$main_title = get_sub_field('main_title'); 
		$content = get_sub_field('content'); 
		$button_text = get_sub_field('button_text'); 
		$button_url = get_sub_field('button_url'); 
		$name = get_sub_field('name'); 
		$sub_title = get_sub_field('sub_title'); ?>

		<?php if($image_alignment == "Top"){?>

			<?php if( !empty($image)):?><div class="overlapcontent" style="background-image: url(<?php echo $image['url']; ?>);">	<?php endif;?>	
			</div>	
			<div class="overlaptextarea">
				<div class="overalappart inner-container">		
						<div class="row paragraphpart">
							<div class="col-md-12 text-center">
									
								<?php if( !empty($name)):?><label><?php echo $name; ?></label>
								<?php endif;?>
								<?php if( !empty($main_title)):?><h1><?php echo $main_title; ?></h1><?php endif;?>
								<?php if( !empty($sub_title)):?><h4><?php echo $sub_title; ?></h4><?php endif;?>
							</div>
						</div>
						<?php if( !empty($content)):?><?php echo $content; ?><?php endif;?>
						<?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>	
				</div>
			</div>
		<?php } else {?>

			<section class="areapart-img">	
				<div class="row">
					<div class="col-lg-6 leftimg <?php if($image_alignment == 'Right'){ echo 'order-2'; }?>">
						<img src="<?php echo $image['url']; ?>">
					</div>	
					<div class="col-lg-6">
						<div class="slide-description">
							<?php if( !empty($main_title)):?><h1><?php echo $main_title; ?></h1><?php endif;?>
		       				<?php if( !empty($name)):?><p><label><?php echo $name; ?></label></p>
							<?php endif;?>
		       				<hr>
							<?php if( !empty($sub_title)):?><h4><?php echo $sub_title; ?></h4><?php endif;?>
							<?php if( !empty($content)):?><p><?php echo $content; ?></p><?php endif;?>
							<?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>
						</div>
					</div>	
				</div>	
			</section>

		<?php }?>

		<?php elseif( get_row_layout() == 'contact_details'):
			$phone_number = get_sub_field('phone_number');
			$address = get_sub_field('address');
			$fax = get_sub_field('fax');
			$principal_detail = get_sub_field('principal_detail'); 

			?>

		<section class="yellowadvance yfixed">
			<div class="content-conainer">
				<div class="row justify-content-center display-flex">
					<div class="col-md-6">
						<div class="darkshade">
						<h2 class="no-background">
							<span>Address</span>					
					    </h2>
					    <?php if( !empty($address)):?><p><?php echo $address; ?></p>
					 <p class="text-right"><a href="https://www.google.com/maps/place/<?php echo $address; ?>" target="_blank" class="schoolviewmap"><?php _e('[View On Map]','roots'); ?></a></p><?php endif;?>

						</div>
					</div>
					<div class="col-md-6">
						<div class="darkshade">
						<h2 class="no-background">
							<span>Contact</span>					
					    </h2>
					    <p>
					    <?php if( have_rows('phone_number') ): $i=1;
										while ( have_rows('phone_number') ) : the_row();
										 $phone = get_sub_field('phone'); ?>

										<?php if( !empty($phone)):?>
											
												<?php if($i == 1){?><strong>Ph:</strong><?php } ?>
												<?php if($i != 1){?> | <?php } ?>
												<a href="tel:<?php echo $phone;?>"><?php echo $phone;?></a>   
										<?php endif;?>
		

										<?php $i++; endwhile; endif;?>
					  	</p>
					    <?php if( !empty($fax)):?><p><strong>Fax</strong> : <?php echo $fax; ?></p><?php endif;?>
						</div>
					</div>
					<div class="col-md-12">
						<div class="darkshade">
						<h2 class="no-background">
							<span>Principal</span>					
					    </h2>
					    <?php if( !empty($principal_detail)):?><p><?php echo $principal_detail; ?></p><?php endif;?>
						</div>
					</div>					
			</div>
		</section>

		<?php elseif( get_row_layout() == 'content_area_with_2_image'):
				$with_background = get_sub_field('with_background'); 
				$main_title = get_sub_field('main_title'); 
				$sub_title = get_sub_field('sub_title'); 
				$content = get_sub_field('content'); 
				$button_text = get_sub_field('button_text'); 
				$button_url = get_sub_field('button_url'); 
				$first_image = get_sub_field('first_image'); 
				$second_image = get_sub_field('second_image'); 
			?>

		<section class="did-you-konw-section <?php if($with_background == "Yes"){ echo 'blue_bg'; }?>" style="<?php if($with_background == "Yes"){ echo 'background-color: #002e59'; }?>">
			<div class="inner-container">		
				<div class="row">
					<div class="col-md-8">
						<?php if( !empty($main_title)):?><h2 class="no-background"><span><?php echo $main_title; ?></span></h2><?php endif;?>	
						<?php if( !empty($sub_title)):?><h3><?php echo $sub_title; ?></h3><?php endif;?>
						<?php if( !empty($content)):?><p><?php echo $content; ?></p><?php endif;?>
						<?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>
					</div>
					<div class="col-md-4">
						<div class="image-thum-block">
							<?php if( !empty($first_image)):?><img src="<?php echo $first_image['url']; ?>" alt="" class="primary-image"><?php else:?>
									<img src="<?php echo $path."/img/nofound.png"; ?>" class="primary-image"><?php endif;?>
							<?php if( !empty($second_image)):?><img src="<?php echo $second_image['url']; ?>" alt="" class="secondary-image"><?php else:?>
									<img src="<?php echo $path."/img/nofound.png"; ?>" class="secondary-image"><?php endif;?>
						</div>
					</div>
				</div>	
			</div>		
		</section>

		<?php elseif( get_row_layout() == 'parallax_green_blocks_with_title'):
			$background_image = get_sub_field('background_image');
			$title = get_sub_field('title');  ?>

			<section class="parallaxpart threebox" style="background-image: url(<?php echo $background_image['url']; ?>">
			<?php if( !empty($title)):?><div class="ribbonpart"><label><?php echo $title; ?></label></div><?php endif;?>
			<div class="inner-container  my-margi">
			<div class="row justify-content-center display-flex">
				<?php if( have_rows('boxs') ): 
					while ( have_rows('boxs') ) : the_row();
					$box_title = get_sub_field('box_title'); 
					$box_description = get_sub_field('box_description'); 
					$button_text = get_sub_field('button_text'); 
					$button_url = get_sub_field('button_url'); ?>
						<div class="col-md-6 col-lg-4">
							<div class="greenbox">
							<?php if( !empty($box_title)):?>
								<h2>
									<?php echo $box_title; ?>					
							    </h2>
							    <hr/>
							<?php endif;?>
						    <?php if( !empty($box_description)):?><p class="text-center"><?php echo $box_description; ?></p><?php endif;?>
						   
						     <?php if( !empty($button_url)):?><a href="<?php echo $button_url; ?>" class="btn"><?php echo $button_text; ?></a><?php endif;?>	
							</div>
						</div>
					<?php endwhile; endif;?>					

				</div>	
			</div>	
			</div>
		</section>


	
		<?php endif; ?>
<?php 
    endwhile;
else :
   echo 'No Row Found';
endif;
?>
		<div class="clerfix"></div>
		</div>
	</div><!-- #content -->
</div><!-- #full-width-page-wrapper -->

<?php get_footer(); ?>

